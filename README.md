# Introduction

The **xyz** security tool checks a system for common configuration faults
that could cause sensitive information or interfaces to be exposed, such as
SSH private keys or GPG secret keys without passphrases, or service accounts
without a password.

It can be run unattended from a **cron** job to generate a system-wide
report which can be delivered by email or detected by a monitoring system
such as [Zabbix](https://www.zabbix.com).

Many of the 150+ checks are derived from recommendations in
[Center for Internet Security](https://www.cisecurity.org/) benchmark
documents, though since **xyz** is intended for use on multiple operating
systems - various GNU/Linux distributions, FreeBSD, and OpenBSD - these
checks are *derived from* standard recommendations rather than
*equivalent to* them.  **xyz** is not associated with or endorsed by CIS or
any other organisation.

Use **xyz** as a risk reduction tool to limit accidental exposure: run it
first *before* a server is placed into a production environment, and then
run it regularly thereafter to check that mistakes have not crept in during
day-to-day maintenance.  It does not look for indicators of compromise and
should not be run after an incident.  A compromised server should be deleted
and rebuilt, and **xyz** run on the rebuilt system as part of hardening
before deployment.

This tool is built to be lightweight.  It complements more in-depth tools
such as [OpenSCAP](https://www.open-scap.org/) rather than attempting to
replace them.

User-defined check and fix actions can be added.  By packaging and deploying
your own check, fix, and hook functions, embodying your estate's
configuration policies, **xyz** can be extended to serve as a configuration
policy compliance tool.  For example, regular **xyz** checks could be run to
ensure that configuration changes made by other tools such as Ansible or
Puppet have had the desired effect and have not introduced regressions.


# Documentation

A manual page is included in this distribution ("`man xyz`").  Before
installation, it is in "[docs/xyz.8](./docs/xyz.8.md)".

Changes are listed in "[docs/NEWS.md](./docs/NEWS.md)".

Developers and translators, please see "[docs/DEVELOPERS.md](./docs/DEVELOPERS.md)".


# Installation

See "[docs/INSTALL](./docs/INSTALL)" for more about the _configure_ script.

The typical process for a system-wide install is:

    sh ./configure --prefix=/usr
    make
    sudo make install

This requires Make ("`sudo apt install make`" on Debian or Ubuntu systems).

If this is not a packaged release, the _configure_ script is not included. 
It is generated with the GNU build system tools (_autoconf_, _aclocal_,
_automake_).  On Debian or Ubuntu, run "`sudo apt install automake`".  Once
those tools are in place, call "`autoreconf -is`" to generate the
_configure_ script, and run it as described above.


# Copyright, bug reporting, and acknowledgements

Copyright (C) 2024-2025 Andrew Wood.

License GPLv3+: GNU GPL version 3 or later <https://www.gnu.org/licenses/gpl-3.0.html>.

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License, version
3, in "[docs/COPYING](./docs/COPYING)".  If not, see
<https://www.gnu.org/licenses/gpl-3.0.html>.

Please report bugs or request features via the issue tracker linked from the
home page.

The **xyz** home page is at:

> [https://ivarch.com/programs/xyz.shtml](https://ivarch.com/programs/xyz.shtml)

The latest version can always be found here.

---
