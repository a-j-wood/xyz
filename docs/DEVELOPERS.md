# Notes for developers and translators


## Translation notes

There are no notes for translators yet, as internationalisation attempts
have not yet begun.  Please contact the author if you wish to assist.


## Release checklist

The package maintainer should run through these steps for a new release:

 * Version bump and documentation checks:
   * Update the version in _configure.ac_ and _docs/NEWS.md_
   * Check that _docs/NEWS.md_ is up to date
   * Check that the manual _docs/xyz.8_ is up to date
   * Run "`make docs/xyz.8.md`" and, if using VPATH, copy the result to the source directory
 * Ensure everything has been committed to the repository
 * Run "`autoreconf -is`" in the source directory
 * Consistency and build checks:
   * Wipe the build directory, and run "`./configure`" there
   * Run "`make distcheck`"
   * Run "`./configure && make check`", using the _tar.gz_ that was just created
 * Run "`make release MAINTAINER=<signing-user>`"
 * Update the project web site:
   * Copy the release _.tar.gz_, _.txt_, and _.asc_ files to the web site
   * Run "`make docs/xyz.8.html docs/NEWS.html`" and copy the results to the web site
   * Update the version numbers on the web site
   * Update the package index on the web site
 * Create a new release in the repository, and apply the associated tag

