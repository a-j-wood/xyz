### 0.2.22 - UNRELEASED

 * *feature:* new check _fs_path_integrity_
 * *feature:* new utility functions **fsOtherCanWrite**, **fsGroupOrOtherCanWrite**, and **fsGroupAndOtherCanWrite**

### 0.2.20 - 9 February 2025

 * *feature:* new **--json** option for the "**reformat**" and "**info**" actions ([#6](https://codeberg.org/ivarch/xyz/issues/6))
 * *fix:* add the prerequisites to the *sched_root_atjobs* check
 * *fix:* correct "**fmt**" invocation for reformatting on BSD systems
 * *fix:* adjust scheduler config paths on BSD systems

### 0.2.16 - 2 February 2025

 * *feature:* checks for crontab and "at" job settings and queued jobs ([#2](https://codeberg.org/ivarch/xyz/issues/2))
 * *feature:* checks for processes running unlinked binaries or libraries, or anonymous files ([#4](https://codeberg.org/ivarch/xyz/issues/4)), ([#5](https://codeberg.org/ivarch/xyz/issues/5))
 * *feature:* extended check prerequisites to permit test commands
 * *fix:* corrected bug in detection of disabled kernel modules
 * *fix:* allowed stricter permissions on the audit log directory
 * *cleanup:* added "**make analyse**" using "**shellcheck**" and addressed the warnings it produced

### 0.2.10 - 26 December 2024

 * *feature:* new **--until** option so that **ignore** and **exception-add** entries can be given an expiry time
 * *feature:* new **--delay** option to add a random startup delay when running from cron
 * *feature:* checks for unnecessary Linux kernel modules, derived from CIS Debian 12 benchmark section 1.1.1
 * *feature:* new variables _kernelName_, _kernelRelease_, and _startStamp_
 * *feature:* new **info** action, to show information about the system
 * *fix:* correct the parsing of "--option=VALUE" option syntax

### 0.2.4 - 23 March 2024

 * *feature:* checks for configuration files in user home directories, derived from CIS Debian 12 benchmark section 7.2.10
 * *feature:* added new variable _osIsRhelDescendant_
 * *fix:* adjusted "accounts with GID 0" check to make allowances for RHEL descendants

### 0.2.1 - 18 March 2024

 * *fix:* corrected an installer bug which creates the hooks and extensions directories in the wrong place

### 0.2.0 - 17 March 2024

This release extends XYZ to include over 100 more checks, and includes new
options which will assist with future work to add unit tests into the build
process.

 * checks for password, shadow, group file permissions derived from CIS Debian 12 benchmark section 7.1
 * renamed acct_shadow_permission to rights_etcshadow_permissions for consistency
 * checks for SSH host private and public key permissions
 * checks for audit log and config permissions derived from CIS Debian 12 benchmark section 6.4.4
 * checks for listening network ports that are reachable from elsewhere
 * new utility functions **fsHasExecutePermissions**, **fsGroupOrOtherCanRead**, and **fsOtherCanRead**
 * new options **-R**, **-C**, **-E**, **-H** to assist with future tests

### 0.1.0 - 6 March 2024

This release extends XYZ to incorporate dozens of new checks, derived from
over 50 CIS recommendations, on top of its original checks for unprotected
SSH and GPG private keys.  It also adds a more fine-grained exceptions
mechanism to allow special cases to be permitted, and a more human-readable
(and optionally colourised) report format.

 * new **reformat** action to reformat fault reports for human eyes
 * new exceptions mechanism to exclude specific results such as allowed private key files
 * 80 checks now defined, derived from over 50 CIS benchmark recommendations
 * filesystem checks derived from CIS Debian 12 benchmark section 1.1.2
 * information leak checks derived from CIS Debian 12 benchmark section 1.6
 * file permissions checks derived from CIS Debian 12 benchmark section 2.4.1, 5.1, and others
 * user account checks derived from CIS Debian 12 benchmark sections 5.4.2, 7.2, and others

### 0.0.1 - 1 March 2024

 * package and manual created
 * extensions framework built
 * detection of unprotected SSH private keys
 * detection of unprotected GPG secret keys
 * detection of /tmp permission and ownership issues
 * detection of duplicate root accounts
 * detection of system and guest accounts with usable passwords
 * detection of user accounts with no home directory
 * detection of user accounts with world writable home directory
 * detection of root account with empty password
