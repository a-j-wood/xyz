#!/bin/sh
#
# Usage: sh build-package.sh source-1.2.3.tar.gz
#
# Given a tar.gz, build a package for the current OS.
#
# Supports these packaging types:
#
#   .deb (Debian, Ubuntu, etc) - requires dpkg-dev, debhelper
#   .rpm (AlmaLinux, Rocky Linux, OpenSUSE etc) - requires rpmbuild
#
# If the MAINTAINER environment variable is set, this is used as a GPG key
# ID for package signing.
#
# The packages built with this script are not "official", in that they are
# not part of any Linux distribution and may not follow the distribution's
# policies.  Use at your own risk.

sourceArchive="$1"
test -n "${sourceArchive}" && test -f "${sourceArchive}" \
|| { printf "%s: %s: %s\n" "${0##*/}" "${sourceArchive}" "source archive not found" 1>&2; exit 1; }

packageName="xyz"
packageSummary="Check for, and correct, common configuration faults"
packageDescription="\
Check for common configuration faults that could cause sensitive
information or interfaces to be exposed, such as SSH private keys or GPG
secret keys without passphrases, or service accounts without a password."
packageUrl="https://ivarch.com/programs/xyz.shtml"
sourceUrl="https://ivarch.com/programs/sources/${packageName}-%{version}.tar.gz"
packageEmail="Andrew Wood <xyz@ivarch.com>"

workDir="$(mktemp -d)" || exit 1
trap 'rm -rf "${workDir}"' EXIT

# Infer the version from the most recent NEWS entry.
version="$(tar xzf "${sourceArchive}" --wildcards -O '*/docs/NEWS.md' 2>/dev/null | awk '/^### [0-9]/{print $2}' | sed -n 1p)"

if command -v rpmbuild >/dev/null 2>&1; then
	# Build an RPM.
	cat > "${workDir}"/spec <<EOF
Summary: ${packageSummary}
Name: ${packageName}
Version: ${version}
Release: 1%{?dist}
License: GPLv3+
Source: ${sourceUrl}
Url: ${packageUrl}
BuildArch: noarch

%description
${packageDescription}

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
test -n "\${RPM_BUILD_ROOT}" && test "\${RPM_BUILD_ROOT}" != '/' && rm -rf "\${RPM_BUILD_ROOT}"
make install DESTDIR="\${RPM_BUILD_ROOT}"
rm -rf "\${RPM_BUILD_ROOT}/usr/share/doc"

%check
make check

%clean
test -n "\${RPM_BUILD_ROOT}" && test "\${RPM_BUILD_ROOT}" != '/' && rm -rf "\${RPM_BUILD_ROOT}"

%files
%defattr(-, root, root)
%{_sbindir}/*
%{_mandir}/man8/*
%attr(0755, root, root) %{_libexecdir}/xyz/*.sh
%attr(0755, root, root) %dir %{_sysconfdir}/xyz
%attr(0755, root, root) %dir %{_libexecdir}/xyz/extensions
%attr(0755, root, root) %dir %{_libexecdir}/xyz/hooks/begin
%attr(0755, root, root) %dir %{_libexecdir}/xyz/hooks/end

%doc README.md docs/COPYING docs/NEWS.md

%changelog
* $(date '+%a %b %d %Y') ${packageEmail} - ${version}-1
- Package built from main source.
EOF
	mkdir "${workDir}/SOURCES"
	cp "${sourceArchive}" "${workDir}/SOURCES/${packageName}-${version}.tar.gz"
	rpmbuild -D "%_topdir ${workDir}" -bb "${workDir}/spec" || exit 1
	if test -n "${MAINTAINER}"; then
		# Sign the RPMs.
		rpm -D "%_gpg_name ${MAINTAINER}" --addsign "${workDir}/RPMS"/*/*.rpm || exit 1
	fi
	cp -v "${workDir}/RPMS"/*/*.rpm .
fi

if command -v dpkg-buildpackage >/dev/null 2>&1; then
	# Build a Debian package.
	mkdir "${workDir}/build" "${workDir}/build/debian" "${workDir}/build/debian/source"

	tar xzf "${sourceArchive}" -C "${workDir}/build" --strip-components=1

	printf "%s\n" "10" > "${workDir}/build/debian/compat"
	touch "${workDir}/build/debian/copyright"
	printf "%s\n" "3.0 (quilt)" > "${workDir}/build/debian/source/format"

	cat > "${workDir}/build/debian/rules" <<EOF
#!/usr/bin/make -f
export DH_VERBOSE = 1

%:
	dh \$@
EOF
	chmod 700 "${workDir}/build/debian/rules"

	cat > "${workDir}/build/debian/control" <<EOF
Source: ${packageName}
Maintainer: ${packageEmail}
Section: misc
Priority: optional
Standards-Version: 3.9.2
Build-Depends: debhelper (>= 9)

Package: ${packageName}
Architecture: all
Depends: \${misc:Depends}
Description: ${packageSummary}
$(printf "%s\n.\n" "${packageDescription}" | sed 's,^, ,')
EOF

	cat > "${workDir}/build/debian/changelog" <<EOF
${packageName} (${version}-1) UNRELEASED; urgency=low

  * Package built from main source.

 -- ${packageEmail}  $(date -R)
EOF

	if test -n "${MAINTAINER}"; then
		(cd "${workDir}/build" && dpkg-buildpackage --build=binary --force-sign --sign-key="${MAINTAINER}") || exit 1
	else
		(cd "${workDir}/build" && dpkg-buildpackage --build=binary) || exit 1
	fi

	cp -v "${workDir}"/*.deb . || exit 1
fi
