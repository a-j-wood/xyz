.TH XYZ 8 2025-02-12 xyz-0.2.22 "User Commands"
.\"
.SH NAME
xyz \- check for, and correct, common configuration faults
.\"
.SH SYNOPSIS
.B xyz
[\fB-e\fR \fIPATTERN\fR] [\fB-r\fR \fIFILE\fR] [\fB-d\fR \fIDELAY\fR]
\fBcheck\fR \fBall\fR|\fIPATTERN...\fR
.br
\fBxyz\fR [\fB-e\fR \fIPATTERN\fR] [\fB-p\fR|\fB-n\fR|\fB-y\fR]
\fBfix\fR \fBall\fR|\fIPATTERN...\fR
.br
\fBxyz\fR \fBchecks\fR
.br
\fBxyz\fR \fBignore\fR \fIITEM\fR \fIREASON\fR
.br
\fBxyz\fR \fBreinstate\fR \fIITEM...\fR
.br
\fBxyz\fR \fBignored\fR
.br
\fBxyz\fR \fBexception\-add\fR \fIITEM\fR \fIVALUE\fR
.br
\fBxyz\fR \fBexception\-remove\fR \fIITEM\fR \fIVALUE\fR
.br
\fBxyz\fR \fBexceptions\fR [\fIITEM\fR]
.br
\fBxyz\fR \fBunavailable\fR
.br
\fBxyz\fR [\fB\-c\fR|\fB\-j\fR] \fBreformat\fR
.br
\fBxyz\fR [\fB\-j\fR] \fBinfo\fR
.br
\fBxyz\fR \fB\-\-help\fR
.br
\fBxyz\fR \fB\-\-version\fR
.\"
.SH DESCRIPTION
Check for common configuration faults that could cause sensitive information
or interfaces to be exposed, such as SSH private keys or GPG secret keys
without passphrases, or service accounts without a password.
For some types of fault, \fBxyz\fR may offer an automated fix.
.PP
Many of the 150+ checks are derived from recommendations in
.UR https://www.cisecurity.org/
Center for Internet Security
.UE
benchmark documents.
Run \*(lq\fIxyz\~checks\~|\~grep\~\-B\~3\~'Derived\~from:'\fR\*(rq for
details.
Note that \fBxyz\fR is intended for use on multiple operating systems \(en
various GNU/Linux distributions, FreeBSD, and OpenBSD \(en which means that
these checks are \fIderived from\fR standard recommendations rather than
\fIequivalent to\fR them.
\fBxyz\fR is not associated with or endorsed by CIS or any other
organisation.
.PP
Use \fBxyz\fR as a risk reduction tool to limit accidental exposure: run it
first \fIbefore\fR a server is placed into a production environment, and
then run it regularly thereafter to check that mistakes have not crept in
during day-to-day maintenance.
It does not look for indicators of compromise and should not be run after an
incident.
A compromised server should be deleted and rebuilt, and \fBxyz\fR run on the
rebuilt system as part of hardening before deployment.
.PP
User-defined check and fix actions can be added \(en see the
\fBEXTENSIONS\fR section for details.
By packaging and deploying your own check, fix, and hook functions,
embodying your estate's configuration policies, \fBxyz\fR can be extended to
serve as a configuration policy compliance tool.
For example, regular checks could be run to ensure that configuration
changes made by other tools such as Ansible or Puppet have had the desired
effect and have not introduced regressions.
.PP
Operators will commonly use the \*(lq\fBignore\fR\*(rq action to prevent
checks from reporting about faults that are a consequence of deliberate
decisions.
To allow future operators to understand what those decisions were, a
human-readable reason must be given to each \*(lq\fBignore\fR\*(rq
directive.
These reasons can be reported by \*(lq\fIxyz\~ignored\fR\*(rq.
For example, when building a web server, the operator should silence
warnings about port 80 being reachable:
.PP
.in +4n
.EX
xyz ignore net_portreachable_80 This is a web server.
.EE
.in
.PP
The reasons can then be re-assessed in future \(en for example in future
this server may no longer provide web services, and at that point,
\*(lq\fIxyz\~reinstate\fR\*(rq can be used to stop ignoring the relevant
checks.
.PP
Fault reports from the \*(lq\fBcheck\fR\*(rq action are intended to be
machine-readable, so they can be consumed by an endpoint management system
or a shell script.
To make them easier to read, pipe them through
\*(lq\fIxyz\~reformat\fR\*(rq.
.\"
.SH OPTIONS
Options must be specified before the actions and arguments.
.TP
\fB\-e\fR, \fB\-\-exclude\fR \fIPATTERN\fR
For \*(lq\fBcheck\fR\*(rq and \*(lq\fBfix\fR\*(rq actions, exclude items
matching \fIPATTERN\fR.
This option can be specified more than once.
.TP
\fB\-r\fR, \fB\-\-report\fR \fIFILE\fR
For the \*(lq\fBcheck\fR\*(rq action, instead of writing a report to
standard output, atomically replace \fIFILE\fR with the report, so that a
monitoring tool such as Zabbix can be used to raise an alarm if the file is
not empty.
.TP
\fB\-u\fR, \fB\-\-until\fR \fISTAMP\fR
For the \*(lq\fBignore\fR\*(rq and \*(lq\fBexception\-add\fR\*(rq actions,
instead of ignoring the item or applying the exception forever, make it
expire at the date and time given by \fISTAMP\fR.
The \fISTAMP\fR must be in ISO 8601 format without a time zone designator,
so it is expressed in the server's local time zone.
The time may be omitted.
For example, \fI2024-10-26T22:58:03\fR and \fI2024-10-26\fR are both valid.
When the time is omitted, \fIT00:00:00\fR is implied (the start of the day).
.TP
\fB\-d\fR, \fB\-\-delay\fR \fIDELAY\fR
For the \*(lq\fBcheck\fR\*(rq action, wait a random number of seconds up to
\fIDELAY\fR before starting the checks.
This can be used when running reports from a scheduler to make it less
likely that all servers will run the report at the exact same moment.
.TP
\fB\-p\fR, \fB\-\-prompt\fR
For the \*(lq\fBfix\fR\*(rq action, prompt for confirmation before fixing
higher-risk items.
This is the default if standard input is a terminal.
.TP
.BR \-n ", " \-\-no
For the \*(lq\fBfix\fR\*(rq action, do not fix higher-risk items, and do not
prompt.
This is the default if standard input is not a terminal.
.TP
.BR \-y ", " \-\-yes
For the \*(lq\fBfix\fR\*(rq action, fix higher-risk items without prompting
for confirmation.
.TP
.BR \-c ", " \-\-colour
For the \*(lq\fBreformat\fR\*(rq action, include colour in the output
instead of keeping it plain text.
.TP
.BR \-j ", " \-\-json
For the \*(lq\fBreformat\fR\*(rq action, rewrite the report as JSON data
containing an array of objects, one object per item.
Each object contains strings named
\*(lq\fBcheckItem\fR\*(rq,
\*(lq\fBitemDescription\fR\*(rq,
\*(lq\fBderivedFrom\fR\*(rq,
\*(lq\fBproblem\fR\*(rq,
\*(lq\fBfixTypeFlag\fR\*(rq,
\*(lq\fBfixTypeDescription\fR\*(rq,
and an array of strings named
\*(lq\fBactions\fR\*(rq.
.IP
For the \*(lq\fBinfo\fR\*(rq action, write the information as a single JSON
object containing string values.
.TP
\fB\-R\fR, \fB\-\-root\fR \fIDIR\fR
For the purposes of checks, behave as if \fIDIR\fR was the root directory.
This can be used to run tests on a representative copy of parts of a system.
Note that not all fixes may honour \fIDIR\fR, and so this option is
recommended only for use with the \*(lq\fBcheck\fR\*(rq action.
.TP
\fB\-C\fR, \fB\-\-component\-dir\fR \fIDIR\fR
Look for the component parts of \fBxyz\fR under \fIDIR\fR instead of the
system default directory.
Not generally used in production.
.TP
\fB\-E\fR, \fB\-\-extension\-dir\fR \fIDIR\fR
Look for the user extensions to \fBxyz\fR under \fIDIR\fR instead of the
system default directory.
Not generally used in production.
.TP
\fB\-H\fR, \fB\-\-hook\-dir\fR \fIDIR\fR
Look for the user \*(lq\fBbegin\fR\*(rq and \*(lq\fBend\fR\*(rq hook scripts
under \fIDIR\fR instead of the system default directory.
Not generally used in production.
.TP
.BR \-h ", " \-\-help
Display a usage message on standard output and exit successfully.
.TP
.BR \-V ", " \-\-version         
Display version information on standard output and exit successfully.
.\"
.SH ACTIONS
.TP
\fBcheck\fR \fIPATTERN...\fR
Run the checks matching the supplied glob patterns, in alphanumeric order. 
Any faults found are reported on standard output (or written to a file with
\*(lq\fB\-\-report\fR\*(rq) in the format described below.
The exit status will be the number of faults found, capped at 124.
The special pattern \*(lq\fBall\fR\*(rq can be used to specify that all
checks should be run.
.TP
\fBfix\fR \fIPATTERN...\fR
Run the fixes matching the supplied glob patterns in alphanumeric order. 
For each fix, the check is automatically run first and the fix will only be
executed if the check finds a fault that needs to be fixed.
Nothing will be output, and the exit status will be zero, unless there are
errors.
As with the \*(lq\fBcheck\fR\*(rq action, the special pattern
\*(lq\fBall\fR\*(rq indicates that all possible fixes should be run.
.IP
Higher-risk fixes will be skipped if standard input is not a terminal and
\*(lq\fB\-\-yes\fR\*(rq was not passed, or if the operator does not answer
the prompt with a word starting with \*(lq\fBy\fR\*(rq.
.TP
.B checks
List all available checks (not counting any ignored items).
.TP
\fBignore\fR \fIITEM\fR \fIREASON\fR
Store a marker for \fIITEM\fR which causes future \*(lq\fBcheck\fR\*(rq and
\*(lq\fBfix\fR\*(rq actions to skip it.
The \fIREASON\fR is a human-readable sentence describing why this \fIITEM\fR
is being ignored.
.TP
\fBreinstate\fR \fIITEM...\fR
Remove the \*(lq\fBignore\fR\*(rq marker for each specified \fIITEM\fR so
that these items will be included in \*(lq\fBcheck\fR\*(rq and
\*(lq\fBfix\fR\*(rq actions once more.
.TP
.B ignored
List all ignored check/fix items, including their expiry time, if any.
.TP
\fBexception-add\fR \fIITEM\fR \fIVALUE\fR
Add an exception which causes the \fIITEM\fR check to not alert on a value
of \fIVALUE\fR, so that it stops being flagged as a fault.
This is only intended for checks that may match multiple paths or users.
For example, if the \fBri_privkey_ssh_identity_unencrypted\fR check reports
that \fBroot\fR has an unencrypted private key but you accept the risk
because this server needs to be able to transfer files unattended,
\*(lq\fIxyz\~exception\-add\~ri_privkey_ssh_identity_unencrypted\~/root/.ssh/id_dsa\fR\*(rq
will stop the \*(lq\fBcheck\fR\*(rq action from alerting about that
particular file.
Compare this with the \*(lq\fBignore\fR\*(rq action, which stops a check
from running at all.
.TP
\fBexception-remove\fR \fIITEM\fR \fIVALUE\fR
Remove the exception that was allowing \fIVALUE\fR to be excluded from fault
conditions for \fIITEM\fR.
This is the reverse of the action above.
.TP
\fBexceptions\fR [\fIITEM\fR]
List the exceptions for all items, or for just the specified \fIITEM\fR,
including their expiry time, if any.
.TP
.B unavailable
List all check/fix items which are unavailable due to missing prerequisites.
.TP
.B reformat
Read a fault report from the \*(lq\fBcheck\fR\*(rq action on standard input,
and produce a version on standard output that is easier to read.
.TP
.B info
Show information about the current system, as shell variable assignments.
This can be a useful reference when developing extensions - see the
\*(lq\fBVariables available at load time\fR\*(rq sub-section under
\*(lq\fBEXTENSIONS\fR\*(rq.
.\"
.SS "Fault report format"
.\"
The \*(lq\fBcheck\fR\*(rq action will write one report line per fault found,
of the form \*(lq\fBFLAG ITEM PROBLEM ACTIONS\fR\*(rq.
Each line starts with a single-letter flag, then a tab, the name of the
check item, a tab, a human-readable problem description, another tab, and a
description of the actions required to fix this fault; if there is more than
one action required for a single fault, they will be on one line, separated
by "|" characters.
.PP
The report flags are:
.TP 5
.B a
This fault can be corrected \fBa\fRutomatically with the \*(lq\fBfix\fR\*(rq
action.
.TP
.B R
This fault can be corrected automatically with the \*(lq\fBfix\fR\*(rq
action, but may be considered \fBr\fRisky or could interrupt service.
Each item marked this way will prompt for confirmation when the
\*(lq\fBfix\fR\*(rq action is run, unless the \*(lq\fB\-\-yes\fR\*(rq option
is passed.
.TP
.B m
This fault must be corrected \fBm\fRanually.
.PP
If there are no faults, \*(lq\fIxyz\~check\fR\*(rq does not produce any
output.
.PP
Pipe a report through \*(lq\fIxyz\~reformat\fR\*(rq to make it easier to
read.
.\"
.SH CHECK ITEMS
The following checks are provided by default.
.PP
Items prefixed with \*(lq\fBri_\fR\*(rq are considered to be more resource
intensive, so they may impact system performance if run too often (e.g. run
them daily rather than hourly).
.PP
Not all items may be available on all systems.
Use \*(lq\fIxyz\~unavailable\fR\*(rq to show which items are unavailable and
why.
.PP
Each item in this list is suffixed with the possible flags indicating the
types of fix available (see above).
.TP
.BR ri_privkey_ssh_identity_exposed " (m)"
Check all home directories for SSH private keys which have no passphrase
and which may be readable to other non-root users.
.TP
.BR ri_privkey_ssh_identity_unencrypted " (m)"
Check all home directories for SSH private keys which have no passphrase,
regardless of their file permissions.
.TP
.BR ri_privkey_gpg_secret_key_unencrypted " (m)"
Check all home directories for GPG secret keys which have no passphrase.
.TP
.BR ri_acct_user_home_dangerousdotfiles " (R)"
Check that all non-system users do not have \fI.forward\fR, \fI.rhosts\fR,
or \fI.netrc\fR files in their home directories.
.TP
.BR ri_acct_user_home_dotfilepermissions " (R)"
Check that the hidden files (starting with ".") of all non-system users have
the correct ownership and permissions - owned by their user's primary UID
and GID, not writable by group or other, and for \fI.bash_history\fR or
\fI.netrc\fR, not readable by group or other.
.TP
.BR ri_acct_user_home_exists " (m)"
Check that all non-system users have a home directory that exists.
.TP
.BR ri_acct_user_home_globalwrite " (R)"
Check that all non-system users have a home directory that does not have
global write permission.
.TP
.BR ri_acct_user_home_groupwrite " (R)"
Check that all non-system users have a home directory that does not have
group write permission.
.TP
.BR ri_acct_user_home_owner " (R)"
Check that all non-system users have a home directory that they own.
.TP
.BR acct_shadowed " (m)"
Check that all local accounts are shadowed, meaning that their password
hash is not stored in \fI/etc/passwd\fR.
.TP
.BR acct_uid_0 " (m)"
Check that only one account has UID 0 (or two, on FreeBSD where the
\fBtoor\fR account also exists).
.TP
.BR acct_user_with_gid_0 " (m)"
Check that only one account has GID 0 (or two, on FreeBSD where the
\fBtoor\fR account also exists).
.TP
.BR acct_gid_0 " (m)"
Check that only one group has GID 0.
.TP
.BR acct_sys_static_locked " (R)"
On systems where \fI/etc/login.defs\fR has the concept of \fBSYS_UID_MIN\fR,
check that all local accounts other than \fBroot\fR with a UID less than
\fBSYS_UID_MIN\fR have locked passwords and so cannot use password
authentication.
This typically includes accounts such as \fBdaemon\fR, \fBbin\fR, \fBlp\fR,
and so on.
.TP
.BR acct_sys_dynamic_locked " (R)"
On systems where \fI/etc/login.defs\fR has the concept of \fBSYS_UID_MAX\fR,
check that all local accounts whose UID is between \fBSYS_UID_MIN\fR and
\fBSYS_UID_MAX\fR have locked passwords and so cannot use password
authentication.
This typically includes accounts such as \fBapache\fR, \fBsshd\fR,
\fB_rpc\fR, and so on.
.TP
.BR acct_guest_locked " (R)"
If guest accounts (\fBguest\fR, \fBpcguest\fR) exist, check they have locked
passwords and so cannot use password authentication.
.TP
.BR acct_root_password_not_empty " (m)"
Check that the \fBroot\fR account has a password or is locked.
.TP
.BR acct_passwords_not_empty " (m)"
Check that no accounts have empty passwords.
.TP
.BR acct_passwd_groups_defined " (m)"
Check that all accounts have a group ID which is defined in
\fI/etc/group\fR.
.TP
.BR acct_shadow_group_empty " (m)"
On systems with a \*(lq\fBshadow\fR\*(rq group, check that it has no
members, and that no accounts have \fBshadow\fR as their primary group.
.TP
.BR acct_passwd_duplicate_uid " (m)"
Check that \fI/etc/passwd\fR does not contain the same UID twice (ignoring
UID 0, which is covered by \fBacct_uid_0\fR).
.TP
.BR acct_group_duplicate_gid " (m)"
Check that \fI/etc/group\fR does not contain the same GID twice (ignoring
GID 0, which is covered by \fBacct_gid_0\fR).
.TP
.BR acct_passwd_duplicate_name " (m)"
Check that \fI/etc/passwd\fR does not contain the same account name twice.
.TP
.BR acct_group_duplicate_name " (m)"
Check that \fI/etc/group\fR does not contain the same group name twice.
.TP
.BR fs_DIR_ownvolume " (m)"
Check that directory \fIDIR\fR is on its own volume, not part of another
filesystem, where \fIDIR\fR is one of \fI/tmp\fR, \fI/dev/shm\fR,
\fI/home\fR, \fI/var\fR, \fI/var/tmp\fR, \fI/var/log\fR, or
\fI/var/log/audit\fR.
In the item name, \fI/\fR characters are omitted - for example,
\fBfs_vartmp_ownvolume\fR.
.TP
.BR fs_DIR_nodev " (m)"
Check that directory \fIDIR\fR, if it is on its own volume, is mounted with
the \*(lq\fBnodev\fR\*(rq mount option.
Repeated for each \fIDIR\fR listed above.
.TP
.BR fs_DIR_nosuid " (m)"
Check that directory \fIDIR\fR, if it is on its own volume, is mounted with
the \*(lq\fBnosuid\fR\*(rq mount option.
Repeated for each \fIDIR\fR listed above.
.TP
.BR fs_DIR_noexec " (m)"
Check that directory \fIDIR\fR, if it is on its own volume, is mounted with
the \*(lq\fBnoexec\fR\*(rq mount option.
Repeated for each \fIDIR\fR listed above except for \fI/home\fR and
\fI/var\fR.
.TP
.BR fs_path_integrity " (m)"
Check that the search path described by the \fI$PATH\fR environment variable
contains only components which are not empty, not "\fI.\fR" or "\fI..\fR",
and which are directories owned by root or the current user, to which only
the owner has write permission.
.TP
.BR infoleak_content_FILE " (m)"
Check that file \fIFILE\fR under \fI/etc\fR does not leak information about
the system by mentioning the operating system name or by using any
\fBgetty\fR(8) style escape sequences that expand to system name, release,
and so on.
The \fIFILE\fR can be one of \fImotd\fR, \fIissue\fR, or \fIissue.net\fR. 
In the item name, "." characters are omitted.
.TP
.BR infoleak_owner_FILE " (R)"
Check that file \fIFILE\fR under \fI/etc\fR is owned by the correct user so
it cannot easily be modified to mislead other users.
Repeated for each \fIFILE\fR listed above.
.TP
.BR infoleak_permissions_FILE " (R)"
Check that file \fIFILE\fR under \fI/etc\fR is only writable or executable
by its owner, and nobody else, so it cannot easily be modified to mislead
other users.
Repeated for each \fIFILE\fR listed above.
.TP
.BR lkm_available_NAME " (R)"
Check whether the Linux kernel module \fINAME\fR is available to load with
\fBmodprobe\fR(8).
The modules checked are
.IR cramfs ,
.IR freevxfs ,
.IR hfs ,
.IR hfsplus ,
.IR jffs2 ,
.IR squashfs ,
.IR udf ,
and \fIusb-storage\fR.
In the \fINAME\fR, dashes are replaced with underscores.
.TP
.BR lkm_loaded_NAME " (R)"
Check whether the Linux kernel module \fINAME\fR is currently loaded.
The modules checked are the same as above.
.TP
.BR net_portreachable_PORT " (m)"
Check whether network port \fIPORT\fR is listening on anything other than a
local interface, meaning that it is reachable from elsewhere on the network.
This does not take any host-based firewalls into account.
Use the \*(lq\fBignore\fR\*(rq action to exclude specific ports on systems
which are deliberately set up to provide services on those ports.
This check is useful to highlight services that have been accidentally left
switched on, or cases such as local mail delivery where a service should
normally listen only locally.
The following ports are checked:
21, 23, 25, 53, 69, 79, 80, 110, 113, 119, 143, 443, 513, 563, 990, 992,
993, 995, 1080, 1194, 2049, 3128, 3306, 5432, 6000, 6001, 6002, 6003, 6004,
6005, 6006, 6007, 6379, 6667, 6697, 7100, 8080, 9418, 10000.
.TP
.BR proc_deleted_exe " (m)"
Check whether any running processes are executing from a file which has
since been deleted - such as when a service is not restarted after applying
an update.
If no updates have recently been applied, this can be an indicator of
suspicious activity.
.TP
.BR proc_deleted_libs " (m)"
Check whether any running processes have executable memory areas which are
mapped to deleted files (executables or libraries), such as when a system
library is updated but the services using it have not been restarted.
This will typically overlap with \fBproc_deleted_exe\fR.
If no updates have recently been applied, this can be an indicator of
suspicious activity.
However, on some systems it can also give false positives, such as with
Python based daemons like \fBfirewalld\fR(1) and \fBtuned\fR(8) on CentOS 7.
.TP
.BR proc_exec_memfd " (m)"
Check whether any running processes have executable memory areas which are
mapped to anonymous files ("memfd").
This is usually an indicator of suspicious activity, though again some
daemons may cause false positives, like \fBfirewalld\fR(1) on AlmaLinux 9.
.TP
.BR rights_PATH_owning_group " (a)"
Check that \fIPATH\fR is owned by the correct group (usually \fBroot\fR or
\fBwheel\fR).
The \fIPATH\fR is one of
.IR /tmp ,
.IR /etc/crontab ,
.IR /etc/cron.hourly ,
.IR /etc/cron.daily ,
.IR /etc/cron.weekly ,
.IR /etc/cron.monthly ,
.IR /etc/cron.d ,
.IR /etc/passwd ,
.IR /etc/passwd- ,
.IR /etc/group ,
.IR /etc/group- ,
.IR /etc/shadow ,
.IR /etc/shadow- ,
.IR /etc/gshadow ,
.IR /etc/gshadow- ,
.IR /etc/shells ,
or \fI/etc/security/opasswd\fR.
In the item name, \fI/\fR and \fI.\fR characters are omitted, and \fI\-\fR
is replaced with "dash".
The pseudo paths \*(lq\fIauditd_config\fR\*(rq,
\*(lq\fIauditd_logdir\fR\*(rq, and \*(lq\fIauditd_logfile\fR\*(rq are also
included, covering the audit daemon's configuration files, log directory,
and configuration files.
.TP
.BR rights_PATH_owning_user " (a)"
Check that \fIPATH\fR is owned by the correct user (usually \fBroot\fR).
This is repeated for each \fIPATH\fR listed above.
.TP
.BR rights_PATH_permissions " (a)"
Check that \fIPATH\fR has the correct file permissions, such as 600 for
\fI/etc/crontab\fR or 1777 for \fI/tmp\fR.
This is repeated for each \fIPATH\fR listed above.
.TP
.BR rights_sshd_config " (R)"
Check that the SSH daemon configuration files have the correct ownership and
file permissions - generally unreadable by anyone other than \fBroot\fR.
.TP
.BR rights_sshd_hostkeys_KEYTYPE_CHECK " (R)"
Check that the SSH host keys have the correct ownership and file
permissions, where \fIKEYTYPE\fR is either "public" or "private", and
\fICHECK\fR is one of "owning_user", "owning_group", or "permissions".
.TP
.BR sched_SCHEDULER_generally_permitted " (a)"
For a \fISCHEDULER\fR of "at" or "cron", check whether the system is
configured such that a non-root user would generally be allowed to use it.
.TP
.BR sched_root_JOBTYPE " (R)"
For a \fIJOBTYPE\fR of "atjobs" or "crontab", check whether the root user
has any scheduled jobs defined.
For a crontab, this refers to the crontab set with the \fBcrontab\fR(1)
command, not to files placed in \fI/etc\fR.
.TP
.BR sched_user_JOBTYPE " (R)"
For a \fIJOBTYPE\fR of "atjobs" or "crontabs", check whether any non-root
users have this type of scheduled job defined.
.\"
.SH "EXIT STATUS"
.TP
.B 0
No faults found (with \*(lq\fBcheck\fR\*(rq) or, for all other actions, no
errors occurred.
.TP
.B 1-124
At least one fault was found when running the \*(lq\fBcheck\fR\*(rq action.
.TP
.B 125
An error occurred.
.TP
.B 126
The arguments were not accepted - an unknown option, action, or item was
specified.
.PP
Note that specifying an item that is defined, but is currently ignored or
unavailable, is not treated as an error - the item will just be skipped.
.\"
.SH FILES
.TP
.I /etc/xyz/*.exception
Exceptions for each item.
.TP
.I /etc/xyz/*.expiring-exception
Exceptions for each item, which have an expiry time.
.TP
.I /etc/xyz/*.ignore
Marker files indicating that specific check/fix items should be ignored on
this system.
.TP
.I /etc/xyz/*.expiring-ignore
As above, but with an expiry time.
.TP
.I /usr/libexec/xyz/extensions/*.sh
Scripts defining new check and fix items.
.TP
.I /usr/libexec/xyz/hooks/begin/*.sh
Scripts to run before an action is performed.
.TP
.I /usr/libexec/xyz/hooks/end/*.sh
Scripts to run after an action is completed.
.\"
.SH EXAMPLES
Run this daily to generate a report file containing the results of
resource-intensive checks, which will be empty if there are no
problems:
.PP
.in +4n
.EX
xyz --report /var/spool/xyz-daily.txt --delay 90 check "ri_*"
.EE
.in
.PP
Run this hourly to generate a different report file containing the results
of all other checks, which will be empty if there are no problems:
.PP
.in +4n
.EX
xyz --exclude "ri_*" --report /var/spool/xyz-hourly.txt --delay 90 check all
.EE
.in
.PP
You could then use a monitoring system like Zabbix to raise an alert if
either file ever has a non-zero size.
.PP
Both of these will begin after a random delay of up to 90 seconds, to avoid
causing load spikes on multi-guest VM hosts.
.PP
Alternatively, run something like this to email the daily file to root:
.PP
.in +4n
.EX
xyz --report /var/spool/xyz-daily.txt --delay 90 check "ri_*"
test -s /var/spool/xyz-daily.txt \\
&& xyz reformat < /var/spool/xyz-daily.txt \\
| mail -s "Security faults detected on $HOSTNAME" root
.EE
.in
.PP
For hourly checks, email is not recommended.
Polling the size of the report with a monitoring agent like Zabbix is more
flexible and scales better.
.\"
.SH EXTENSIONS
New checks and fixes can be defined by placing shell scripts in the
extensions directory, which is usually \fI/usr/libexec/xyz/extensions/\fR.
.PP
All files ending in \*(lq\fI.sh\fR\*(rq will be loaded by \fBxyz\fR as shell
script sources; these files are expected to define functions named
\fBcheck_ITEM\fR and \fBfix_ITEM\fR, which perform the check and fix actions
for an item \fIITEM\fR.
If there is no possible fix for an item, its \fBfix_\fR function does not
need to be defined.
.PP
Each file must call the \fBregisterItem\fR function for every item it
defines - see the \*(lq\fBFunctions available at load time\fR\*(rq
sub-section below.
.PP
Items are always processed in alphanumeric order, so bear this in mind when
naming new items.
.PP
All extension scripts must be written in Bourne shell (not bash, ksh, csh,
etc).
.PP
Checks should prefix all paths referring to system files and directories
with \fB${rootDir}\fR, such as \*(lq\fB${rootDir}/etc/passwd\fR\*(rq.
.PP
Extensions can be included in standardised configuration deployed to all
systems using whatever management mechanism is appropriate for your estate.
For example you could drop your local extensions in there via Ansible, or
package them into your own RPM.
.PP
Each \fBcheck\fR function must output nothing, and must return one of these
values:
.RS 2
.TP
.B ${XYZ_RC_NOFAULT} (0)
No fault found.
.TP
.B ${XYZ_RC_FAULT_LOWRISK} (1)
A fault was found with a low-risk automatic fix ("a").
.TP
.B ${XYZ_RC_FAULT_HIGHRISK} (2)
A fault was found with a high-risk automatic fix ("R").
.TP
.B ${XYZ_RC_FAULT_NOFIX} (3)
A fault was found for which there is no automatic fix ("m").
.RE
.PP
When the function returns with a non-zero value, it must have set these
variables:
.RS 2
.TP
.B faultDescription
A human-readable description of the problem.
.TP
.B fixActions
A list of actions required to fix the problem, separated by "|" or by
newlines.
.RE
.PP
Each \fBfix\fR function should return 0 on success (and output nothing), or
any non-zero value on error (and report the error with \fBreportError\fR).
.\"
.SS "Functions available at load time"
Extensions can call the following functions at any time.
.TP
\fBregisterItem\fR \fIITEM\fR \fIDESCRIPTION\fR \fIDERIVEDFROM\fR [\fIPREREQUISITE...\fR]
Register a check item.
.IP
The \fIITEM\fR is the item name (so you must also define a function named
\fBcheck_ITEM\fR, and optionally, also define a function named
\fBfix_ITEM\fR for the associated automated fix).
.IP
The \fIDESCRIPTION\fR is a short description of this check.
.IP
If the item was derived from anything, such as being related to any
standards, describe them in \fIDERIVEDFROM\fR, otherwise this string should
be empty.
.IP
If the item's check or fix requires any specific commands, each should be
given as a \fIPREREQUISITE\fR.
If any of the prerequisites are not discoverable with
\*(lq\fBcommand\~\-v\fR\*(rq then the item will be treated as unavailable
and will be shown in the \*(lq\fBxyz\~unavailable\fR\*(rq list.
.IP
If a \fIPREREQUISITE\fR starts with "?", it is executed and if false (exit
non-zero), the prerequisite is not met - for example,
\*(lq\fB?test\~\-d\~/proc\fR\*(rq to require the existence of \fI/proc\fR.
.TP
\fBreportError\fR \fIMESSAGE\fR
Write \fIMESSAGE\fR to the standard error stream, prefixed with the program
name and a colon.
.\"
.SS "Variables available at load time"
Extensions can read the following shell variables at any time.
.TP
.B rootDir
The directory to prefix all system paths (like \fI/etc/passwd\fR) with.
This variable is usually empty unless the \fB\-\-root\fR option was passed.
.TP
.B workDir
The name of a temporary directory which will be deleted when \fBxyz\fR
exits, to be used for scratch space.
.TP
.B kernelName
The result of \*(lq\fBuname\~\-s\fR\*(rq, converted to lower case.
.TP
.B kernelRelease
The result of \*(lq\fBuname\~\-r\fR\*(rq, converted to lower case.
.TP
.B osId
A lower-case string indicating the operating system type, such as "linux",
"debian", "almalinux", "openbsd", "freebsd", taken from \fBID\fR in
\fI/etc/os-release\fR if possible, other files if appropriate, or falling
back to \*(lq\fBuname\~\-s\fR\*(rq.
.TP
.B osVersion
A lower-case string indicating the operating system version as a number,
such as "15.3" or "9", taken from \fBVERSION_ID\fR in \fI/etc/os-release\fR
if possible, other files if appropriate, or falling back to
\*(lq\fBuname\~\-r\fR\*(rq (stopping at the first \*(lq\-\*(rq).
.TP
.B osIsRhelDescendant
The string "no" or "yes" depending on whether this operating system appears
to be a Linux distribution equivalent to, or descended from, Red Hat
Enterprise Linux.
.TP
.B statType
The string "bsd" or "gnu" depending on whether the \fBstat\fR(1) command
uses the BSD syntax or the GNU syntax.
.TP
.B startStamp
The date and time the script started running (after any initial delay), in
ISO 8601 format without a time zone designator.
.TP
.B XYZ_RC_NOFAULT
The return code to use in a check function if no fault was found (0).
.TP
.B XYZ_RC_FAULT_LOWRISK
The return code to use in a check function if a fault was found for which
there is a low-risk automatic fix (1, meaning "a").
.TP
.B XYZ_RC_FAULT_HIGHRISK
The return code to use in a check function if a fault was found for which
there is a high-risk automatic fix (2, meaning "R").
.TP
.B XYZ_RC_FAULT_NOFIX
The return code to use in a check function if a fault was found for which
there is no automatic fix (3, meaning "m").
.TP
.B action
The selected action.
.TP
.B selectedItems
A space-separated list of the items applicable to the action, after
expanding patterns in the command line arguments and excluding ignored or
unavailable items.
This is not available in the "begin" hook.
.TP
.B reportFile
The filename to write a \*(lq\fBcheck\fR\*(rq report to, or empty if writing
to standard output.
.TP
.B excludePattern
A newline-separated list of glob patterns - any items matching these
patterns will be ignored.
.TP
.B runHighRiskFixes
Whether to run high-risk fixes - one of "prompt", "yes", or "no".
.\"
.SS "Functions available at run time"
Extensions can call the following functions after loading is complete.
This means that they should only be called from inside check or fix
functions, or in an \*(lq\fBend\fR\*(rq hook.
The functions listed in the previous
\*(lq\fBFunctions available at load time\fR\*(rq sub-section are also still
available at run time.
.TP
\fBstripExceptions\fR \fIFILE\fR
Remove any exceptions for the current item, that had been added earlier with
the \*(lq\fBexception-add\fR\*(rq action, from the file \fIFILE\fR.
This should be called in check functions between generating a list of
possible faults and acting on that list.
.TP
\fBfsOwnerUid\fR \fIPATH\fR
Output the owning user ID of \fIPATH\fR, or nothing on error.
.TP
\fBfsOwnerAccountName\fR \fIPATH\fR
Output the owning user name of \fIPATH\fR, or nothing on error.
.TP
\fBfsOwnerGid\fR \fIPATH\fR
Output the owning group ID of \fIPATH\fR, or nothing on error.
.TP
\fBfsOwnerGroupName\fR \fIPATH\fR
Output the owning group name of \fIPATH\fR, or nothing on error.
.TP
\fBfsFileMode\fR \fIPATH\fR
Output the last 4 digits of the octal file mode of \fIPATH\fR, or nothing on
error.
.TP
\fBfsOtherCanRead\fR \fIPATH\fR
Return true (0) if \fIPATH\fR is readable to other, i.e. the last octet of
the file mode is at least 4.
.TP
\fBfsOtherCanWrite\fR \fIPATH\fR
Return true (0) if \fIPATH\fR is writable by other, i.e. the last octet of
the file mode is at least 2.
.TP
\fBfsGroupOrOtherCanRead\fR \fIPATH\fR
Return true (0) if \fIPATH\fR is readable to either group or other, i.e.
either of the last 2 octets of the file mode are at least 4.
.TP
\fBfsGroupOrOtherCanWrite\fR \fIPATH\fR
Return true (0) if \fIPATH\fR is writable by either group or other, i.e.
either of the last 2 octets of the file mode are at least 2.
.TP
\fBfsGroupAndOtherCanRead\fR \fIPATH\fR
Return true (0) if \fIPATH\fR is readable to both group and other, i.e. the
last 2 octets of the file mode are at least 44.
.TP
\fBfsGroupAndOtherCanWrite\fR \fIPATH\fR
Return true (0) if \fIPATH\fR is writable by both group and other, i.e. the
last 2 octets of the file mode are at least 22.
.TP
\fBfsGroupOrOtherCanDoMoreThanRead\fR \fIPATH\fR
Return true (0) if \fIPATH\fR can be written to or executed by either group
or other.
.TP
\fBfsHasExecutePermissions\fR \fIPATH\fR
Return true (0) if \fIPATH\fR has any executable bits set in its file mode
(user, group, or other).
.TP
\fBfsIsMountPoint\fR \fIPATH\fR
Return true (0) if \fIPATH\fR exists, is a directory, and is a mount point.
.TP
\fBfsMountOptions\fR \fIPATH\fR
Output the mount options that \fIPATH\fR is mounted with (such as "nosuid"),
one per line.
.TP
\fBacctIsAccountPasswordLocked\fR \fIUSERNAME\fR
Return true (0) if the user account named \fIUSERNAME\fR exists and its
password is locked.
.TP
\fBacctLockAccountPassword\fR \fIUSERNAME\fR
Lock the password of the user account named \fIUSERNAME\fR.
.TP
\fBacctIsAccountPasswordEmpty\fR \fIUSERNAME\fR
Return true (0) if the user account named \fIUSERNAME\fR is not locked, and
has an empty password.
.TP
\fBacctStaticSystemAccounts\fR
Output a list of static system accounts (built-in accounts) other than
\fBroot\fR, such as
.BR daemon ,
.BR bin ,
.BR sys ,
.BR nobody ,
and so on.
.TP
\fBacctDynamicSystemAccounts\fR
Output a list of dynamic system accounts (service accounts for system
applications), such as
.BR messagebus ,
.BR _rpc ,
.BR apache ,
and so on.
.TP
\fBacctUserAccounts\fR
Output a list of user accounts - that is, accounts for normal users, not
\fBroot\fR or any of the accounts listed by the above functions.
.TP
\fBnetListListeningSockets\fR
Output a list of listening TCP and UDP sockets, both IPv4 and IPv6, in the
format common to
.BR ss (8),
.BR netstat (8),
and
.BR sockstat (1)
\- the local listening address, a colon, and the port number, such as
"0.0.0.0:993" or "*:443" or "[::1]:53".
.\"
.SS "Hooks"
User-defined hooks can be placed in the hooks directory, which is usually
\fI/usr/libexec/xyz/hooks/\fR.
.PP
Depending on your requirements, you can use the hooks mechanism to set
variables and define functions that your extension items can use.
.PP
Underneath the hooks directory, the following subdirectories are present:
.RS 2
.TP
.B begin
Contains code to be run just before any actions start, after the above
variables have been set up and the arguments have been parsed.
.TP
.B end
Contains code to be run after all actions complete, just before the
temporary working directory is removed and \fBxyz\fR exits.
.RE
.PP
Within those subdirectories, every file ending in \*(lq\fI.sh\fR\*(rq will
be sourced in alphanumeric order.
.PP
Functions and variables defined in the \fBbegin\fR hook files will be
available to the rest of \fBxyz\fR after they are loaded.
.PP
The scripts must not produce any output and must be written in Bourne shell
(not bash, ksh, csh, etc).
.PP
Hooks can be included in standardised configuration deployed to all systems
in the same way that extensions can be.
.\"
.SH "REPORTING BUGS"
Please report bugs or feature requests via the issue tracker linked from the
.UR https://ivarch.com/programs/xyz.shtml
\fBxyz\fR home page
.UE .
.\"
.SH "SEE ALSO"
.BR ssh-keygen (1),
.BR gpg (1)
.\"
.SH COPYRIGHT
Copyright \(co 2024-2025 Andrew Wood.
.PP
License GPLv3+:
.UR https://www.gnu.org/licenses/gpl-3.0.html
GNU GPL version 3 or later
.UE .
.PP
This is free software: you are free to change and redistribute it.  There is
NO WARRANTY, to the extent permitted by law.
