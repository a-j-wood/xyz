#!/bin/sh
#
# Checks and fixes relating to account security.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

registerItem \
  "acct_shadowed" \
  "Check no password hashes are stored in /etc/passwd" \
  "CIS-Debian12-7.2.1"

check_acct_shadowed () {
	awk -F ':' 'length($2)!=1{print $1}' "${rootDir}/etc/passwd" > "${workDir}/accountsWithHash"
	stripExceptions "${workDir}/accountsWithHash"

	test -s "${workDir}/accountsWithHash" || return "${XYZ_RC_NOFAULT}"

	{
	while read -r accountName; do
		printf "%s: %s\n" "correct the password for non-shadowed account" "${accountName}"
	done
	} < "${workDir}/accountsWithHash" > "${workDir}/accountsWithHash.fix"

	faultDescription="At least one local account has a hash in /etc/passwd, or no password at all"
	fixActions="$(cat "${workDir}/accountsWithHash.fix")"

	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "acct_uid_0" \
  "Check that there are not multiple UID 0 accounts" \
  "CIS-Debian12-5.4.2.1"

check_acct_uid_0 () {
	awk -F ':' '$3==0{print $1}' "${rootDir}/etc/passwd" | grep -Fvx 'root' > "${workDir}/acctUid0"
	stripExceptions "${workDir}/acctUid0"
	zeroUidAccounts="$(cat "${workDir}/acctUid0")"
	test "${osId}" = "freebsd" && zeroUidAccounts="$(printf "%s\n" "${zeroUidAccounts}" | grep -Fvx 'toor')"
	test -z "${zeroUidAccounts}" && return "${XYZ_RC_NOFAULT}"
	faultDescription="Multiple accounts detected with UID 0"
	fixActions="remove the extra accounts ($(printf "%s" "${zeroUidAccounts}" | tr '\n' ',' | sed 's/,$//;s/,/, /g'))"
	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "acct_user_with_gid_0" \
  "Check that there are not multiple GID 0 user accounts" \
  "CIS-Debian12-5.4.2.2"

check_acct_user_with_gid_0 () {
	awk -F ':' '$4==0{print $1}' "${rootDir}/etc/passwd" | grep -Fvx 'root' > "${workDir}/acctUserGid0"
	stripExceptions "${workDir}/acctUserGid0"
	# Red Hat derivatives have more GID 0 accounts as standard.
	if test "${osIsRhelDescendant}" = "yes"; then
		grep -Fvx -e 'sync' -e 'shutdown' -e 'halt' -e 'operator' < "${workDir}/acctUserGid0" > "${workDir}/acctUserGid0.upd"
		mv -f "${workDir}/acctUserGid0.upd" "${workDir}/acctUserGid0"
	fi
	zeroGidAccounts="$(cat "${workDir}/acctUserGid0")"
	test "${osId}" = "freebsd" && zeroGidAccounts="$(printf "%s\n" "${zeroGidAccounts}" | grep -Fvx 'toor')"
	test -z "${zeroGidAccounts}" && return "${XYZ_RC_NOFAULT}"
	faultDescription="Multiple accounts detected with GID 0"
	fixActions="remove the extra accounts ($(printf "%s" "${zeroGidAccounts}" | tr '\n' ',' | sed 's/,$//;s/,/, /g'))"
	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "acct_gid_0" \
  "Check that there are not multiple GID 0 groups" \
  "CIS-Debian12-5.4.2.3"

check_acct_gid_0 () {
	groupZero="root"
	test "${osId}" = "freebsd" && groupZero="wheel"
	test "${osId}" = "openbsd" && groupZero="wheel"
	awk -F ':' '$3==0{print $1}' "${rootDir}/etc/group" | grep -Fvx "${groupZero}" > "${workDir}/acctGid0"
	stripExceptions "${workDir}/acctGid0"
	zeroGidGroups=$(cat "${workDir}/acctGid0")
	test -z "${zeroGidGroups}" && return "${XYZ_RC_NOFAULT}"
	faultDescription="Multiple groups detected with GID 0"
	fixActions="remove the extra groups ($(printf "%s" "${zeroGidGroups}" | tr '\n' ',' | sed 's/,$//;s/,/, /g'))"
	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "acct_sys_static_locked" \
  "Check that static system accounts have locked passwords" \
  "CIS-Debian12-5.4.2.8"

check_acct_sys_static_locked () {
	acctStaticSystemAccounts > "${workDir}/acctStaticSystemAccounts"
	{
	while read -r accountName; do
		acctIsAccountPasswordLocked "${accountName}" && continue
		printf "%s\n" "${accountName}"
	done
	} < "${workDir}/acctStaticSystemAccounts" > "${workDir}/acctStaticSystemAccounts.unlocked"

	stripExceptions "${workDir}/acctStaticSystemAccounts.unlocked"

	test -s "${workDir}/acctStaticSystemAccounts.unlocked" || return "${XYZ_RC_NOFAULT}"

	faultDescription="Static system accounts with usable passwords detected"
	{
	while read -r accountName; do
		printf "%s: %s\n" "lock the password for static system account" "${accountName}"
	done
	} < "${workDir}/acctStaticSystemAccounts.unlocked" > "${workDir}/acctStaticSystemAccounts.unlocked.fix"
	fixActions="$(cat "${workDir}/acctStaticSystemAccounts.unlocked.fix")"

	return "${XYZ_RC_FAULT_HIGHRISK}"
}

fix_acct_sys_static_locked () {
	exec 3<&0
	{
	while read -r accountName; do
		acctLockAccountPassword "${accountName}" <&3
	done
	} < "${workDir}/acctStaticSystemAccounts.unlocked"
}


registerItem \
  "acct_sys_dynamic_locked" \
  "Check that dynamic system accounts have locked passwords" \
  "CIS-Debian12-5.4.2.8"

check_acct_sys_dynamic_locked () {
	acctDynamicSystemAccounts > "${workDir}/acctDynamicSystemAccounts"

	{
	while read -r accountName; do
		acctIsAccountPasswordLocked "${accountName}" && continue
		printf "%s\n" "${accountName}"
	done
	} < "${workDir}/acctDynamicSystemAccounts" > "${workDir}/acctDynamicSystemAccounts.unlocked"

	stripExceptions "${workDir}/acctDynamicSystemAccounts.unlocked"

	test -s "${workDir}/acctDynamicSystemAccounts.unlocked" || return "${XYZ_RC_NOFAULT}"

	faultDescription="Dynamic system accounts with usable passwords detected"
	{
	while read -r accountName; do
		printf "%s: %s\n" "lock the password for dynamic system account" "${accountName}"
	done
	} < "${workDir}/acctDynamicSystemAccounts.unlocked" > "${workDir}/acctDynamicSystemAccounts.unlocked.fix"
	fixActions="$(cat "${workDir}/acctDynamicSystemAccounts.unlocked.fix")"

	return "${XYZ_RC_FAULT_HIGHRISK}"
}

fix_acct_sys_dynamic_locked () {
	exec 3<&0
	{
	while read -r accountName; do
		acctLockAccountPassword "${accountName}" <&3
	done
	} < "${workDir}/acctDynamicSystemAccounts.unlocked"
}


registerItem \
  "acct_guest_locked" \
  "Check that guest accounts have locked passwords" \
  "CIS-Debian12-5.4.2.8"

check_acct_guest_locked () {
	{
	for accountName in "guest" "pcguest"; do
		grep -Eq "^${accountName}:" "${rootDir}/etc/passwd" 2>/dev/null || continue
		acctIsAccountPasswordLocked "${accountName}" && continue
		printf "%s\n" "${accountName}"
	done
	} > "${workDir}/acctGuestAccounts.unlocked"

	stripExceptions "${workDir}/acctGuestAccounts.unlocked"

	test -s "${workDir}/acctGuestAccounts.unlocked" || return "${XYZ_RC_NOFAULT}"
	faultDescription="Guest accounts with usable passwords detected"
	fixActions="lock the accounts ($(tr '\n' ',' < "${workDir}/acctGuestAccounts.unlocked" | sed 's/,$//'))"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}

fix_acct_guest_locked () {
	exec 3<&0
	{
	while read -r accountName; do
		acctLockAccountPassword "${accountName}" <&3
	done
	} < "${workDir}/acctGuestAccounts.unlocked"
}


registerItem \
  "ri_acct_user_home_exists" \
  "Check that all non-system accounts have a home directory" \
  "CIS-Debian12-7.2.9"

check_ri_acct_user_home_exists () {
	test -s "${workDir}/acctUserAccounts" || acctUserAccounts > "${workDir}/acctUserAccounts"
	{
	while read -r accountName; do
		userHome=$(awk -v u="${accountName}" -F ':' '$1==u {print $6}' "${rootDir}/etc/passwd")
		test -n "${userHome}" || continue
		test -d "${userHome}/" && continue
		printf "%s\n" "${accountName}"
	done
	} < "${workDir}/acctUserAccounts" > "${workDir}/acctUserAccounts.nohome"

	stripExceptions "${workDir}/acctUserAccounts.nohome"

	test -s "${workDir}/acctUserAccounts.nohome" || return "${XYZ_RC_NOFAULT}"

	faultDescription="User accounts with no home directory"
	{
	while read -r accountName; do
		printf "%s: %s\n" "create a home directory for user account" "${accountName}"
	done
	} < "${workDir}/acctUserAccounts.nohome" > "${workDir}/acctUserAccounts.nohome.fix"
	fixActions="$(cat "${workDir}/acctUserAccounts.nohome.fix")"

	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "ri_acct_user_home_globalwrite" \
  "Check that all non-system account home directories are not world writable" \
  "CIS-Debian12-7.2.9"

check_ri_acct_user_home_globalwrite () {
	test -s "${workDir}/acctUserAccounts" || acctUserAccounts > "${workDir}/acctUserAccounts"
	{
	while read -r accountName; do
		userHome=$(awk -v u="${accountName}" -F ':' '$1==u {print $6}' "${rootDir}/etc/passwd")
		test -n "${userHome}" || continue
		test -d "${userHome}/" || continue

		dirMode=$(fsFileMode "${userHome}")
		test -n "${dirMode}" || continue

		lastOctet=$(printf "%s\n" "${dirMode}" | rev | cut -b 1)
		test -n "${lastOctet}" || continue

		# Not a problem if the global write bit is not set.
		test "$((lastOctet & 2))" = "2" || continue

		printf "%s\n" "${accountName}"
	done
	} < "${workDir}/acctUserAccounts" > "${workDir}/acctUserAccounts.worldwritable"

	stripExceptions "${workDir}/acctUserAccounts.worldwritable"

	test -s "${workDir}/acctUserAccounts.worldwritable" || return "${XYZ_RC_NOFAULT}"

	faultDescription="User accounts with global write permissions on home directory"
	{
	while read -r accountName; do
		printf "%s: %s\n" "remove global write permission from home directory of user" "${accountName}"
	done
	} < "${workDir}/acctUserAccounts.worldwritable" > "${workDir}/acctUserAccounts.worldwritable.fix"
	fixActions="$(cat "${workDir}/acctUserAccounts.worldwritable.fix")"

	return "${XYZ_RC_FAULT_HIGHRISK}"
}

fix_ri_acct_user_home_globalwrite () {
	{
	while read -r accountName; do
		userHome=$(awk -v u="${accountName}" -F ':' '$1==u {print $6}' "${rootDir}/etc/passwd")
		test -n "${userHome}" || continue
		test -d "${userHome}/" || continue
		chmod o-w "${userHome}/" || return 1
	done
	} < "${workDir}/acctUserAccounts.worldwritable"
	return 0
}


registerItem \
  "ri_acct_user_home_groupwrite" \
  "Check that all non-system account home directories are not group writable" \
  "CIS-Debian12-7.2.9"

check_ri_acct_user_home_groupwrite () {
	test -s "${workDir}/acctUserAccounts" || acctUserAccounts > "${workDir}/acctUserAccounts"
	{
	while read -r accountName; do
		userHome=$(awk -v u="${accountName}" -F ':' '$1==u {print $6}' "${rootDir}/etc/passwd")
		test -n "${userHome}" || continue
		test -d "${userHome}/" || continue

		dirMode=$(fsFileMode "${userHome}")
		test -n "${dirMode}" || continue

		secondLastOctet=$(printf "%s\n" "${dirMode}" | rev | cut -b 2)
		test -n "${secondLastOctet}" || continue

		# Not a problem if the group write bit is not set.
		test "$((secondLastOctet & 2))" = "2" || continue

		printf "%s\n" "${accountName}"
	done
	} < "${workDir}/acctUserAccounts" > "${workDir}/acctUserAccounts.groupwritable"

	stripExceptions "${workDir}/acctUserAccounts.groupwritable"

	test -s "${workDir}/acctUserAccounts.groupwritable" || return "${XYZ_RC_NOFAULT}"

	faultDescription="User accounts with group write permissions on home directory"
	{
	while read -r accountName; do
		printf "%s: %s\n" "remove group write permission from home directory of user" "${accountName}"
	done
	} < "${workDir}/acctUserAccounts.groupwritable" > "${workDir}/acctUserAccounts.groupwritable.fix"
	fixActions="$(cat "${workDir}/acctUserAccounts.groupwritable.fix")"

	return "${XYZ_RC_FAULT_HIGHRISK}"
}

fix_ri_acct_user_home_groupwrite () {
	{
	while read -r accountName; do
		userHome=$(awk -v u="${accountName}" -F ':' '$1==u {print $6}' "${rootDir}/etc/passwd")
		test -n "${userHome}" || continue
		test -d "${userHome}/" || continue
		chmod g-w "${userHome}/" || return 1
	done
	} < "${workDir}/acctUserAccounts.groupwritable"
	return 0
}


registerItem \
  "ri_acct_user_home_owner" \
  "Check that all non-system account home directories are owned by their user" \
  "CIS-Debian12-7.2.9"

check_ri_acct_user_home_owner () {
	test -s "${workDir}/acctUserAccounts" || acctUserAccounts > "${workDir}/acctUserAccounts"
	{
	while read -r accountName; do
		userHome=$(awk -v u="${accountName}" -F ':' '$1==u {print $6}' "${rootDir}/etc/passwd")
		test -n "${userHome}" || continue
		test -d "${userHome}/" || continue
		test "${userHome}" = "/" && continue
		test "${userHome}" = "/tmp" && continue

		dirOwner=$(fsOwnerAccountName "${userHome}")
		test -n "${dirOwner}" || continue

		# Not a problem if the directory owner is the account.
		test "${dirOwner}" = "${accountName}" && continue

		printf "%s\n" "${accountName}"
	done
	} < "${workDir}/acctUserAccounts" > "${workDir}/acctUserAccounts.wrongowner"

	stripExceptions "${workDir}/acctUserAccounts.wrongowner"

	test -s "${workDir}/acctUserAccounts.wrongowner" || return "${XYZ_RC_NOFAULT}"

	faultDescription="User accounts that do not own their home directory"
	{
	while read -r accountName; do
		printf "%s: %s\n" "change home directory ownership of user" "${accountName}"
	done
	} < "${workDir}/acctUserAccounts.wrongowner" > "${workDir}/acctUserAccounts.wrongowner.fix"
	fixActions="$(cat "${workDir}/acctUserAccounts.wrongowner.fix")"

	return "${XYZ_RC_FAULT_HIGHRISK}"
}

fix_ri_acct_user_home_owner () {
	{
	while read -r accountName; do
		userHome=$(awk -v u="${accountName}" -F ':' '$1==u {print $6}' "${rootDir}/etc/passwd")
		test -n "${userHome}" || continue
		test -d "${userHome}/" || continue
		test "${userHome}" = "/" && continue
		test "${userHome}" = "/tmp" && continue
		chown "${accountName}" "${userHome}/" || return 1
	done
	} < "${workDir}/acctUserAccounts.wrongowner"
	return 0
}


registerItem \
  "ri_acct_user_home_dangerousdotfiles" \
  "Check that all non-system account home directories do not contain .forward, .rhosts, or .netrc files" \
  "CIS-Debian12-7.2.10"

check_ri_acct_user_home_dangerousdotfiles () {
	test -s "${workDir}/acctUserAccounts" || acctUserAccounts > "${workDir}/acctUserAccounts"
	{
	while read -r accountName; do
		userHome=$(awk -v u="${accountName}" -F ':' '$1==u {print $6}' "${rootDir}/etc/passwd")
		test -n "${userHome}" || continue
		test -d "${userHome}/" || continue
		test "${userHome}" = "/" && continue
		test "${userHome}" = "/tmp" && continue

		find "${userHome}" -maxdepth 1 \( -name '.forward' -o -name '.rhosts' -o -name '.netrc' \)
	done
	} < "${workDir}/acctUserAccounts" > "${workDir}/dangerousDotFiles"

	stripExceptions "${workDir}/dangerousDotFiles"

	test -s "${workDir}/dangerousDotFiles" || return "${XYZ_RC_NOFAULT}"

	faultDescription="Dangerous dot-files found in user home directories"
	{
	while read -r dangerousPath; do
		printf "delete %s\n" "${dangerousPath}"
	done
	} < "${workDir}/dangerousDotFiles" > "${workDir}/dangerousDotFiles.fix"
	fixActions="$(cat "${workDir}/dangerousDotFiles.fix")"

	return "${XYZ_RC_FAULT_HIGHRISK}"
}

fix_ri_acct_user_home_dangerousdotfiles () {
	{
	while read -r dangerousPath; do
		rm "${dangerousPath}" || return 1
	done
	} < "${workDir}/dangerousDotFiles"
	return 0
}


registerItem \
  "ri_acct_user_home_dotfilepermissions" \
  "Check that all non-system account home directories have appropriate ownership and permissions on config files" \
  "CIS-Debian12-7.2.10"

check_ri_acct_user_home_dotfilepermissions () {
	test -s "${workDir}/acctUserAccounts" || acctUserAccounts > "${workDir}/acctUserAccounts"
	true > "${workDir}/userDotFiles.wrongOwner"
	true > "${workDir}/userDotFiles.wrongGroup"
	true > "${workDir}/userDotFiles.wrongPermissions"
	{
	while read -r accountName; do
		userHome=$(awk -v u="${accountName}" -F ':' '$1==u {print $6}' "${rootDir}/etc/passwd")
		test -n "${userHome}" || continue
		test -d "${userHome}/" || continue
		test "${userHome}" = "/" && continue
		test "${userHome}" = "/tmp" && continue

		userUid=$(awk -v u="${accountName}" -F ':' '$1==u {print $3}' "${rootDir}/etc/passwd")
		userGid=$(awk -v u="${accountName}" -F ':' '$1==u {print $4}' "${rootDir}/etc/passwd")

		find "${userHome}" -maxdepth 1 -type f -name '.*' | {
		while read -r dotFilePath; do
			test "$(fsOwnerUid "${dotFilePath}")" = "${userUid}" || printf "%s %s\n" "${userUid}" "${dotFilePath}" >> "${workDir}/userDotFiles.wrongOwner"
			test "$(fsOwnerGid "${dotFilePath}")" = "${userGid}" || printf "%s %s\n" "${userGid}" "${dotFilePath}" >> "${workDir}/userDotFiles.wrongGroup"
			if test "${dotFilePath##*/}" = ".bash_history" || test "${dotFilePath##*/}" = ".netrc"; then
				if fsGroupOrOtherCanRead "${dotFilePath}" || fsGroupOrOtherCanDoMoreThanRead "${dotFilePath}"; then
					printf "%s %s\n" "600" "${dotFilePath}" >> "${workDir}/userDotFiles.wrongPermissions"
				fi
			else
				fsGroupOrOtherCanDoMoreThanRead "${dotFilePath}" && printf "%s %s\n" "644" "${dotFilePath}" >> "${workDir}/userDotFiles.wrongPermissions"
			fi
		done
		}
	done
	} < "${workDir}/acctUserAccounts"

	stripExceptions "${workDir}/userDotFiles.wrongOwner"
	stripExceptions "${workDir}/userDotFiles.wrongGroup"
	stripExceptions "${workDir}/userDotFiles.wrongPermissions"

	{
		{
		while read -r correctValue dotFilePath; do printf "chown %s %s\n" "${correctValue}" "${dotFilePath}"; done
		} < "${workDir}/userDotFiles.wrongOwner"
		{
		while read -r correctValue dotFilePath; do printf "chown :%s %s\n" "${correctValue}" "${dotFilePath}"; done
		} < "${workDir}/userDotFiles.wrongGroup"
		{
		while read -r correctValue dotFilePath; do printf "chmod %s %s\n" "${correctValue}" "${dotFilePath}"; done
		} < "${workDir}/userDotFiles.wrongPermissions"
	} > "${workDir}/userDotFiles.fix"

	test -s "${workDir}/userDotFiles.fix" || return "${XYZ_RC_NOFAULT}"

	faultDescription="User configuration files found with incorrect ownership or permissions"
	fixActions="$(cat "${workDir}/userDotFiles.fix")"

	return "${XYZ_RC_FAULT_HIGHRISK}"
}

fix_ri_acct_user_home_dotfilepermissions () {
	{
	while read -r correctValue dotFilePath; do chown "${correctValue}" "${dotFilePath}" || return 1; done
	} < "${workDir}/userDotFiles.wrongOwner"
	{
	while read -r correctValue dotFilePath; do chown ":${correctValue}" "${dotFilePath}" || return 1; done
	} < "${workDir}/userDotFiles.wrongGroup"
	{
	while read -r correctValue dotFilePath; do chmod "${correctValue}" "${dotFilePath}" || return 1; done
	} < "${workDir}/userDotFiles.wrongPermissions"

	return 0
}


registerItem \
  "acct_root_password_not_empty" \
  "Check that the root password is not empty" \
  "CIS-Debian12-5.4.2.4"

check_acct_root_password_not_empty () {
	acctIsAccountPasswordEmpty "root" || return "${XYZ_RC_NOFAULT}"
	faultDescription="Root password is empty"
	fixActions="set or lock the root password"
	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "acct_passwords_not_empty" \
  "Check that no password fields are empty" \
  "CIS-Debian12-7.2.2"

check_acct_passwords_not_empty () {
	{
	awk -F ':' 'length($2)==0{print $1}' "${rootDir}/etc/passwd" 2>/dev/null
	awk -F ':' 'length($2)==0{print $1}' "${rootDir}/etc/shadow" 2>/dev/null
	awk -F ':' 'length($2)==0{print $1}' "${rootDir}/etc/master.passwd" 2>/dev/null
	} | sort -u > "${workDir}/acctAccountsWithEmptyPasswords"

	stripExceptions "${workDir}/acctAccountsWithEmptyPasswords"

	test -s "${workDir}/acctAccountsWithEmptyPasswords" || return "${XYZ_RC_NOFAULT}"

	faultDescription="Accounts with empty passwords detected"
	fixActions="$(sed 's/^/set password for account: /' "${workDir}/acctAccountsWithEmptyPasswords")"

	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "acct_passwd_groups_defined" \
  "Check that all groups in /etc/passwd exist in /etc/group" \
  "CIS-Debian12-7.2.3"

check_acct_passwd_groups_defined () {
	awk -F ':' '{print $4}' "${rootDir}/etc/passwd" 2>/dev/null | sort -u > "${workDir}/acctPasswdGidList"
	awk -F ':' '{print $3}' "${rootDir}/etc/group" 2>/dev/null | sort -u > "${workDir}/acctGroupGidList"
	comm -23 "${workDir}/acctPasswdGidList" "${workDir}/acctGroupGidList" > "${workDir}/acctPasswdGidsUndefined"

	test -s "${workDir}/acctPasswdGidsUndefined" || return "${XYZ_RC_NOFAULT}"

	{
	while read -r unknownGid; do
		associatedUsers="$(awk -v g="${unknownGid}" -F ':' '$4==g{print $1}' "${rootDir}/etc/passwd" 2>/dev/null | tr '\n' ',' | sed 's/,$//;s/,/, /g')"
		printf "Define group #%s or change GID of users (%s)\n" "${unknownGid}" "${associatedUsers}"
	done
	} < "${workDir}/acctPasswdGidsUndefined" > "${workDir}/acctPasswdGidsUndefined.fixes"

	faultDescription="User accounts referring to undefined groups"
	fixActions="$(cat "${workDir}/acctPasswdGidsUndefined.fixes")"

	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "acct_shadow_group_empty" \
  "Check that the \"shadow\" group is empty" \
  "CIS-Debian12-7.2.4"

check_acct_shadow_group_empty () {
	shadowGid="$(awk -F ':' '$1=="shadow"{print $3}' "${rootDir}/etc/group" 2>/dev/null)"
	test -n "${shadowGid}" || return "${XYZ_RC_NOFAULT}"
	{
		awk -F ':' '$1=="shadow"{print $4}' "${rootDir}/etc/group" 2>/dev/null | tr ',' '\n'
		awk -F ':' -v g="${shadowGid}" '$4==g{print $1}' "${rootDir}/etc/passwd" 2>/dev/null
	} | sort -u | sed '/^$/d' > "${workDir}/acctShadowGroupMembers"

	stripExceptions "${workDir}/acctShadowGroupMembers"

	test -s "${workDir}/acctShadowGroupMembers" || return "${XYZ_RC_NOFAULT}"

	faultDescription="The \"shadow\" group is not empty"
	fixActions="$(sed 's/^/Remove user from "shadow" group: /' "${workDir}/acctShadowGroupMembers" 2>/dev/null)"

	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "acct_passwd_duplicate_uid" \
  "Check for duplicate UIDs in /etc/passwd" \
  "CIS-Debian12-7.2.5"

check_acct_passwd_duplicate_uid () {
	# NB we skip UID 0 as "acct_uid_0" is also looking for that.
	awk -F ':' '$3>0{print $3}' "${rootDir}/etc/passwd" 2>/dev/null | sort -n | uniq -c | awk '$1>1{print $2}' > "${workDir}/acctPasswdDuplicateUids"

	stripExceptions "${workDir}/acctPasswdDuplicateUids"

	test -s "${workDir}/acctPasswdDuplicateUids" || return "${XYZ_RC_NOFAULT}"

	{
	while read -r duplicateUid; do
		associatedUsers="$(awk -v u="${duplicateUid}" -F ':' '$3==u{print $1}' "${rootDir}/etc/passwd" 2>/dev/null | tr '\n' ',' | sed 's/,$//;s/,/, /g')"
		printf "Resolve duplicate entries for UID #%s (account names: %s)\n" "${duplicateUid}" "${associatedUsers}"
	done
	} < "${workDir}/acctPasswdDuplicateUids" > "${workDir}/acctPasswdDuplicateUids.fixes"

	faultDescription="Duplicate user IDs found in /etc/passwd"
	fixActions="$(cat "${workDir}/acctPasswdDuplicateUids.fixes")"

	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "acct_group_duplicate_gid" \
  "Check for duplicate GIDs in /etc/group" \
  "CIS-Debian12-7.2.6"

check_acct_group_duplicate_gid () {
	# NB we skip GID 0 as "acct_gid_0" is also looking for that.
	awk -F ':' '$3>0{print $3}' "${rootDir}/etc/group" 2>/dev/null | sort -n | uniq -c | awk '$1>1{print $2}' > "${workDir}/acctGroupDuplicateGids"

	stripExceptions "${workDir}/acctGroupDuplicateGids"

	test -s "${workDir}/acctGroupDuplicateGids" || return "${XYZ_RC_NOFAULT}"

	{
	while read -r duplicateGid; do
		associatedGroups="$(awk -v u="${duplicateGid}" -F ':' '$3==u{print $1}' "${rootDir}/etc/group" 2>/dev/null | tr '\n' ',' | sed 's/,$//;s/,/, /g')"
		printf "Resolve duplicate entries for GID #%s (group names: %s)\n" "${duplicateGid}" "${associatedGroups}"
	done
	} < "${workDir}/acctGroupDuplicateGids" > "${workDir}/acctGroupDuplicateGids.fixes"

	faultDescription="Duplicate group IDs found in /etc/group"
	fixActions="$(cat "${workDir}/acctGroupDuplicateGids.fixes")"

	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "acct_passwd_duplicate_name" \
  "Check for duplicate account names in /etc/passwd" \
  "CIS-Debian12-7.2.7"

check_acct_passwd_duplicate_name () {
	awk -F ':' '{print $1}' "${rootDir}/etc/passwd" 2>/dev/null | sort -n | uniq -c | awk '$1>1{print $2}' > "${workDir}/acctPasswdDuplicateNames"

	stripExceptions "${workDir}/acctPasswdDuplicateNames"

	test -s "${workDir}/acctPasswdDuplicateNames" || return "${XYZ_RC_NOFAULT}"

	faultDescription="Duplicate account names found in /etc/passwd"
	fixActions="Resolve duplicate names: $(tr '\n' ',' < "${workDir}/acctPasswdDuplicateNames" | sed 's/,$//;s/,/, /g')"

	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem \
  "acct_group_duplicate_name" \
  "Check for duplicate group names in /etc/group" \
  "CIS-Debian12-7.2.8"

check_acct_group_duplicate_name () {
	awk -F ':' '{print $1}' "${rootDir}/etc/group" 2>/dev/null | sort -n | uniq -c | awk '$1>1{print $2}' > "${workDir}/acctGroupDuplicateNames"

	stripExceptions "${workDir}/acctGroupDuplicateNames"

	test -s "${workDir}/acctGroupDuplicateNames" || return "${XYZ_RC_NOFAULT}"

	faultDescription="Duplicate group names found in /etc/group"
	fixActions="Resolve duplicate names: $(tr '\n' ',' < "${workDir}/acctGroupDuplicateNames" | sed 's/,$//;s/,/, /g')"

	return "${XYZ_RC_FAULT_NOFIX}"
}
