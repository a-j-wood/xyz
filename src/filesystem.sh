#!/bin/sh
#
# Checks and fixes relating to the filesystem.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# Check $1 is a separate partition, populating faultDescription and
# fixActions if not, returning the appropriate return code.
#
fsCheckSeparatePartition () {
	test -d "$1" || return "${XYZ_RC_NOFAULT}"
	fsIsMountPoint "$1" && return "${XYZ_RC_NOFAULT}"
	faultDescription="$1 is not a separate volume"
	fixActions="make a new volume for $1 and mount it"
	return "${XYZ_RC_FAULT_NOFIX}"
}


# Check $1 is mounted with option $2, populating faultDescription and
# fixActions if not, returning the appropriate return code.
#
fsCheckMountedWithOption () {
	# Can't check if not mounted.
	fsIsMountPoint "$1" || return "${XYZ_RC_NOFAULT}"
	fsMountOptions "$1" | grep -Fqx "$2" && return "${XYZ_RC_NOFAULT}"
	faultDescription="$1 is not mounted with the \"$2\" option"
	fixActions="alter mount options for $1 in /etc/fstab and remount or reboot"
	return "${XYZ_RC_FAULT_NOFIX}"
}


registerItem "fs_tmp_ownvolume" "Check /tmp is on its own volume" "CIS-Debian12-1.1.2.1.1"
check_fs_tmp_ownvolume () { fsCheckSeparatePartition "${rootDir}/tmp"; return $?; }

registerItem "fs_tmp_nodev" "Check /tmp is using the \"nodev\" mount option" "CIS-Debian12-1.1.2.1.2"
check_fs_tmp_nodev () { fsCheckMountedWithOption "${rootDir}/tmp" "nodev"; return $?; }

registerItem "fs_tmp_nosuid" "Check /tmp is using the \"nosuid\" mount option" "CIS-Debian12-1.1.2.1.3"
check_fs_tmp_nosuid () { fsCheckMountedWithOption "${rootDir}/tmp" "nosuid"; return $?; }

registerItem "fs_tmp_noexec" "Check /tmp is using the \"noexec\" mount option" "CIS-Debian12-1.1.2.1.4"
check_fs_tmp_noexec () { fsCheckMountedWithOption "${rootDir}/tmp" "noexec"; return $?; }

registerItem "fs_devshm_ownvolume" "Check /dev/shm is on its own volume" "CIS-Debian12-1.1.2.2.1"
check_fs_devshm_ownvolume () { fsCheckSeparatePartition "${rootDir}/dev/shm"; return $?; }

registerItem "fs_devshm_nodev" "Check /dev/shm is using the \"nodev\" mount option" "CIS-Debian12-1.1.2.2.2"
check_fs_devshm_nodev () { fsCheckMountedWithOption "${rootDir}/dev/shm" "nodev"; return $?; }

registerItem "fs_devshm_nosuid" "Check /dev/shm is using the \"nosuid\" mount option" "CIS-Debian12-1.1.2.2.3"
check_fs_devshm_nosuid () { fsCheckMountedWithOption "${rootDir}/dev/shm" "nosuid"; return $?; }

registerItem "fs_devshm_noexec" "Check /dev/shm is using the \"noexec\" mount option" "CIS-Debian12-1.1.2.2.4"
check_fs_devshm_noexec () { fsCheckMountedWithOption "${rootDir}/dev/shm" "noexec"; return $?; }

registerItem "fs_home_ownvolume" "Check /home is on its own volume" "CIS-Debian12-1.1.2.3.1"
check_fs_home_ownvolume () { fsCheckSeparatePartition "${rootDir}/home"; return $?; }

registerItem "fs_home_nodev" "Check /home is using the \"nodev\" mount option" "CIS-Debian12-1.1.2.3.2"
check_fs_home_nodev () { fsCheckMountedWithOption "${rootDir}/home" "nodev"; return $?; }

registerItem "fs_home_nosuid" "Check /home is using the \"nosuid\" mount option" "CIS-Debian12-1.1.2.3.3"
check_fs_home_nosuid () { fsCheckMountedWithOption "${rootDir}/home" "nosuid"; return $?; }

registerItem "fs_var_ownvolume" "Check /var is on its own volume" "CIS-Debian12-1.1.2.4.1"
check_fs_var_ownvolume () { fsCheckSeparatePartition "${rootDir}/var"; return $?; }

registerItem "fs_var_nodev" "Check /var is using the \"nodev\" mount option" "CIS-Debian12-1.1.2.4.2"
check_fs_var_nodev () { fsCheckMountedWithOption "${rootDir}/var" "nodev"; return $?; }

registerItem "fs_var_nosuid" "Check /var is using the \"nosuid\" mount option" "CIS-Debian12-1.1.2.4.3"
check_fs_var_nosuid () { fsCheckMountedWithOption "${rootDir}/var" "nosuid"; return $?; }

registerItem "fs_vartmp_ownvolume" "Check /var/tmp is on its own volume" "CIS-Debian12-1.1.2.5.1"
check_fs_vartmp_ownvolume () { fsCheckSeparatePartition "${rootDir}/var/tmp"; return $?; }

registerItem "fs_vartmp_nodev" "Check /var/tmp is using the \"nodev\" mount option" "CIS-Debian12-1.1.2.5.2"
check_fs_vartmp_nodev () { fsCheckMountedWithOption "${rootDir}/var/tmp" "nodev"; return $?; }

registerItem "fs_vartmp_nosuid" "Check /var/tmp is using the \"nosuid\" mount option" "CIS-Debian12-1.1.2.5.3"
check_fs_vartmp_nosuid () { fsCheckMountedWithOption "${rootDir}/var/tmp" "nosuid"; return $?; }

registerItem "fs_vartmp_noexec" "Check /var/tmp is using the \"noexec\" mount option" "CIS-Debian12-1.1.2.5.4"
check_fs_vartmp_noexec () { fsCheckMountedWithOption "${rootDir}/var/tmp" "noexec"; return $?; }

registerItem "fs_varlog_ownvolume" "Check /var/log is on its own volume" "CIS-Debian12-1.1.2.6.1"
check_fs_varlog_ownvolume () { fsCheckSeparatePartition "${rootDir}/var/log"; return $?; }

registerItem "fs_varlog_nodev" "Check /var/log is using the \"nodev\" mount option" "CIS-Debian12-1.1.2.6.2"
check_fs_varlog_nodev () { fsCheckMountedWithOption "${rootDir}/var/log" "nodev"; return $?; }

registerItem "fs_varlog_nosuid" "Check /var/log is using the \"nosuid\" mount option" "CIS-Debian12-1.1.2.6.3"
check_fs_varlog_nosuid () { fsCheckMountedWithOption "${rootDir}/var/log" "nosuid"; return $?; }

registerItem "fs_varlog_noexec" "Check /var/log is using the \"noexec\" mount option" "CIS-Debian12-1.1.2.6.4"
check_fs_varlog_noexec () { fsCheckMountedWithOption "${rootDir}/var/log" "noexec"; return $?; }

registerItem "fs_varlogaudit_ownvolume" "Check /var/log/audit is on its own volume" "CIS-Debian12-1.1.2.7.1"
check_fs_varlogaudit_ownvolume () { fsCheckSeparatePartition "${rootDir}/var/log/audit"; return $?; }

registerItem "fs_varlogaudit_nodev" "Check /var/log/audit is using the \"nodev\" mount option" "CIS-Debian12-1.1.2.7.2"
check_fs_varlogaudit_nodev () { fsCheckMountedWithOption "${rootDir}/var/log/audit" "nodev"; return $?; }

registerItem "fs_varlogaudit_nosuid" "Check /var/log/audit is using the \"nosuid\" mount option" "CIS-Debian12-1.1.2.7.3"
check_fs_varlogaudit_nosuid () { fsCheckMountedWithOption "${rootDir}/var/log/audit" "nosuid"; return $?; }

registerItem "fs_varlogaudit_noexec" "Check /var/log/audit is using the \"noexec\" mount option" "CIS-Debian12-1.1.2.7.4"
check_fs_varlogaudit_noexec () { fsCheckMountedWithOption "${rootDir}/var/log/audit" "noexec"; return $?; }


registerItem \
  "fs_path_integrity" \
  "Check the integrity of \$PATH" \
  "CIS-Debian12-5.4.2.5"

check_fs_path_integrity () {
	printf '%s\n' "$PATH" | tr ':' '\n' > "${workDir}/fsRootPathComponents"
	{
	currentUid="$(id -u)"
	while read -r pathComponent; do
		if test -z "${pathComponent}"; then
			faultDescription="Empty component found in \$PATH"
			fixActions="Remove the empty component (\"::\") from \$PATH"
			return "${XYZ_RC_FAULT_NOFIX}"
		elif test "${pathComponent}" = "."; then
			faultDescription="\$PATH contains \".\""
			fixActions="Remove the \".\" component from \$PATH"
			return "${XYZ_RC_FAULT_NOFIX}"
		elif test "${pathComponent}" = ".."; then
			faultDescription="\$PATH contains \"..\""
			fixActions="Remove the \"..\" component from \$PATH"
			return "${XYZ_RC_FAULT_NOFIX}"
		elif ! test -e "${pathComponent}"; then
			faultDescription="\$PATH contains a nonexistent component: ${pathComponent}"
			fixActions="Remove \"${pathComponent}\" from \$PATH"
			return "${XYZ_RC_FAULT_NOFIX}"
		elif ! test -d "${pathComponent}"; then
			faultDescription="\$PATH contains a non-directory: ${pathComponent}"
			fixActions="Remove \"${pathComponent}\" from \$PATH"
			return "${XYZ_RC_FAULT_NOFIX}"
		fi
		componentOwner="$(fsOwnerUid "${pathComponent}")"
		if test "${componentOwner}" -ne 0 && ! test "${componentOwner}" -eq "${currentUid}"; then
			faultDescription="\$PATH contains a directory not owned by root or the current user: ${pathComponent}"
			fixActions="Remove \"${pathComponent}\" from \$PATH"
			return "${XYZ_RC_FAULT_NOFIX}"
		fi
		if fsGroupOrOtherCanWrite "${pathComponent}/"; then
			faultDescription="\$PATH contains a directory writable by more than its owner: ${pathComponent}"
			fixActions="Remove non-owner write permission from \"${pathComponent}\""
			return "${XYZ_RC_FAULT_NOFIX}"
		fi
	done
	} < "${workDir}/fsRootPathComponents"
	return "${XYZ_RC_NOFAULT}"
}
