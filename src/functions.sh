#!/bin/sh
#
# Utility functions for check and fix functions to call.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# Output the owning UID of path $1, or nothing on error.
#
fsOwnerUid () {
	if test "${statType}" = "gnu"; then
		stat -c '%u' "$1" 2>/dev/null
	else
		stat -f '%u' "$1" 2>/dev/null
	fi
}


# Output the owning user name of path $1, or nothing on error.
#
fsOwnerAccountName () {
	if test "${statType}" = "gnu"; then
		stat -c '%U' "$1" 2>/dev/null
	else
		stat -f '%Su' "$1" 2>/dev/null
	fi
}


# Output the owning GID of path $1, or nothing on error.
#
fsOwnerGid () {
	if test "${statType}" = "gnu"; then
		stat -c '%g' "$1" 2>/dev/null
	else
		stat -f '%g' "$1" 2>/dev/null
	fi
}


# Output the owning group name of path $1, or nothing on error.
#
fsOwnerGroupName () {
	if test "${statType}" = "gnu"; then
		stat -c '%G' "$1" 2>/dev/null
	else
		stat -f '%Sg' "$1" 2>/dev/null
	fi
}


# Output the last 4 digits of the file mode of $1, or nothing on error.
#
fsFileMode () {
	# Re-use the previous result if it's the same path.
	if test "${fsFileModePrevPath}" = "$1"; then
		test -n "${fsFileModePrevResult}" && printf "%s\n" "${fsFileModePrevResult}"
		return
	fi
	fsFileModePrevPath="$1"
	if test "${statType}" = "gnu"; then
		fsFileModePrevResult="$(stat -c '%a' "$1" 2>/dev/null | rev | cut -b1-4 | rev)"
	else
		fsFileModePrevResult="$(stat -f '%p' "$1" 2>/dev/null | rev | cut -b1-4 | rev)"
	fi
	test -n "${fsFileModePrevResult}" && printf "%s\n" "${fsFileModePrevResult}"
}


# Return true if $1 is readable to other.
#
fsOtherCanRead () {
	fileMode="$(fsFileMode "$1")"
	test -n "${fileMode}" || return 1
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return 1
	test "$((lastOctet & 4))" = "0" && return 1
	return 0
}


# Return true if $1 is writable by other.
#
fsOtherCanWrite () {
	fileMode="$(fsFileMode "$1")"
	test -n "${fileMode}" || return 1
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return 1
	test "$((lastOctet & 2))" = "0" && return 1
	return 0
}


# Return true if $1 is readable to both group and other.
#
fsGroupAndOtherCanRead () {
	fileMode="$(fsFileMode "$1")"
	test -n "${fileMode}" || return 1
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return 1
	test "$((lastOctet & 4))" = "0" && return 1
	secondLastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 2)
	test -n "${secondLastOctet}" || return 1
	test "$((secondLastOctet & 4))" = "0" && return 1
	return 0
}


# Return true if $1 is writable by both group and other.
#
fsGroupAndOtherCanWrite () {
	fileMode="$(fsFileMode "$1")"
	test -n "${fileMode}" || return 1
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return 1
	test "$((lastOctet & 2))" = "0" && return 1
	secondLastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 2)
	test -n "${secondLastOctet}" || return 1
	test "$((secondLastOctet & 2))" = "0" && return 1
	return 0
}


# Return true if $1 is readable to either group or other.
#
fsGroupOrOtherCanRead () {
	fileMode="$(fsFileMode "$1")"
	test -n "${fileMode}" || return 1
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return 1
	test "$((lastOctet & 4))" = "4" && return 0
	secondLastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 2)
	test -n "${secondLastOctet}" || return 1
	test "$((secondLastOctet & 4))" = "4" && return 0
	return 1
}


# Return true if $1 is writable by either group or other.
#
fsGroupOrOtherCanWrite () {
	fileMode="$(fsFileMode "$1")"
	test -n "${fileMode}" || return 1
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return 1
	test "$((lastOctet & 2))" = "2" && return 0
	secondLastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 2)
	test -n "${secondLastOctet}" || return 1
	test "$((secondLastOctet & 2))" = "2" && return 0
	return 1
}


# Return true if $1 is writable or executable to group or other.
#
fsGroupOrOtherCanDoMoreThanRead () {
	fileMode="$(fsFileMode "$1")"
	test -n "${fileMode}" || return 1
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return 1
	test "$((lastOctet % 4))" = "0" || return 0
	secondLastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 2)
	test -n "${secondLastOctet}" || return 1
	test "$((secondLastOctet % 4))" = "0" || return 0
	return 1
}


# Return true if $1 has any execute bits set.
#
fsHasExecutePermissions () {
	fileMode="$(fsFileMode "$1")"
	test -n "${fileMode}" || return 1
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return 1
	test "$((lastOctet % 1))" = "0" || return 0
	secondLastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 2)
	test -n "${secondLastOctet}" || return 1
	test "$((secondLastOctet % 1))" = "0" || return 0
	thirdLastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 3)
	test -n "${thirdLastOctet}" || return 1
	test "$((thirdLastOctet % 1))" = "0" || return 0
	return 1
}


# Return true if $1 is a mount point.
#
fsIsMountPoint () {
	test -d "$1" || return 1
	if command -v mountpoint >/dev/null 2>&1; then
		mountpoint -q "$1" && return 0
	else
		df -P "$1" 2>/dev/null | sed 1d | awk -v p="$1" '$NF==p{print p}' | grep -Eq . && return 0
	fi
	return 1
}


# Output the mount options that $1 is mounted with, one per line.
#
fsMountOptions () {
	# Re-use the previous result if it's the same directory.
	if test "${fsMountOptionsPrevDir}" = "$1"; then
		test -n "${fsMountOptionsPrevResult}" && printf "%s\n" "${fsMountOptionsPrevResult}"
		return
	fi
	fsMountOptionsPrevDir="$1"
	case "${osId}" in
	"freebsd")
		fsMountOptionsPrevResult="$(mount -p | awk -v p="$1" '$2==p {print $4}' | tr ',' '\n')"
		;;
	"openbsd"|"netbsd")
		# Can't read current mount options but can read fstab.
		fsMountOptionsPrevResult="$(awk -v p="$1" '$2==p {print $4}' "${rootDir}/etc/fstab" | tr ',' '\n')"
		;;
	*)
		fsMountOptionsPrevResult="$(awk -v p="$1" '$2==p {print $4}' "${rootDir}/etc/mtab" | tr ',' '\n')"
		;;
	esac
	test -n "${fsMountOptionsPrevResult}" && printf "%s\n" "${fsMountOptionsPrevResult}"
}


# Return true (0) if the user account's password is locked.
#
acctIsAccountPasswordLocked () {
	case "${osId}" in
	*"bsd")
		# If the password field in master.passwd is between 1 and 13
		# characters long or contains "LOCKED", password
		# authentication is disabled.
		awk -v u="$1" -F ':' '$1==u && ($2 ~ /LOCKED/ || $2 ~ /^\*/ || (length($2) > 0 && length($2) < 14)) {print u}' "${rootDir}/etc/master.passwd" 2>/dev/null | grep -Eq . && return 0
		;;
	*)
		LC_ALL=C passwd -S "$1" 2>&1 | grep -Fq -e ' LK ' -e ' L ' && return 0
		;;
	esac
	return 1
}


# Lock a user account's password.
#
acctLockAccountPassword () {
	case "${osId}" in
	"freebsd")
		pw lock "$1" -q
		;;
	"openbsd")
		usermod -Z "$1"
		;;
	*)
		passwd -q -l "$1"
		;;
	esac
}


# Return true (0) if the user account's password is empty (and not locked).
#
acctIsAccountPasswordEmpty () {
	acctIsAccountPasswordLocked "$1" && return 1
	case "${osId}" in
	*"bsd")
		# If the password field in master.passwd is empty, the
		# password is empty.
		awk -v u="$1" -F ':' '$1==u && length($2)==0 {print u}' "${rootDir}/etc/master.passwd" 2>/dev/null | grep -Eq . && return 0
		;;
	*)
		LC_ALL=C passwd -S "$1" 2>&1 | grep -Fq -e ' PS ' -e ' P ' && return 1
		;;
	esac
	return 0
}


# Populate acctUidMin, acctUidMax, acctSysUidMin, acctSysUidMax with the
# appropriate values from /etc/login.defs, or the nearest analogue on this
# system.
#
acctReadUidBands () {
	# Early return if we've already been run.
	test -n "${acctUidMin}" && return

	acctDefaultUidMin=500
	acctDefaultUidMax=60000
	acctDefaultSysUidMin=100
	acctDefaultSysUidMax=499

	acctUidMin=$(awk '$1=="UID_MIN"{print $2}' "${rootDir}/etc/login.defs" 2>/dev/null)
	acctUidMax=$(awk '$1=="UID_MAX"{print $2}' "${rootDir}/etc/login.defs" 2>/dev/null)
	acctSysUidMin=$(awk '$1=="SYS_UID_MIN"{print $2}' "${rootDir}/etc/login.defs" 2>/dev/null)
	acctSysUidMax=$(awk '$1=="SYS_UID_MAX"{print $2}' "${rootDir}/etc/login.defs" 2>/dev/null)

	test -z "${acctSysUidMax}" && test -n "${acctUidMin}" && acctSysUidMax=$((acctUidMin-1))

	test -n "${acctUidMin}" || acctUidMin="${acctDefaultUidMin}"
	test -n "${acctUidMax}" || acctUidMax="${acctDefaultUidMax}"
	test -n "${acctSysUidMin}" || acctSysUidMin="${acctDefaultSysUidMin}"
	test -n "${acctSysUidMax}" || acctSysUidMax="${acctDefaultSysUidMax}"

	acctUidNobody=65534
}


# Output a list of static system accounts - those whose UID is not 0 and is
# either below acctSysUidMin, or is acctUidNobody.
#
acctStaticSystemAccounts () {
	acctReadUidBands
	awk -v min="${acctSysUidMin}" -v nobody="${acctUidNobody}" -F ':' '$3>0 && ($3<min || $3==nobody) {print $1}' "${rootDir}/etc/passwd" 2>/dev/null
}


# Output a list of dynamic system accounts - those whose UID is not 0, is
# between acctSysUidMin and acctSysUidMax inclusive, and is not
# acctUidNobody.
#
acctDynamicSystemAccounts () {
	acctReadUidBands
	awk -v min="${acctSysUidMin}" -v max="${acctSysUidMax}" -v nobody="${acctUidNobody}" -F ':' '$3>0 && $3>=min && $3<=max && $3!=nobody {print $1}' "${rootDir}/etc/passwd" 2>/dev/null
}


# Output a list of user accounts - those whose UID is between acctUidMin and
# acctUidMax inclusive, and is not acctUidNobody.
#
acctUserAccounts () {
	acctReadUidBands
	awk -v min="${acctUidMin}" -v max="${acctUidMax}" -v nobody="${acctUidNobody}" -F ':' '$3>=min && $3<=max && $3!=nobody {print $1}' "${rootDir}/etc/passwd" 2>/dev/null
}


# Output a list of listening TCP and UDP sockets, in the form
# "<address>:<port>", where "<address>" may be IPv4 or IPv6 or "*".
#
netListListeningSockets () {
	case "${osId}" in
	"freebsd")
		sockstat -n -l -4 -6 2>/dev/null | awk 'FNR>1{print $6}' | sort -u
		;;
	"openbsd")
		{ netstat -n -l -p tcp 2>/dev/null; netstat -n -l -p udp 2>/dev/null; } | awk '/^(tcp|udp)/{print $4}' | sort -u
		;;
	*)
		if command -v ss >/dev/null 2>&1; then
			ss -tunl 2>/dev/null | awk 'FNR>1{print $5}' | sort -u
		else
			netstat -tunl 2>/dev/null | awk 'FNR>2{print $4}' | sort -u
		fi
	esac
}
