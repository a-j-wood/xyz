#!/bin/sh
#
# The "help" action.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

xyzHelp () {
	printf "%s: xyz %s\n" "Usage" "[OPTION]... ACTION [ARGUMENT]..."
	printf "%s\n\n" "Check for, and correct, common configuration faults."
	printf "%s\n" "Options:"
	printf "  -e, --exclude PATTERN    %s\n" "exclude items matching PATTERN"
	printf "  -r, --report FILE        %s\n" "write the check report to FILE"
	printf "  -u, --until STAMP        %s\n" "ignore or exception until STAMP, not forever"
	printf "  -d, --delay SEC          %s\n" "random startup delay up to SEC seconds"
	printf "  -p, --prompt             %s\n" "prompt for high-risk fix confirmation"
	printf "  -n, --no                 %s\n" "do not apply high-risk fixes"
	printf "  -y, --yes                %s\n" "apply high-risk fixes without confirmation"
	printf "  -c, --colour             %s\n" "use colour in \"reformat\" output"
	printf "  -j, --json               %s\n" "produce JSON as \"reformat\" output"
	printf "\n"
	printf "  -R, --root DIR           %s\n" "behave as if DIR was the root directory"
	printf "  -C, --component-dir DIR  %s\n" "look in DIR for tool components"
	printf "  -E, --extension-dir DIR  %s\n" "look in DIR for extensions"
	printf "  -H, --hook-dir DIR       %s\n" "look in DIR for hooks"
	printf "\n"
	printf "  -h, --help               %s\n" "show this help and exit"
	printf "  -V, --version            %s\n" "show version information and exit"
	printf "\n%s\n" "Actions:"
	printf "  check all|PATTERN...        %s\n" "check items matching PATTERN"
	printf "  fix all|PATTERN...          %s\n" "fix items matching PATTERN"
	printf "  checks                      %s\n" "list available checks"
	printf "  ignore ITEM REASON          %s\n" "ignore ITEM, for reason REASON"
	printf "  reinstate ITEM...           %s\n" "stop ignoring each ITEM"
	printf "  ignored                     %s\n" "list ignored items"
	printf "  unavailable                 %s\n" "list items with unmet prerequisites"
	printf "  exception-add ITEM VALUE    %s\n" "permit VALUE for ITEM so it no longer alerts"
	printf "  exception-remove ITEM VALUE %s\n" "stop accepting VALUE for ITEM"
	printf "  exceptions [ITEM]           %s\n" "list exceptions for all items, or just ITEM"
	printf "  reformat                    %s\n" "reformat a fault report for human eyes"
	printf "  info                        %s\n" "show information about current system"
	printf "\n%s <%s>.\n" "Please report any bugs to" "${PACKAGE_BUGREPORT}"
}
