#!/bin/sh
#
# Checks for information leaks.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# Check whether file $1 exists and could leak information about the system
# such as its OS and version, setting the appropriate variables and
# returning the relevant return code.
#
infoLeakCheckFileContent () {
	test -s "$1" || return "${XYZ_RC_NOFAULT}"
	if grep -Fq -e '\v' -e '\r' -e '\m' -e '\s' "$1" 2>/dev/null || grep -Fiq "${osId}" "$1"; then
		faultDescription="File \"$1\" leaks information about the system"
		fixActions="update $1 to remove any getty-style information escape sequences or mentions of the OS name"
		return "${XYZ_RC_FAULT_NOFIX}"
	fi
	return "${XYZ_RC_NOFAULT}"
}


# Check whether file $1 is owned by UID and GID 0, setting the appropriate
# variables and returning the relevant return code.
#
infoLeakCheckFileOwner () {
	test -s "$1" || return "${XYZ_RC_NOFAULT}"
	test "$(fsOwnerUid "$1")" = "0" && test "$(fsOwnerGid "$1")" = "0" && return "${XYZ_RC_NOFAULT}"
	faultDescription="File \"$1\" is not owned by UID and GID 0"
	fixActions="chown 0:0 $1"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}


# Check whether file $1 is not writable or executable by anyone more than
# its owner, setting the appropriate variables and returning the relevant
# return code.
#
infoLeakCheckFilePermissions () {
	test -s "$1" || return "${XYZ_RC_NOFAULT}"
	fsGroupOrOtherCanDoMoreThanRead "$1" || return "${XYZ_RC_NOFAULT}"
	faultDescription="File \"$1\" is writable or executable by more than its owning user"
	fixActions="chmod 644 $1"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}


registerItem "infoleak_content_motd" "Check /etc/motd does not leak system information" "CIS-Debian12-1.6.1"
check_infoleak_content_motd () { infoLeakCheckFileContent "${rootDir}/etc/motd"; return $?; }

registerItem "infoleak_content_issue" "Check /etc/issue does not leak system information" "CIS-Debian12-1.6.2"
check_infoleak_content_issue () { infoLeakCheckFileContent "${rootDir}/etc/issue"; return $?; }

registerItem "infoleak_content_issuenet" "Check /etc/issue.net does not leak system information" "CIS-Debian12-1.6.3"
check_infoleak_content_issuenet () { infoLeakCheckFileContent "${rootDir}/etc/issue.net"; return $?; }

registerItem "infoleak_owner_motd" "Check /etc/motd has the correct owner" "CIS-Debian12-1.6.4"
check_infoleak_owner_motd () { infoLeakCheckFileOwner "${rootDir}/etc/motd"; return $?; }
fix_infoleak_owner_motd () { chown 0:0 "${rootDir}/etc/motd" || { reportError "failed to correct ownership of /etc/motd"; return 1; }; return 0; }

registerItem "infoleak_permissions_motd" "Check /etc/motd has the correct file mode" "CIS-Debian12-1.6.4"
check_infoleak_permissions_motd () { infoLeakCheckFilePermissions "${rootDir}/etc/motd"; return $?; }
fix_infoleak_permissions_motd () { chmod 644 "${rootDir}/etc/motd" || { reportError "failed to correct file mode of /etc/motd"; return 1; }; return 0; }

registerItem "infoleak_owner_issue" "Check /etc/issue has the correct owner" "CIS-Debian12-1.6.5"
check_infoleak_owner_issue () { infoLeakCheckFileOwner "${rootDir}/etc/issue"; return $?; }
fix_infoleak_owner_issue () { chown 0:0 "${rootDir}/etc/issue" || { reportError "failed to correct ownership of /etc/issue"; return 1; }; return 0; }

registerItem "infoleak_permissions_issue" "Check /etc/issue has the correct file mode" "CIS-Debian12-1.6.5"
check_infoleak_permissions_issue () { infoLeakCheckFilePermissions "${rootDir}/etc/issue"; return $?; }
fix_infoleak_permissions_issue () { chmod 644 "${rootDir}/etc/issue" || { reportError "failed to correct file mode of /etc/issue"; return 1; }; return 0; }

registerItem "infoleak_owner_issuenet" "Check /etc/issue.net has the correct owner" "CIS-Debian12-1.6.6"
check_infoleak_owner_issuenet () { infoLeakCheckFileOwner "${rootDir}/etc/issue.net"; return $?; }
fix_infoleak_owner_issuenet () { chown 0:0 "${rootDir}/etc/issue.net" || { reportError "failed to correct ownership of /etc/issue.net"; return 1; }; return 0; }

registerItem "infoleak_permissions_issuenet" "Check /etc/issue.net has the correct file mode" "CIS-Debian12-1.6.6"
check_infoleak_permissions_issuenet () { infoLeakCheckFilePermissions "${rootDir}/etc/issue.net"; return $?; }
fix_infoleak_permissions_issuenet () { chmod 644 "${rootDir}/etc/issue.net" || { reportError "failed to correct file mode of /etc/issue.net"; return 1; }; return 0; }
