#!/bin/sh
#
# Item processing functions.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# Given a list of item patterns, populate the selectedItems variable with a
# space-separated list of matching items which are not locally ignored, not
# excluded by patterns listed in the excludePattern variable, and whose
# prerequisites are available.
#
# If any of the provided item patterns refer to an item which is entirely
# undefined, report an error, and exit with code XYZ_EXIT_BADARGS.
#
selectAvailableItems () {
	test -d "${workDir}/items" || mkdir "${workDir}/items"

	# Expand the item patterns, exiting early if any undefined items
	# have been specified.
	true > "${workDir}/items/allPatternMatches"
	for itemPattern in "$@"; do
		test -n "${itemPattern}" || continue
		test "${itemPattern}" = "all" && itemPattern="*"

		# NB can't use "find -printf '%f\n'" to remove the need for
		# the following "awk", as "-printf" is not in every "find".

		find "${workDir}/items/description" -type f -name "${itemPattern}" -maxdepth 1 -print 2>/dev/null | awk -F '/' '{print $NF}' > "${workDir}/items/patternMatch"
		test -s "${workDir}/items/patternMatch" || { reportError "${itemPattern}: unknown item"; exit "${XYZ_EXIT_BADARGS}"; }
		cat "${workDir}/items/patternMatch" >> "${workDir}/items/allPatternMatches"
	done

	# For each selected item, create a file if it is both available and
	# not ignored.
	test -d "${workDir}/items/selected" || mkdir "${workDir}/items/selected"
	LC_ALL=C sort -u < "${workDir}/items/allPatternMatches" | while read -r matchedItem; do
		test -e "${workDir}/items/unavailable/${matchedItem}" && continue
		test -e "${rootDir}${ignoreDir}/${matchedItem}.ignore" && continue
		if test -e "${rootDir}${ignoreDir}/${matchedItem}.expiring-ignore"; then
			ignoreUntil="$(sed -n 1p "${rootDir}${ignoreDir}/${matchedItem}.expiring-ignore" 2>/dev/null)"
			test "${ignoreUntil}" '>' "${startStamp}" && continue
		fi
		true > "${workDir}/items/selected/${matchedItem}"
	done

	# Remove selected items matching any excludePattern.
	printf "%s\n" "${excludePattern}" | while read -r exclusionPattern; do
		test -n "${exclusionPattern}" || continue
		find "${workDir}/items/selected" -type f -name "${exclusionPattern}" -maxdepth 1 -delete 2>/dev/null
	done 

	# List the remaining items.
	selectedItems="$(find "${workDir}/items/selected" -type f -maxdepth 1 -print 2>/dev/null | awk -F '/' '{print $NF}' | LC_ALL=C sort)"
}


# Check that the item name $1 contains no disallowed characters and refers
# to a defined item, exiting the script with XYZ_EXIT_BADARGS if not.
#
validateItemName () {
	test -n "$1" || { reportError "empty item name"; exit "${XYZ_EXIT_BADARGS}"; }
	if printf "%s\n" "$1" | tr -d '0-9A-Za-z_' | grep -Eq .; then
		reportError "$1: item name contains disallowed characters"
		exit "${XYZ_EXIT_BADARGS}"
	fi
	test -e "${workDir}/items/description/$1" || { reportError "unknown item"; exit "${XYZ_EXIT_BADARGS}"; }
	return 0
}


# Alter the list file $1 to strip any exceptions for the current item.
#
stripExceptions () {
	test -s "$1" || return

	test -s "${rootDir}${ignoreDir}/${currentItem}.exception" && cat "${rootDir}${ignoreDir}/${currentItem}.exception" >> "${workDir}/exceptions-${currentItem}"
	if test -s "${rootDir}${ignoreDir}/${currentItem}.expiring-exception"; then
		awk -v "t=${startStamp}" 't<$1 {print}' < "${rootDir}${ignoreDir}/${currentItem}.expiring-exception" \
		| cut -d ' ' -f 2- \
		>> "${workDir}/exceptions-${currentItem}"
	fi

	# Early return if there are no exceptions to strip.
	test -s "${workDir}/exceptions-${currentItem}" || return

	tmpReplacementFile=$(mktemp "$1.tmpXXXXXX")
	test -n "${tmpReplacementFile}" || { reportError "failed to create temporary exception removal file"; exit "${XYZ_EXIT_FAILURE}"; }
	grep -Fvxf "${workDir}/exceptions-${currentItem}" < "$1" > "${tmpReplacementFile}"
	mv -f "${tmpReplacementFile}" "$1"
}


# Return true if item $1's check function calls the "stripExceptions"
# function, otherwise report an error and return false.
#
doesItemUseExceptions () {
	itemName="$1"

	find "${componentDir}" -type f -name "*.sh" -maxdepth 1 2>/dev/null > "${workDir}/stripExceptionsCheckScriptFiles"
	test -n "${extensionDir}" && find "${extensionDir}" -type f -name "*.sh" -maxdepth 1 2>/dev/null >> "${workDir}/stripExceptionsCheckScriptFiles"

	itemUsesExceptions="no"
	{
	while read -r scriptFileName; do
		definitionStartLine="$(grep -hn -E "^check_${itemName}[[:space:]]" "${scriptFileName}" 2>/dev/null | cut -d : -f 1 | sed -n 1p)"
		test -n "${definitionStartLine}" || continue
		sed -n "${definitionStartLine},/^}/p" "${scriptFileName}" 2>/dev/null | grep -Fq "stripExceptions" && itemUsesExceptions="yes"
		break
	done
	} < "${workDir}/stripExceptionsCheckScriptFiles"

	if test "${itemUsesExceptions}" = "no"; then
		reportError "${itemName}: this item does not allow exceptions"
		return 1
	fi

	return 0
}


# Run all check functions for the items in selectedItems, updating
# exitStatus if there is an error, and incrementing faultCount for each
# fault found.
#
xyzCheck () {
	checkReport=""

	for itemName in ${selectedItems}; do
		currentItem="${itemName}"
		checkFunction="check_${itemName}"
		if ! type "${checkFunction}" >/dev/null 2>&1; then
			reportError "${itemName}: no check function is defined"
			exitStatus="${XYZ_EXIT_FAILURE}"
			continue
		fi

		${checkFunction}
		checkResult=$?

		test "${checkResult}" -eq "${XYZ_RC_NOFAULT}" && continue

		faultCount=$((1+faultCount))

		flagString="m"
		case "${checkResult}" in
		"${XYZ_RC_FAULT_LOWRISK}") flagString="a" ;;
		"${XYZ_RC_FAULT_HIGHRISK}") flagString="R" ;;
		esac

		reportLine="$(printf "%s\t%s\t%s\t%s" "${flagString}" "${itemName}" "${faultDescription}" "$(printf "%s" "${fixActions}" | tr '\n' '|' | sed 's/|$//')")"

		if test -n "${reportFile}"; then
			if test -n "${checkReport}"; then
				checkReport="$(printf "%s\n%s" "${checkReport}" "${reportLine}")"
			else
				checkReport="${reportLine}"
			fi
		else
			printf "%s\n" "${reportLine}"
		fi
	done

	if test -n "${reportFile}"; then
		tmpReplacementFile=$(mktemp "${reportFile}.tmpXXXXXX")
		test -n "${tmpReplacementFile}" || { reportError "${reportFile}: failed to create temporary report file"; exit "${XYZ_EXIT_FAILURE}"; }
		true > "${tmpReplacementFile}"
		test -n "${checkReport}" && printf "%s\n" "${checkReport}" > "${tmpReplacementFile}"
		chmod 644 "${tmpReplacementFile}"
		mv -f "${tmpReplacementFile}" "${reportFile}"
	fi
}


# For the items in selectedItems that have a defined fix function, run the
# check function, and if there is a fault, run the fix function, subject to
# the runHighRiskFixes setting and user input for high-risk fixes; updates
# exitStatus if there are any errors.
#
xyzFix () {
	for itemName in ${selectedItems}; do
		currentItem="${itemName}"
		checkFunction="check_${itemName}"
		if ! type "${checkFunction}" >/dev/null 2>&1; then
			reportError "${itemName}: no check function is defined"
			exitStatus="${XYZ_EXIT_FAILURE}"
			continue
		fi

		fixFunction="fix_${itemName}"
		type "${fixFunction}" >/dev/null 2>&1 || continue

		${checkFunction}
		checkResult=$?

		case "${checkResult}" in
		"${XYZ_RC_FAULT_LOWRISK}")
			${fixFunction} || exitStatus="${XYZ_EXIT_FAILURE}"
			;;
		"${XYZ_RC_FAULT_HIGHRISK}")
			case "${runHighRiskFixes}" in
			"prompt")
				printf "%s\n%s: %s\n%s:\n%s\n-- %s: " "**********" "${itemName}" "${faultDescription}" "Actions to be performed" "$(printf "%s" "${fixActions}" | tr '|' '\n')" "Type \"yes\" to proceed"
				read -r response
				case "${response}" in
				"Y"*|"y"*)
					printf "%s\n" "Confirmation received - running actions."
					${fixFunction} || exitStatus="${XYZ_EXIT_FAILURE}"
					;;
				esac
				;;
			"yes")
				${fixFunction} || exitStatus="${XYZ_EXIT_FAILURE}"
				;;
			"no")	;;
			esac
			;;
		esac
	done
}


# List the checks in selectedItems.
#
xyzListChecks () {
	printf "%s\n" "${selectedItems}" | while read -r itemName; do
		read -r itemDescription < "${workDir}/items/description/${itemName}"
		read -r itemDerivedFrom < "${workDir}/items/derivedFrom/${itemName}"
		printf "%s\n  %s\n" "${itemName}" "${itemDescription}"
		test -n "${itemDerivedFrom}" && printf "  - %s: %s\n" "Derived from" "${itemDerivedFrom}"
		printf "\n"
	done
}


# Mark item $1 as ignored, for reason $2 onwards.
#
xyzIgnore () {
	validateItemName "$1"
	itemName="$1"
	shift
	test -d "${rootDir}${ignoreDir}" || mkdir -m 755 "${rootDir}${ignoreDir}"
	if test -n "${untilStamp}"; then
		printf "%s\n%s\n" "${untilStamp}" "$*" > "${rootDir}${ignoreDir}/${itemName}.expiring-ignore"
		rm -f "${rootDir}${ignoreDir}/${itemName}.ignore"
	else
		printf "%s\n" "$*" > "${rootDir}${ignoreDir}/${itemName}.ignore"
		rm -f "${rootDir}${ignoreDir}/${itemName}.expiring-ignore"
	fi
}


# Remove the ignore marker for the given items.
#
xyzReinstate () {
	for itemName in "$@"; do
		validateItemName "${itemName}"
		test -e "${rootDir}${ignoreDir}/${itemName}.ignore" && rm -f "${ignoreDir}/${itemName}.ignore"
		test -e "${rootDir}${ignoreDir}/${itemName}.expiring-ignore" && rm -f "${ignoreDir}/${itemName}.expiring-ignore"
	done
}


# List all ignored items, when their ignore entry expires, and the reason
# they were ignored.
#
xyzListIgnored () {
	find "${workDir}/items/description" -type f -maxdepth 1 -print 2>/dev/null | awk -F '/' '{print $NF}' | while read -r itemName; do
		ignoreReason=""
		if test -e "${rootDir}${ignoreDir}/${itemName}.ignore"; then
			read -r ignoreReason < "${rootDir}${ignoreDir}/${itemName}.ignore"
			printf "%s\t-\t%s\n" "${itemName}" "${ignoreReason}"
		elif test -e "${rootDir}${ignoreDir}/${itemName}.expiring-ignore"; then
			ignoreUntil="-"
			{
			read -r ignoreUntil
			read -r ignoreReason
			test "${ignoreUntil}" '>' "${startStamp}" && printf "%s\t%s\t%s\n" "${itemName}" "${ignoreUntil}" "${ignoreReason}"
			} < "${rootDir}${ignoreDir}/${itemName}.expiring-ignore"
		fi
	done
}


# Add an exception for faults in $2 for item $1.
#
xyzExceptionAdd () {
	validateItemName "$1"
	doesItemUseExceptions "$1" || { exitStatus="${XYZ_EXIT_BADARGS}"; return 1; }
	itemName="$1"
	test -d "${rootDir}${ignoreDir}" || mkdir -m 755 "${rootDir}${ignoreDir}"

	# Remove the exception first, so it's never added twice, and never
	# ends up in both the expiring and non-expiring lists.
	xyzExceptionRemove "$1" "$2"

	if test -n "${untilStamp}"; then
		# Add the exception to the expiring exceptions list.
		printf "%s %s\n" "${untilStamp}" "$2" >> "${rootDir}${ignoreDir}/${itemName}.expiring-exception"
	else
		# Add the exception to the non-expiring exceptions list.
		printf "%s\n" "$2" >> "${rootDir}${ignoreDir}/${itemName}.exception"
	fi
}


# Remove an exception for faults in $2 from item $1.
#
xyzExceptionRemove () {
	validateItemName "$1"
	doesItemUseExceptions "$1" || { exitStatus="${XYZ_EXIT_BADARGS}"; return 1; }
	itemName="$1"

	if test -s "${rootDir}${ignoreDir}/${itemName}.exception"; then
		# Remove matching non-expiring exception entries.
		tmpReplacementFile=$(mktemp "${rootDir}${ignoreDir}/${itemName}.exception.tmpXXXXXX")
		test -n "${tmpReplacementFile}" || { reportError "failed to create temporary exception file"; exit "${XYZ_EXIT_FAILURE}"; }
		grep -Fvx "$2" < "${rootDir}${ignoreDir}/${itemName}.exception" > "${tmpReplacementFile}"
		mv -f "${tmpReplacementFile}" "${rootDir}${ignoreDir}/${itemName}.exception"
	fi

	if test -s "${rootDir}${ignoreDir}/${itemName}.expiring-exception"; then
		# Remove matching expiring exception entries.
		lineNumbers="$(cut -d ' ' -f 2- < "${rootDir}${ignoreDir}/${itemName}.expiring-exception" | grep -Fxhn "$2" | cut -d : -f 1)"
		# Delete matching lines, bottom of the file to top.
		printf "%s\n" "${lineNumbers}" | sort -rn | grep -E . | while read -r lineNumber; do
			sed -i "${lineNumber}d" "${rootDir}${ignoreDir}/${itemName}.expiring-exception"
		done
	fi
}


# List all exceptions for item $1, or for all items if $1 is not given.
#
xyzListExceptions () {
	test -n "$1" && validateItemName "$1"
	itemPattern="*"
	test -n "$1" && itemPattern="$1"
	find "${workDir}/items/description" -type f -name "${itemPattern}" -maxdepth 1 -print 2>/dev/null \
	| awk -F '/' '{print $NF}' \
	| {
	while read -r itemName; do
		exceptions="$(
		test -s "${rootDir}${ignoreDir}/${itemName}.exception" \
		&& awk '{printf "\t-\t%s\n", $0}' < "${rootDir}${ignoreDir}/${itemName}.exception"
		test -s "${rootDir}${ignoreDir}/${itemName}.expiring-exception" \
		&& awk -v "t=${startStamp}" 't<$1 {sub(/ /,"\t",$0);printf "\t%s\n", $0}' < "${rootDir}${ignoreDir}/${itemName}.expiring-exception"
		)"
		test -n "${exceptions}" && printf "%s:\n%s\n" "${itemName}" "${exceptions}"
	done
	}
}


# List all items which are unavailable due to missing prerequisites.
#
xyzListUnavailable () {
	find "${workDir}/items/unavailable" -type f -maxdepth 1 -print 2>/dev/null | while read -r filename; do
		missingCommands=$(tr '\n' ' ' < "${filename}")
		test -n "${missingCommands}" || continue
		printf "%s\t%s: %s\n" "${filename##*/}" "requires" "${missingCommands}"
	done
}
