#!/bin/sh
#
# String function for JSON output.
#
# Copyright 2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# Output $1 with JSON escaping, such that it could be surrounded by double
# quotes to form a valid JSON string.
#
# Note that this will strip the trailing newline, if there is one.
#
# (This was taken from UACT - https://ivarch.com/programs/uact.shtml)
#
xyzJsonEscapeString () {
	rawString="$1"
	test -n "${rawString}" || return
	# OpenBSD sed doesn't seem to get on with 's,\\,\\\\,g'.
	# printf "%s\n" "${rawString}" | sed -e 's,\\,\\\\,g;s,",\\",g;s,\r,\\r,g;s,\t,\\t,g;s,$,\\n,' | tr -d '\n' | sed 's,\\n$,,'
	printf "%s\n" "${rawString}" \
	| awk '{ gsub(/\\/,"\\\\",$0); gsub(/"/,"\\\"",$0); gsub(/\t/,"\\t",$0); gsub(/\r/,"\\r",$0); print $0 "\\n" }' \
	| tr -d '\n' \
	| sed 's,\\n$,,'
}

# Output a string $1="$2" unless reformatJson is "yes", in which case output
# "$1": "$2"$3, where $3 is a comma if this isn't the last item.
#
xyzOutputInfo () {
	if test "${reformatJson}" = "yes"; then
		printf ' "%s": "%s"%s\n' "$1" "$(xyzJsonEscapeString "$2")" "$3"
	else
		printf '%s="%s"\n' "$1" "$2"
	fi
}
