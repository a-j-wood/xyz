#!/bin/sh
#
# Checks and fixes relating to Linux kernel modules.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# These functions only make sense on Linux systems.
test "${kernelName}" = "linux" || return 0

# Return true (0) if kernel module $1, such as "cramfs", is available for
# any kernel version on this system.
#
isKernelModuleAvailableAtAll () {
	find "${rootDir}/lib/modules"/*/kernel -type d -name "$1" 2>/dev/null | grep -Eq . && return 0
	find "${rootDir}/lib/modules"/*/kernel -type f -name "$1.*" 2>/dev/null | grep -Eq . && return 0
	return 1
}

# Return true (0) if kernel module $1, such as "cramfs", is available for
# the currently running kernel on this system.
#
isKernelModuleAvailableForRunningKernel () {
	find "${rootDir}/lib/modules/${kernelRelease}/kernel" -type d -name "$1" 2>/dev/null | grep -Eq . && return 0
	find "${rootDir}/lib/modules/${kernelRelease}/kernel" -type f -name "$1.*" 2>/dev/null | grep -Eq . && return 0
	return 1
}

# Return true (0) if kernel module $1 is possibly loadable - that is,
# running "modprobe $1" could actually load a module into an installed
# kernel according to the current configuration, if it was present on the
# system (so check isKernelModuleAvailableAtAll first).
#
isKernelModuleLoadableAtAll () {
	# NB we ignore "blacklist" entries because they don't stop a module
	# being loaded by "modprobe" when the module is directly specified. 
	# Currently, the only way to stop a module from being installable
	# via "modprobe" is with an "install <module> /bin/true"
	# configuration directive.
	modprobeConfig="${rootDir}/etc/modprobe.d"
	test -d "${modprobeConfig}" || modprobeConfig="${rootDir}/etc/modprobe.conf"
	relevantModprobeConfig="$(modprobe --config "${modprobeConfig}" --showconfig 2>/dev/null | awk -v m="$1" '$1=="install"&&$2==m{print}')"
	test -z "${relevantModprobeConfig}" \
	&& relevantModprobeConfig="$(modprobe --config "${modprobeConfig}" --showconfig 2>/dev/null | awk -v m="$(printf '%s\n' "$1" | tr '-' '_')" '$1=="install"&&$2==m{print}')"

	# If no "install" lines for this module, it will just load normally.
	test -n "${relevantModprobeConfig}" || return 0

	# If the "install" command is a dummy like /bin/true, the module
	# won't load.
	printf "%s\n" "${relevantModprobeConfig}" \
	| awk '{$1="";$2="";print}' \
	| grep -Eq '^[[:space:]]*/bin/(false|true)[[:space:]]*$' \
	&& return 1

	# If it's anything else, we can't assume the module won't load, so
	# we say it can.
	return 0
}

# Return true (0) if kernel module $1 is loadable now - that is, running
# "modprobe $1" would actually load a module into the current kernel as its
# last step.
#
isKernelModuleLoadableNow () {
	modprobeConfig="${rootDir}/etc/modprobe.d"
	test -d "${modprobeConfig}" || modprobeConfig="${rootDir}/etc/modprobe.conf"
	modprobe --config "${modprobeConfig}" --dry-run --verbose "$1" 2>/dev/null | sed -n '$p' | grep -Eq "^insmod" && return 0
	return 1
}

# Return true (0) if kernel module $1 is currently loaded.
#
isKernelModuleLoaded () {
	lsmod 2>/dev/null | awk '{print $1}' | grep -Fqx "$1" && return 0
	return 1
}

# Check function, used by all of the lkm_available_* items, to check whether
# kernel module $1 is available to load with modprobe.
#
lkmCheckAvailable () {
	if isKernelModuleAvailableForRunningKernel "$1" && isKernelModuleLoadableNow "$1"; then
		faultDescription="The \"$1\" module is available to load into the currently running kernel"
		fixActions="add a modprobe.d configuration entry to block the module \"$1\" from loading"
		return "${XYZ_RC_FAULT_HIGHRISK}"
	fi
	if isKernelModuleAvailableAtAll "$1" && isKernelModuleLoadableAtAll "$1"; then
		faultDescription="The \"$1\" module is available to load into an installed kernel"
		fixActions="add a modprobe.d configuration entry to block the module \"$1\" from loading"
		return "${XYZ_RC_FAULT_HIGHRISK}"
	fi
	return "${XYZ_RC_NOFAULT}"
}

# Fix function, used by all of the lkm_available_* items, to make kernel
# module $1 unavailable to modprobe.
#
lkmFixAvailable () {
	printf "%s %s %s\n" "install" "$1" "/bin/true" > "${rootDir}/etc/modprobe.d/prevent-load-$1.conf" \
	|| { reportError "$1: failed to write configuration file to block kernel module loading"; return 1; }
	return 0
}

# Check function, used by all of the lkm_loaded_* items, to check whether
# kernel module $1 is currently loaded.
#
lkmCheckLoaded () {
	isKernelModuleLoaded "$1" || return "${XYZ_RC_NOFAULT}"
	faultDescription="The \"$1\" module is currently loaded"
	fixActions="modprobe -r $1"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}

# Fix function, used by all of the lkm_loaded_* items, to unload kernel
# module $1.
#
lkmFixLoaded () {
	modprobe -r "$1" || { reportError "$1: failed to unload kernel module"; return 1; }
	isKernelModuleLoaded "$1" && { reportError "$1: kernel module still present after attempting to unload"; return 1; }
	return 0
}

for moduleSpec in \
  cramfs:CIS-Debian12-1.1.1.1 \
  freevxfs:CIS-Debian12-1.1.1.2 \
  hfs:CIS-Debian12-1.1.1.3 \
  hfsplus:CIS-Debian12-1.1.1.4 \
  jffs2:CIS-Debian12-1.1.1.5 \
  squashfs:CIS-Debian12-1.1.1.6 \
  udf:CIS-Debian12-1.1.1.7 \
  usb-storage:CIS-Debian12-1.1.1.8 \
; do
	kernelModule="${moduleSpec%:*}"
	derivedFrom="${moduleSpec#*:}"
	moduleItemName="$(printf "%s\n" "${kernelModule}" | tr '-' '_')"
	registerItem "lkm_available_${moduleItemName}" "Check that the \"${kernelModule}\" module is not available to load" "${derivedFrom}"
	eval "check_lkm_available_${moduleItemName} () { lkmCheckAvailable \"${kernelModule}\"; return \$?; }"
	eval "fix_lkm_available_${moduleItemName} () { lkmFixAvailable \"${kernelModule}\"; return \$?; }"
	registerItem "lkm_loaded_${moduleItemName}" "Check that the \"${kernelModule}\" module is not currently loaded" "${derivedFrom}"
	eval "check_lkm_loaded_${moduleItemName} () { lkmCheckLoaded \"${kernelModule}\"; return \$?; }"
	eval "fix_lkm_loaded_${moduleItemName} () { lkmFixLoaded \"${kernelModule}\"; return \$?; }"
done
