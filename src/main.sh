#!/bin/sh
#
# Core functions and main program entry point.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# Adjust the default path.
# Note that /usr/local is vital on BSDs for tools like gpg.
export PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin:/usr/local/sbin:$PATH"

# Explicitly set the umask.
umask 0022

#
# Core functions.
#

# Output the message $1 to standard error, prefixed with the program name
# and a colon.
#
reportError () {
	printf "%s: %s\n" "${PROGRAM_NAME}" "$1" 1>&2
}

# Call reportError and provide an additional reminder about how to check the
# program syntax.
#
reportArgumentsError () {
	reportError "$*"
	printf "%s\n" "Try \`${PROGRAM_NAME} --help' for more information." 1>&2
}

# Source all script files in directory $1 matching pattern $2, in
# alphanumeric order, excluding any whose basename ends in regexp $3.
#
loadScriptsInDirectory () {
	test -n "$1" || return
	test -n "$2" || return
	test -d "$1" || return
	find "$1" -type f -name "$2" -maxdepth 1 2>/dev/null | LC_ALL=C sort | grep -Ev "/$3\$" > "${workDir}/scriptFiles"
	exec 3<&0
	{
	while read -r scriptFile; do
		test -f "${scriptFile}" || continue
		. "${scriptFile}" <&3
	done
	} < "${workDir}/scriptFiles"
}

# Register a check item named $1, with description $2, pertaining to
# standards $3; remaining arguments, if any, are the prerequisites - if any
# are not found with "command -v", the item is marked as unavailable.
#
# If a prerequisite starts with "?", then the "?" is stripped and the
# remainder executed; if the result is false, the item is marked as
# unavailable (for example, "?test -e /proc/self/exe").
#
registerItem () {
	itemName="$1"
	shift
	itemDescription="$1"
	shift
	itemDerivedFrom="$1"
	shift

	test -d "${workDir}/items" || mkdir "${workDir}/items"
	test -d "${workDir}/items/description" || mkdir "${workDir}/items/description"
	test -d "${workDir}/items/derivedFrom" || mkdir "${workDir}/items/derivedFrom"
	test -d "${workDir}/items/unavailable" || mkdir "${workDir}/items/unavailable"

	printf "%s\n" "${itemDescription}" > "${workDir}/items/description/${itemName}"
	printf "%s\n" "${itemDerivedFrom}" > "${workDir}/items/derivedFrom/${itemName}"
	for itemPrerequisite in "$@"; do
		prerequisiteMet='false'
		case "${itemPrerequisite}" in
		'?'*) ${itemPrerequisite#?} >/dev/null 2>&1 && prerequisiteMet='true' ;;
		*) command -v "${itemPrerequisite}" >/dev/null 2>&1 && prerequisiteMet='true' ;;
		esac
		test "${prerequisiteMet}" = 'true' && continue
		printf "%s\n" "${itemPrerequisite}" >> "${workDir}/items/unavailable/${itemName}"
	done
}

#
# Variable initialisation and command-line option processing.
#

# Initial values for command-line option processing.
rootDir=""
action=""
excludePattern=""
reportFile=""
runHighRiskFixes="prompt"
reformatColour="no"
reformatJson="no"
randomDelay="0"
untilStamp=""
minArgs=0
maxArgs=0

# Default to no high-risk fixes if stdin is not a terminal.
test -t 0 || runHighRiskFixes="no"

# Parse the command-line options to apply options and select the action.
while test -n "$1" && test -z "${action}"; do
	arg="$1"
	shift
	case "${arg}" in
	"-h"|"--help"|"help")
		action="help"
		;;
	"-V"|"--version"|"version")
		action="version"
		;;
	"-e"|"--exclude")
		excludePattern="$(printf "%s\n%s" "$1" "${excludePattern}")"
		shift
		;;
	"-r"|"--report")
		reportFile="$1"
		shift
		;;
	"-d"|"--delay")
		randomDelay="$1"
		shift
		;;
	"-u"|"--until")
		untilStamp="$1"
		shift
		;;
	"-p"|"--prompt")
		runHighRiskFixes="prompt"
		;;
	"-y"|"--yes")
		runHighRiskFixes="yes"
		;;
	"-n"|"--no")
		runHighRiskFixes="no"
		;;
	"-c"|"--colour"|"--color")
		reformatColour="yes"
		;;
	"-j"|"--json")
		reformatJson="yes"
		;;
	"-R"|"--root"|"--root-dir")
		rootDir="$1"
		shift
		;;
	"-C"|"--component-dir")
		componentDir="$1"
		shift
		;;
	"-E"|"--extension-dir")
		extensionDir="$1"
		shift
		;;
	"-H"|"--hook-dir")
		hookDir="$1"
		shift
		;;
	"--exclude="*)
		excludePattern="$(printf "%s\n%s" "${arg#*=}" "${excludePattern}")"
		;;
	"--report="*)
		reportFile="${arg#*=}"
		;;
	"--delay="*)
		randomDelay="${arg#*=}"
		;;
	"--until="*)
		untilStamp="${arg#*=}"
		;;
	"--root="*|"--root-dir="*)
		rootDir="${arg#*=}"
		;;
	"--component-dir="*)
		componentDir="${arg#*=}"
		;;
	"--extension-dir="*)
		extensionDir="${arg#*=}"
		;;
	"--hook-dir="*)
		hookDir="${arg#*=}"
		;;
	"-"*)
		reportArgumentsError "${arg}: unknown option"
		exit "${XYZ_EXIT_BADARGS}"
		;;
	"check")
		minArgs=1
		maxArgs=0
		action="check"
		;;
	"fix")
		minArgs=1
		maxArgs=0
		action="fix"
		;;
	"checks")
		minArgs=0
		maxArgs=0
		action="checks"
		;;
	"ignore")
		minArgs=2
		maxArgs=0
		action="ignore"
		;;
	"reinstate")
		minArgs=1
		maxArgs=0
		action="reinstate"
		;;
	"ignored")
		minArgs=0
		maxArgs=0
		action="ignored"
		;;
	"exception-add")
		minArgs=2
		maxArgs=2
		action="exception-add"
		;;
	"exception-remove")
		minArgs=2
		maxArgs=2
		action="exception-remove"
		;;
	"exceptions")
		minArgs=0
		maxArgs=1
		action="exceptions"
		;;
	"unavailable")
		minArgs=0
		maxArgs=0
		action="unavailable"
		;;
	"reformat")
		minArgs=0
		maxArgs=0
		action="reformat"
		;;
	"info")
		minArgs=0
		maxArgs=0
		action="info"
		;;
	*)
		reportArgumentsError "${arg}: unknown action"
		exit "${XYZ_EXIT_BADARGS}"
		;;
	esac
done

# Early exit if no action was chosen.
test -z "${action}" && { reportArgumentsError "no action specified"; exit "${XYZ_EXIT_BADARGS}"; }

# Early exit if the number of parameters is incorrect.
test "${minArgs}" -gt 0 && test -z "$1" && { reportArgumentsError "${action}: insufficient parameters"; exit "${XYZ_EXIT_BADARGS}"; }
test "${minArgs}" -gt 1 && test -z "$2" && { reportArgumentsError "${action}: insufficient parameters"; exit "${XYZ_EXIT_BADARGS}"; }
test "${minArgs}" -eq 0 && test "${maxArgs}" -eq 0 && test -n "$1" && { reportArgumentsError "${action}: too many parameters"; exit "${XYZ_EXIT_BADARGS}"; }
test "${maxArgs}" -eq 1 && test -n "$2" && { reportArgumentsError "${action}: too many parameters"; exit "${XYZ_EXIT_BADARGS}"; }
test "${maxArgs}" -eq 2 && test -n "$3" && { reportArgumentsError "${action}: too many parameters"; exit "${XYZ_EXIT_BADARGS}"; }

# If the "--until" option was used, check its context and content.
if test -n "${untilStamp}"; then
	case "${action}" in
	"ignore"|"exception-add") ;;
	*)
		reportArgumentsError "${action}: --until not available with this action"
		exit "${XYZ_EXIT_BADARGS}"
		;;
	esac
	if printf "%s\n" "${untilStamp}" | grep -Eq -e '^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$'; then
		# Date only - add a time at the start of the day
		untilStamp="${untilStamp}T00:00:00"
	elif printf "%s\n" "${untilStamp}" | grep -Eq -e '^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]T[0-9][0-9]:[0-9][0-9]$'; then
		# Time with no seconds - set to the start of the minute
		untilStamp="${untilStamp}:00"
	elif ! printf "%s\n" "${untilStamp}" | grep -Eq -e '^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]T[0-9][0-9]:[0-9][0-9]:[0-9][0-9]$'; then
		reportArgumentsError "--until: time must be in ISO 8601 format with no time zone designator"
		exit "${XYZ_EXIT_BADARGS}"
	fi
fi

# Wait for a random amount of time, up to randomDelay seconds, in check
# mode, if a delay was given.
if test "${action}" = "check" && test "${randomDelay}" -gt 0 2>/dev/null; then
	randomSleepLength="$(awk -v "m=${randomDelay}" 'END{srand();print int(rand()*m)}' </dev/null)"
	test "${randomSleepLength}" -gt 0 2>/dev/null || randomSleepLength=1
	sleep "${randomSleepLength}"
fi

# When we started, in ISO 8601 format, for ignore and exclusion expiry.
startStamp="$(date "+%Y-%m-%dT%H:%M:%S")"

# Populate the operating system information variables "osId", "osVersion",
# "kernelName", "kernelRelease", and "osIsRhelDescendant".
osId=""
osVersion=""
osReleaseFile="${rootDir}/etc/os-release"
kernelName="$(uname -s 2>/dev/null | tr '[:upper:]' '[:lower:]')"
kernelRelease="$(uname -r 2>/dev/null | tr '[:upper:]' '[:lower:]')"
test -e "${osReleaseFile}" || osReleaseFile="${rootDir}/usr/lib/os-release"
if test -e "${osReleaseFile}"; then
	osId=$(awk -F '=' '$1=="ID"{print $2}' "${osReleaseFile}" 2>/dev/null | tr -d '"')
	osVersion=$(awk -F '=' '$1=="VERSION_ID"{print $2}' "${osReleaseFile}" 2>/dev/null | tr -d '"')
fi
if test -z "${osId}"; then
	if grep -Fq "Red Hat" "${rootDir}/etc/redhat-release" 2>/dev/null; then
		osId="rhel"
		osVersion=$(sed 's/^.* release //' "${rootDir}/etc/redhat-release" 2>/dev/null | sed 's/[^0-9].*$//')
	elif grep -Fq "CentOS" "${rootDir}/etc/redhat-release" 2>/dev/null; then
		osId="centos"
		osVersion=$(sed 's/^.* release //' "${rootDir}/etc/redhat-release" 2>/dev/null | sed 's/[^0-9].*$//')
	elif test -e "${rootDir}/etc/centos-release"; then
		osId="centos"
		osVersion=$(sed 's/^.* release //' "${rootDir}/etc/centos-release" 2>/dev/null | sed 's/[^0-9].*$//')
	fi
fi
test -z "${osId}" && osId="${kernelName}"
test -z "${osVersion}" && osVersion="${kernelRelease%%-*}"
test -z "${osId}" && osId="unknown"
test -z "${osVersion}" && osVersion="0"
osIsRhelDescendant="no"
test -e "/etc/redhat-release" && osIsRhelDescendant="yes"
case "${osId}" in
"rhel"|"centos"|"oel"|"rocky"|"almalinux") osIsRhelDescendant="yes" ;;
esac

# Set "statType" to "gnu" for GNU stat(1) or "bsd" for BSD stat(1), since
# they differ slightly.
statType="bsd"
stat --version 2>&1 | grep -Fq 'coreutils' && statType="gnu"

# Create the working directory, and set up an exit trap so it is removed on
# exit.
workDir="$(mktemp -d)" || { reportError "failed to create temporary working directory"; exit "${XYZ_EXIT_FAILURE}"; }
trap 'rm -rf "${workDir}"' EXIT

# Run the pre-action hooks.
test -n "${hookDir}" && loadScriptsInDirectory "${hookDir}/begin" "*.sh"

# Item definitions aren't needed if we are just displaying the version or
# help.
if test "${action}" = "version"; then
	. "${componentDir}/version.sh"
elif test "${action}" = "help"; then
	. "${componentDir}/help.sh"
elif test "${action}" = "info"; then
	. "${componentDir}/json.sh"
else
	# Load the item definitions.
	loadScriptsInDirectory "${componentDir}" "*.sh" "main.sh"
	test -n "${extensionDir}" && loadScriptsInDirectory "${extensionDir}" "*.sh"

	# Extract the available items from those listed; exit early if any items
	# were specified which are not defined at all.
	selectedItems=""
	if test "${action}" = "checks"; then
		# "checks" takes no arguments, so select all items.
		selectAvailableItems "all"
	elif test "${action}" = "ignore"; then
		# "ignore" takes one item, so select just that item.
		selectAvailableItems "$1"
	elif test "${action}" = "exception-add"; then
		# "exception-add" takes one item, so select just that item.
		selectAvailableItems "$1"
	elif test "${action}" = "exception-remove"; then
		# "exception-remove" takes one item, so select just that item.
		selectAvailableItems "$1"
	elif test "${action}" = "exceptions"; then
		# "exceptions" takes zero or one item.
		test -n "$1" && selectAvailableItems "$1"
	else
		selectAvailableItems "$@"
	fi
fi

# Run the appropriate action.
exitStatus=${XYZ_EXIT_NOFAULT}
faultCount=0
case "${action}" in
"help")		xyzHelp ;;
"version")	xyzVersion ;;
"check")	xyzCheck ;;
"fix")		xyzFix ;;
"checks")	xyzListChecks ;;
"ignore")	xyzIgnore "$@" ;;
"reinstate")	xyzReinstate "$@" ;;
"ignored")	xyzListIgnored ;;
"exception-add")	xyzExceptionAdd "$@" ;;
"exception-remove")	xyzExceptionRemove "$@" ;;
"exceptions")	xyzListExceptions "$@" ;;
"unavailable")	xyzListUnavailable ;;
"reformat")	xyzReformat ;;
"info")
	test "${reformatJson}" = "yes" && printf "{\n"
	xyzOutputInfo 'kernelName' "${kernelName}" ','
	xyzOutputInfo 'kernelRelease' "${kernelRelease}" ','
	xyzOutputInfo 'osId' "${osId}" ','
	xyzOutputInfo 'osVersion' "${osVersion}" ','
	xyzOutputInfo 'osIsRhelDescendant' "${osIsRhelDescendant}" ','
	xyzOutputInfo 'statType' "${statType}"
	test "${reformatJson}" = "yes" && printf "}\n"
	;;
esac

# Run the post-action hooks.
test -n "${hookDir}" && loadScriptsInDirectory "${hookDir}/end" "*.sh"

# For the check action, use the fault count as the exit status if there were
# no errors, capping the fault count at a maximum value.
if test "${action}" = "check"; then
	if test "${exitStatus}" -eq 0; then
		exitStatus=${faultCount}
		test "${exitStatus}" -gt "${XYZ_EXIT_MAXFAULT}" && exitStatus=${XYZ_EXIT_MAXFAULT}
	else
		exitStatus=${XYZ_EXIT_FAILURE}
	fi
fi

exit "${exitStatus}"
