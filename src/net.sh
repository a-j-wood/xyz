#!/bin/sh
#
# Checks and fixes relating to networking.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# Report fault $2, fix actions $3, if port $1 is listening and reachable
# from other hosts on the network.
reportFaultIfPortReachable () {
	test -e "${workDir}/listeningSockets" || netListListeningSockets > "${workDir}/listeningSockets"
	test -e "${workDir}/listeningReachableSockets" \
	|| grep -Fv -e '[::1]:' -e '127.0.0.1:' < "${workDir}/listeningSockets" > "${workDir}/listeningReachableSockets"
	grep -Eq ":$1\$" < "${workDir}/listeningReachableSockets" || return "${XYZ_RC_NOFAULT}"
	faultDescription="$2"
	fixActions="$3"
	return "${XYZ_RC_FAULT_NOFIX}"
}

registerItem "net_portreachable_21" "Check whether port 21 is reachable from other hosts" ""
check_net_portreachable_21 () { reportFaultIfPortReachable 21 "Port 21 (FTP) is open to other network hosts" "deactivate FTP services"; return $?; }

registerItem "net_portreachable_23" "Check whether port 23 is reachable from other hosts" ""
check_net_portreachable_23 () { reportFaultIfPortReachable 23 "Port 23 (TELNET) is open to other network hosts" "deactivate TELNET services"; return $?; }

registerItem "net_portreachable_25" "Check whether port 25 is reachable from other hosts" ""
check_net_portreachable_25 () { reportFaultIfPortReachable 25 "Port 25 (SMTP) is open to other network hosts" "deactivate SMTP services or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_53" "Check whether port 53 is reachable from other hosts" ""
check_net_portreachable_53 () { reportFaultIfPortReachable 53 "Port 53 (DNS) is open to other network hosts" "deactivate DNS services or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_69" "Check whether port 69 is reachable from other hosts" ""
check_net_portreachable_69 () { reportFaultIfPortReachable 69 "Port 69 (TFTP) is open to other network hosts" "deactivate TFTP services"; return $?; }

registerItem "net_portreachable_79" "Check whether port 79 is reachable from other hosts" ""
check_net_portreachable_79 () { reportFaultIfPortReachable 79 "Port 79 (Finger) is open to other network hosts" "deactivate Finger services"; return $?; }

registerItem "net_portreachable_80" "Check whether port 80 is reachable from other hosts" ""
check_net_portreachable_80 () { reportFaultIfPortReachable 80 "Port 80 (HTTP) is open to other network hosts" "deactivate HTTP services"; return $?; }

registerItem "net_portreachable_110" "Check whether port 110 is reachable from other hosts" ""
check_net_portreachable_110 () { reportFaultIfPortReachable 110 "Port 110 (POP3) is open to other network hosts" "deactivate POP3 services"; return $?; }

registerItem "net_portreachable_113" "Check whether port 113 is reachable from other hosts" ""
check_net_portreachable_113 () { reportFaultIfPortReachable 113 "Port 113 (identd) is open to other network hosts" "deactivate identd services"; return $?; }

registerItem "net_portreachable_119" "Check whether port 119 is reachable from other hosts" ""
check_net_portreachable_119 () { reportFaultIfPortReachable 119 "Port 119 (NNTP) is open to other network hosts" "deactivate NNTP services"; return $?; }

registerItem "net_portreachable_143" "Check whether port 143 is reachable from other hosts" ""
check_net_portreachable_143 () { reportFaultIfPortReachable 143 "Port 143 (IMAP) is open to other network hosts" "deactivate IMAP services"; return $?; }

registerItem "net_portreachable_443" "Check whether port 443 is reachable from other hosts" ""
check_net_portreachable_443 () { reportFaultIfPortReachable 443 "Port 443 (HTTPS) is open to other network hosts" "deactivate HTTPS services"; return $?; }

registerItem "net_portreachable_513" "Check whether port 513 is reachable from other hosts" ""
check_net_portreachable_513 () { reportFaultIfPortReachable 513 "Port 513 (rlogin, rwho) is open to other network hosts" "deactivate rlogin or rwho services"; return $?; }

registerItem "net_portreachable_563" "Check whether port 563 is reachable from other hosts" ""
check_net_portreachable_563 () { reportFaultIfPortReachable 563 "Port 563 (NNTPS) is open to other network hosts" "deactivate NNTPS services"; return $?; }

registerItem "net_portreachable_990" "Check whether port 990 is reachable from other hosts" ""
check_net_portreachable_990 () { reportFaultIfPortReachable 990 "Port 990 (FTPS) is open to other network hosts" "deactivate FTPS services"; return $?; }

registerItem "net_portreachable_992" "Check whether port 992 is reachable from other hosts" ""
check_net_portreachable_992 () { reportFaultIfPortReachable 992 "Port 992 (TELNETS) is open to other network hosts" "deactivate TELNETS services"; return $?; }

registerItem "net_portreachable_993" "Check whether port 993 is reachable from other hosts" ""
check_net_portreachable_993 () { reportFaultIfPortReachable 993 "Port 993 (IMAPS) is open to other network hosts" "deactivate IMAPS services"; return $?; }

registerItem "net_portreachable_995" "Check whether port 995 is reachable from other hosts" ""
check_net_portreachable_995 () { reportFaultIfPortReachable 995 "Port 995 (POP3S) is open to other network hosts" "deactivate POP3S services"; return $?; }

registerItem "net_portreachable_1080" "Check whether port 1080 is reachable from other hosts" ""
check_net_portreachable_1080 () { reportFaultIfPortReachable 1080 "Port 1080 (SOCKS) is open to other network hosts" "deactivate SOCKS services"; return $?; }

registerItem "net_portreachable_1194" "Check whether port 1194 is reachable from other hosts" ""
check_net_portreachable_1194 () { reportFaultIfPortReachable 1194 "Port 1194 (OpenVPN) is open to other network hosts" "deactivate OpenVPN services"; return $?; }

registerItem "net_portreachable_2049" "Check whether port 2049 is reachable from other hosts" ""
check_net_portreachable_2049 () { reportFaultIfPortReachable 2049 "Port 2049 (NFS) is open to other network hosts" "deactivate NFS services"; return $?; }

registerItem "net_portreachable_3128" "Check whether port 3128 is reachable from other hosts" ""
check_net_portreachable_3128 () { reportFaultIfPortReachable 3128 "Port 3128 (Squid HTTP proxy) is open to other network hosts" "deactivate Squid HTTP proxy services"; return $?; }

registerItem "net_portreachable_3306" "Check whether port 3306 is reachable from other hosts" ""
check_net_portreachable_3306 () { reportFaultIfPortReachable 3306 "Port 3306 (MySQL/MariaDB) is open to other network hosts" "deactivate MySQL/MariaDB services or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_5432" "Check whether port 5432 is reachable from other hosts" ""
check_net_portreachable_5432 () { reportFaultIfPortReachable 5432 "Port 5432 (PostgreSQL) is open to other network hosts" "deactivate PostgreSQL services or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_6000" "Check whether port 6000 is reachable from other hosts" ""
check_net_portreachable_6000 () { reportFaultIfPortReachable 6000 "Port 6000 (X11 :0) is open to other network hosts" "remove X11 from servers, or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_6001" "Check whether port 6001 is reachable from other hosts" ""
check_net_portreachable_6001 () { reportFaultIfPortReachable 6001 "Port 6001 (X11 :1) is open to other network hosts" "remove X11 from servers, or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_6002" "Check whether port 6002 is reachable from other hosts" ""
check_net_portreachable_6002 () { reportFaultIfPortReachable 6002 "Port 6002 (X11 :2) is open to other network hosts" "remove X11 from servers, or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_6003" "Check whether port 6003 is reachable from other hosts" ""
check_net_portreachable_6003 () { reportFaultIfPortReachable 6003 "Port 6003 (X11 :3) is open to other network hosts" "remove X11 from servers, or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_6004" "Check whether port 6004 is reachable from other hosts" ""
check_net_portreachable_6004 () { reportFaultIfPortReachable 6004 "Port 6004 (X11 :4) is open to other network hosts" "remove X11 from servers, or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_6005" "Check whether port 6005 is reachable from other hosts" ""
check_net_portreachable_6005 () { reportFaultIfPortReachable 6005 "Port 6005 (X11 :5) is open to other network hosts" "remove X11 from servers, or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_6006" "Check whether port 6006 is reachable from other hosts" ""
check_net_portreachable_6006 () { reportFaultIfPortReachable 6006 "Port 6006 (X11 :6) is open to other network hosts" "remove X11 from servers, or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_6007" "Check whether port 6007 is reachable from other hosts" ""
check_net_portreachable_6007 () { reportFaultIfPortReachable 6007 "Port 6007 (X11 :7) is open to other network hosts" "remove X11 from servers, or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_6379" "Check whether port 6379 is reachable from other hosts" ""
check_net_portreachable_6379 () { reportFaultIfPortReachable 6379 "Port 6379 (Redis) is open to other network hosts" "deactivate Redis services or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_6667" "Check whether port 6667 is reachable from other hosts" ""
check_net_portreachable_6667 () { reportFaultIfPortReachable 6667 "Port 6667 (IRC) is open to other network hosts" "deactivate IRC services"; return $?; }

registerItem "net_portreachable_6697" "Check whether port 6697 is reachable from other hosts" ""
check_net_portreachable_6697 () { reportFaultIfPortReachable 6697 "Port 6697 (IRCS) is open to other network hosts" "deactivate IRCS services"; return $?; }

registerItem "net_portreachable_7100" "Check whether port 7100 is reachable from other hosts" ""
check_net_portreachable_7100 () { reportFaultIfPortReachable 7100 "Port 7100 (X Font Service) is open to other network hosts" "remove X11 and XFS from servers, or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_8080" "Check whether port 8080 is reachable from other hosts" ""
check_net_portreachable_8080 () { reportFaultIfPortReachable 8080 "Port 8080 (web cache) is open to other network hosts" "deactivate web cache services"; return $?; }

registerItem "net_portreachable_9418" "Check whether port 9418 is reachable from other hosts" ""
check_net_portreachable_9418 () { reportFaultIfPortReachable 9418 "Port 9418 (Git) is open to other network hosts" "deactivate Git services or adjust to listen only on localhost"; return $?; }

registerItem "net_portreachable_10000" "Check whether port 10000 is reachable from other hosts" ""
check_net_portreachable_10000 () { reportFaultIfPortReachable 10000 "Port 10000 (webmin) is open to other network hosts" "deactivate webmin services"; return $?; }
