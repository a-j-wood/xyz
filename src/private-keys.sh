#!/bin/sh
#
# Checks and fixes relating to finding private keys.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# Populate ${workDir}/privateKeys/home/directories with a list of possible
# user home directories taken from /etc/passwd, if the file is not already
# present.
#
# For each home directory in the list, a subdirectory named after the line
# number is created under ${workDir}/privateKeys/home/, under which the
# "users" file contains a list of the user accounts associated with that
# home directory, one per line.
#
privateKeysListHomeDirectories () {
	test -d "${workDir}/privateKeys" || mkdir "${workDir}/privateKeys"
	test -d "${workDir}/privateKeys/home" || mkdir "${workDir}/privateKeys/home"
	test -e "${workDir}/privateKeys/home/directories" && return

	true > "${workDir}/privateKeys/home/directories"

	awk -F ':' '{print $6}' < "${rootDir}/etc/passwd" | sort -u | {
	lineNumber=0
	while read -r homeDirectory; do
		test -n "${homeDirectory}" || continue
		test -d "${rootDir}${homeDirectory}" || continue
		printf "%s\n" "${homeDirectory}" >> "${workDir}/privateKeys/home/directories"
		lineNumber=$((1+lineNumber))
		mkdir "${workDir}/privateKeys/home/${lineNumber}"
		awk -v "h=${homeDirectory}" -F ':' '$6==h{print $1}' < "${rootDir}/etc/passwd" | sort -u > "${workDir}/privateKeys/home/${lineNumber}/users"
	done
	}
}


# Expand the path $1 relative to $3, expanding a leading ~ in $1 with $2 (so
# arguments are PATH HOMEDIR CURRENTDIR).
#
privateKeysExpandRelativePath () {
	expandPattern="$1"
	expandHome="$2"
	expandCwd="$3"
	case "${expandPattern}" in
	/*)	printf "%s\n" "${expandPattern}" ;;
	\~/*)	printf "%s\n" "${expandHome}${expandPattern#\~}" ;;
	*)	printf "%s\n" "${expandCwd}/${expandPattern}" ;;
	esac
}


# For each home directory found by privateKeysListHomeDirectories, write a
# list of possible SSH private key files to a file "sshPossibleKeyFiles"
# under ${workDir}/privateKeys/home/<number>, where <number> is the line
# number of the home directory.
#
privateKeysFindIdentities () {
	{
	lineNumber=0
	while read -r homeDirectory; do
		lineNumber=$((1+lineNumber))
		test -n "${homeDirectory}" || continue
		test -d "${rootDir}${homeDirectory}" || continue

		homeInfoDir="${workDir}/privateKeys/home/${lineNumber}"

		# Find possible shell history files.
		find "${rootDir}${homeDirectory}" -mindepth 1 -maxdepth 1 -type f -name ".*history*" > "${homeInfoDir}/shellHistoryFiles" 2>/dev/null

		# Find possible SSH configuration files.
		#
		# Look in the shell history files for "ssh" or "scp"
		# commands which have used use "-F" to specify an
		# alternative SSH configuration file.  Filenames are assumed
		# to be relative to the home directory, so this may be
		# inaccurate.

		{
		while read -r shellHistoryFile; do
			test -f "${shellHistoryFile}" || continue
			grep -E '^[[:space:]]*(ssh|scp)[[:space:]]*' "${shellHistoryFile}" 2>/dev/null \
			| tr '-' '\n' | awk '/^F /{print $2}' \
			| while read -r potentialConfigFile; do
				privateKeysExpandRelativePath "${potentialConfigFile}" "${rootDir}${homeDirectory}" "${rootDir}${homeDirectory}"
			done
		done
		printf "%s/.ssh/config\n" "${rootDir}${homeDirectory}"
		} < "${homeInfoDir}/shellHistoryFiles" \
		| sort -u > "${homeInfoDir}/sshConfigFiles"

		# Expand the patterns passed to "Include" directives in the
		# SSH configuration files to extend the list of possible
		# configuration files.

		{
		while read -r sshConfigFile; do
			test -f "${sshConfigFile}" || continue
			# NB sed on OpenBSD has no "/i", hence [Ii][Nn] etc.
			sed -n 's/^[[:space:]]*[Ii][Nn][Cc][Ll][Uu][Dd][Ee][[:space:]]*//p' "${sshConfigFile}" 2>/dev/null \
			| tr ' ' '\n' | grep -E . | sort -u | {
			while read -r includePattern; do
				fullIncludePattern="$(privateKeysExpandRelativePath "${includePattern}" "${rootDir}${homeDirectory}" "${rootDir}${homeDirectory}/.ssh")"
				case "${fullIncludePattern}" in
				*\**)
					# TODO: find a safer way to expand glob patterns
					# shellcheck disable=SC2086
					find ${fullIncludePattern} -type f 2>/dev/null
					;;
				*)	printf "%s\n" "${fullIncludePattern}" ;;
				esac
			done
			}
		done
		} < "${homeInfoDir}/sshConfigFiles" > "${homeInfoDir}/sshConfigFilesPlusIncludes"
		sort -u "${homeInfoDir}/sshConfigFiles" "${homeInfoDir}/sshConfigFilesPlusIncludes" | {
		while read -r sshConfigFile; do
			test -f "${sshConfigFile}" && printf "%s\n" "${sshConfigFile}"
		done
		} > "${workDir}/sshConfigFilesThatExist"
		mv -f "${workDir}/sshConfigFilesThatExist" "${workDir}/sshConfigFiles"
		rm -f "${workDir}/sshConfigFilesPlusIncludes"

		# List all possible SSH identity files listed in the shell
		# history, the SSH configuration files, and the user's SSH
		# configuration directory.

		{
			# Look for possible SSH identity files in the shell history.
			{
			while read -r shellHistoryFile; do
				test -f "${shellHistoryFile}" || continue
				{
				grep -E '^[[:space:]]*(ssh|scp)[[:space:]]+' "${shellHistoryFile}" 2>/dev/null | tr '-' '\n' | awk '/^o IdentityFile=/{print $2}' | cut -d = -f 2
				grep -E '^[[:space:]]*(ssh|scp)[[:space:]]+' "${shellHistoryFile}" 2>/dev/null | tr '-' '\n' | awk '/^i /{print $2}'
				} | while read -r possibleKeyFile; do
					privateKeysExpandRelativePath "${possibleKeyFile}" "${rootDir}${homeDirectory}" "${rootDir}${homeDirectory}"
				done
			done
			} < "${homeInfoDir}/shellHistoryFiles"

			# Expand the arguments to IdentityFile directives in
			# the SSH configuration files.
			{
			while read -r sshConfigFile; do
				test -f "${sshConfigFile}" || continue
				# See above about "sed" and /i.
				grep -Fi 'IdentityFile' "${sshConfigFile}" 2>/dev/null \
				| sed -n 's/[[:space:]]*#.*$//;s/[[:space:]]*$//;s/^[[:space:]]*[Ii][Dd][Ee][Nn][Tt][Ii][Tt][Yy][Ff][Ii][Ll][Ee][ =]*\(.*\)[ \t]*$/\1/p' \
				| while read -r possibleKeyFile; do
					privateKeysExpandRelativePath "${possibleKeyFile}" "${rootDir}${homeDirectory}" "${rootDir}${homeDirectory}/.ssh"
				done
			done
			} < "${homeInfoDir}/sshConfigFiles"

			# Add all files in .ssh.
			find "${homeDirectory}/.ssh" -type f 2>/dev/null
		} | sort -u | {
		# Filter out the candidates that aren't regular files.
		while read -r possibleKeyFile; do
			test -f "${possibleKeyFile}" || continue
			printf "%s\n" "${possibleKeyFile}"
		done
		} > "${homeInfoDir}/sshPossibleKeyFiles"

	done
	} < "${workDir}/privateKeys/home/directories"
}


# Check private key file $1 and return 0 (true) if it is usable without a
# passphrase.
#
privateKeysIdentityHasNoPassphrase () {
	# Take a copy of the key so we can force it to have the right
	# permissions, otherwise ssh-keygen will refuse to process a file
	# that is world-readable.
	rm -f "${workDir}/privateKeys/sshPrivateKey"
	cp "$1" "${workDir}/privateKeys/sshPrivateKey" 2>/dev/null || return 1
	chmod 600 "${workDir}/privateKeys/sshPrivateKey"

	# If "ssh-keygen -y" produces any output, the key is unprotected.
	GPG_AGENT_INFO="" SSH_AGENT_PID="" ssh-keygen -P '' -y -f "${workDir}/privateKeys/sshPrivateKey" 2>/dev/null \
	| grep -Eq . \
	&& { rm -f "${workDir}/privateKeys/sshPrivateKey"; return 0; }

	rm -f "${workDir}/privateKeys/sshPrivateKey"
	return 1
}


# Look in all home directories for SSH private keys which have no passphrase
# and which may be readable to other non-root users.

registerItem \
  "ri_privkey_ssh_identity_exposed" \
  "Check for unsecured user SSH private keys with lax permissions" \
  "" \
  ssh-keygen

check_ri_privkey_ssh_identity_exposed () {
	privateKeysListHomeDirectories
	privateKeysFindIdentities

	{
	lineNumber=0
	while read -r homeDirectory; do
		lineNumber=$((1+lineNumber))
		homeInfoDir="${workDir}/privateKeys/home/${lineNumber}"
		test -s "${homeInfoDir}/sshPossibleKeyFiles" || continue
		{
		while read -r possibleKeyFile; do
			test -f "${possibleKeyFile}" || continue

			# Check the permissions of the file.

			# NB find "${possibleKeyFile}" -perm /044 would be
			# ideal, but that syntax isn't supported everywhere.

			fileMode=$(fsFileMode "${possibleKeyFile}")
			test -n "${fileMode}" || continue

			lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
			test -n "${lastOctet}" || continue
			secondLastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 2)
			test -n "${secondLastOctet}" || continue

			# Skip the file if it's not readable to group or
			# world.
			test "${lastOctet}" -lt 4 && test "${secondLastOctet}" -lt 4 && continue

			# Skip the file unless it has no passphrase.
			privateKeysIdentityHasNoPassphrase "${possibleKeyFile}" || continue

			printf "%s\n" "${possibleKeyFile}"
		done
		} < "${homeInfoDir}/sshPossibleKeyFiles" > "${homeInfoDir}/sshKeyFilesReadableAndHaveNoPassphrase"

		stripExceptions "${homeInfoDir}/sshKeyFilesReadableAndHaveNoPassphrase"

		if test -s "${homeInfoDir}/sshKeyFilesReadableAndHaveNoPassphrase"; then
			associatedUsers=$(sort -u "${homeInfoDir}/users" 2>/dev/null | tr '\n' ',' | sed 's/,$//')
			{
			while read -r unprotectedKeyFile; do
				printf "%s: %s (%s: %s)\n" "${unprotectedKeyFile}" "add passphrase and tighten file permissions" "associated users" "${associatedUsers}"
			done
			} < "${homeInfoDir}/sshKeyFilesReadableAndHaveNoPassphrase"
		fi
	done
	} < "${workDir}/privateKeys/home/directories" > "${workDir}/privateKeys/sshExposedIdentityFaults"

	test -s "${workDir}/privateKeys/sshExposedIdentityFaults" || return "${XYZ_RC_NOFAULT}"

	faultDescription="Group or world readable SSH private keys found with no passphrase"
	fixActions="$(cat "${workDir}/privateKeys/sshExposedIdentityFaults")"

	return "${XYZ_RC_FAULT_NOFIX}"
}


# Look in all home directories for SSH private keys which have no
# passphrase, regardless of their file permissions.

registerItem \
  "ri_privkey_ssh_identity_unencrypted" \
  "Check for user SSH private keys with no passphrase" \
  "" \
  ssh-keygen

check_ri_privkey_ssh_identity_unencrypted () {
	privateKeysListHomeDirectories
	privateKeysFindIdentities

	{
	lineNumber=0
	while read -r homeDirectory; do
		lineNumber=$((1+lineNumber))
		homeInfoDir="${workDir}/privateKeys/home/${lineNumber}"
		test -s "${homeInfoDir}/sshPossibleKeyFiles" || continue
		{
		while read -r possibleKeyFile; do
			test -f "${possibleKeyFile}" || continue
			# Skip the file unless it has no passphrase.
			privateKeysIdentityHasNoPassphrase "${possibleKeyFile}" || continue
			printf "%s\n" "${possibleKeyFile}"
		done
		} < "${homeInfoDir}/sshPossibleKeyFiles" > "${homeInfoDir}/sshKeyFilesWithNoPassphrase"

		stripExceptions "${homeInfoDir}/sshKeyFilesWithNoPassphrase"

		if test -s "${homeInfoDir}/sshKeyFilesWithNoPassphrase"; then
			associatedUsers=$(sort -u "${homeInfoDir}/users" 2>/dev/null | tr '\n' ',' | sed 's/,$//')
			{
			while read -r unprotectedKeyFile; do
				printf "%s: %s (%s: %s)\n" "${unprotectedKeyFile}" "add passphrase" "associated users" "${associatedUsers}"
			done
			} < "${homeInfoDir}/sshKeyFilesWithNoPassphrase"
		fi
	done
	} < "${workDir}/privateKeys/home/directories" > "${workDir}/privateKeys/sshUnprotectedIdentityFaults"

	test -s "${workDir}/privateKeys/sshUnprotectedIdentityFaults" || return "${XYZ_RC_NOFAULT}"

	faultDescription="SSH private keys found with no passphrase"
	fixActions="$(cat "${workDir}/privateKeys/sshUnprotectedIdentityFaults")"

	return "${XYZ_RC_FAULT_NOFIX}"
}


# Look in all home directories for GPG secret keys which have no passphrase.

registerItem \
  "ri_privkey_gpg_secret_key_unencrypted" \
  "Check for user GnuPG secret keys with no passphrase" \
  "" \
  gpg

check_ri_privkey_gpg_secret_key_unencrypted () {
	privateKeysListHomeDirectories

	# Check whether GPG accepts "--pinentry-mode".
	gpgPinEntryOptions=""
	LC_ALL=C gpg --pinentry-mode 2>&1 | grep -Fq 'missing argument' && gpgPinEntryOptions="--pinentry-mode loopback"

	{
	lineNumber=0
	while read -r homeDirectory; do
		lineNumber=$((1+lineNumber))
		homeInfoDir="${workDir}/privateKeys/home/${lineNumber}"
		test -d "${rootDir}${homeDirectory}/.gnupg" || continue

		# Work from a copy of the .gnupg directory so we can't
		# affect the original.
		rm -rf "${homeInfoDir}/dot-gnupg"
		cp -a "${rootDir}${homeDirectory}/.gnupg" "${homeInfoDir}/dot-gnupg" 2>/dev/null

		gpg --homedir "${homeInfoDir}/dot-gnupg" --batch --no-tty --list-secret-keys --with-colons 2>/dev/null \
		| awk -F : '$1~/^(sec|ssb)$/{print $5}' \
		| {
		while read -r gpgSecretKey; do
			# For each secret key, try encrypting a message to
			# it and decrypting using it, with no passphrase;
			# success means the key is unprotected.
			#
			# shellcheck disable=SC2086
			encryptedMessage="$(true | gpg --homedir "${homeInfoDir}/dot-gnupg" --batch --no-tty --encrypt --armor ${gpgPinEntryOptions} --passphrase-file /dev/null --recipient "${gpgSecretKey}" 2>/dev/null)"
			# shellcheck disable=SC2086
			if test -n "${encryptedMessage}" && printf "%s" "${encryptedMessage}" | LC_ALL=C gpg --homedir "${homeInfoDir}/dot-gnupg" --batch --no-tty --decrypt ${gpgPinEntryOptions} --passphrase-file /dev/null >/dev/null 2>&1; then
				printf "%s (%s): %s: %s\n" "${rootDir}${homeDirectory}" "${associatedUsers}" "${gpgSecretKey}" "add passphrase to secret key"
			fi
		done
		}
		rm -rf "${homeInfoDir}/dot-gnupg"
	done
	} < "${workDir}/privateKeys/home/directories" > "${workDir}/privateKeys/gpgUnprotectedKeyFaults"

	# No simple way to add exceptions here.

	test -s "${workDir}/privateKeys/gpgUnprotectedKeyFaults" || return "${XYZ_RC_NOFAULT}"

	faultDescription="GnuPG secret keys found with no passphrase"
	fixActions="$(cat "${workDir}/privateKeys/gpgUnprotectedKeyFaults")"

	return "${XYZ_RC_FAULT_NOFIX}"
}
