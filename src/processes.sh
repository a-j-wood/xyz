#!/bin/sh
#
# Checks and fixes relating to running processes.
#
# Copyright 2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

registerItem "proc_deleted_exe" "Check whether any processes are running deleted binaries" "" "?test -e /proc/self/exe"
check_proc_deleted_exe () {
	processList="$(
	  find /proc -mindepth 2 -maxdepth 2 \( -not -name '[0-9]*' -prune \) -name 'exe' -a -lname '* (deleted)' 2>/dev/null \
	  | cut -d / -f 3
	)"
	test -n "${processList}" || return "${XYZ_RC_NOFAULT}"
	faultDescription="Processes running executables which have since been deleted: $(printf '%s\n' "${processList}" | tr '\n' ' ')"
	fixActions="restart associated services"
	return "${XYZ_RC_FAULT_NOFIX}"
}

registerItem "proc_deleted_libs" "Check whether any processes are running from since-deleted executables or libraries" "" "?test -e /proc/self/maps"
check_proc_deleted_libs () {
	processList="$(
	  find /proc -mindepth 2 -maxdepth 2 \( -not -name '[0-9]*' -prune \) -name 'maps' -type f -exec grep -HE '^[0-9a-f-]+ ..x. .* \(deleted\)$' '{}' + 2>/dev/null \
	  | cut -d / -f 3 \
	  | uniq
	)"
	test -n "${processList}" || return "${XYZ_RC_NOFAULT}"
	faultDescription="Processes running libraries or executables which have since been deleted: $(printf '%s\n' "${processList}" | tr '\n' ' ')"
	fixActions="restart associated services"
	return "${XYZ_RC_FAULT_NOFIX}"
}

registerItem "proc_exec_anonymous" "Check whether any processes have anonymous files mapped as executable" "" "?test -e /proc/self/maps"
check_proc_exec_anonymous () {
	processList="$(
	  find /proc -mindepth 2 -maxdepth 2 \( -not -name '[0-9]*' -prune \) -name 'maps' -type f -exec grep -HE '^[0-9a-f-]+ ..x. [0-9a-f]+ [0-9:]+ [0-9]+ [[:space:]] */memfd:.* \(deleted\)$' '{}' + 2>/dev/null \
	  | cut -d / -f 3 \
	  | uniq
	)"
	test -n "${processList}" || return "${XYZ_RC_NOFAULT}"
	faultDescription="Processes with executable memory mapped from anonymous files: $(printf '%s\n' "${processList}" | tr '\n' ' ')"
	fixActions="investigate possible suspicious activity"
	return "${XYZ_RC_FAULT_NOFIX}"
}
