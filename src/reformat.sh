#!/bin/sh
#
# Reformat a fault report to make it easier to read.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# Reformat a string on standard input to a maximum width of $1 characters.
# Relies on fmtType and fmtOptions being pre-populated.
xyzStringReformat () {
	fmtMaxWidth="$1"
	test "${fmtMaxWidth}" -gt 10 || fmtMaxWidth=10
	fmtGoalWidth=$((fmtMaxWidth-1))
	if test "${fmtType}" = "gnu"; then
		fmt -g "${fmtGoalWidth}" -w "${fmtMaxWidth}"
	elif test "${fmtType}" = "bsd"; then
		if test -n "${fmtOptions}"; then
			fmt "${fmtOptions}" "${fmtGoalWidth}" "${fmtMaxWidth}"
		else
			fmt "${fmtGoalWidth}" "${fmtMaxWidth}"
		fi
	else
		# Fallback.
		fmt "${fmtGoalWidth}" "${fmtMaxWidth}" 2>/dev/null || cat
	fi
}

# Perform the "reformat" action.
#
xyzReformat () {
	outputWidth="77"
	headingWidth="15"
	if test "${reformatColour}" = "yes" && command -v tput >/dev/null 2>&1; then
		formatHeading="$(tput bold 2>/dev/null)"
		formatProblem="$(tput setaf 5 2>/dev/null; tput bold 2>/dev/null)"
		formatLowRiskFix="$(tput setaf 3 2>/dev/null)"
		formatHighRiskFix="$(tput setaf 1 2>/dev/null; tput bold 2>/dev/null)"
		formatManualFix="$(tput setaf 6 2>/dev/null; tput bold 2>/dev/null)"
		formatReset="$(tput sgr0 2>/dev/null)"
	else
		formatHeading=""
		formatProblem=""
		formatLowRiskFix=""
		formatHighRiskFix=""
		formatManualFix=""
		formatReset=""
	fi

	test "${reformatJson}" = "yes" && printf '['

	# Detect the type of "fmt" syntax to use later, since *BSD and GNU
	# use different syntax (and the BSDs differ from each other
	# slightly).
	#
	# GNU coreutils style: fmt [-g GOAL] -w MAXWIDTH (default GOAL 93%)
	# BSD style: fmt [options] GOAL MAXWIDTH
	#
	# We only run this detection if needed, i.e. if not in JSON mode.
	#
	fmtType="gnu"
	fmtOptions=""
	if ! test "${reformatJson}" = "yes"; then
		fmtOutput="$(fmt --version 2>&1)"
		if printf '%s\n' "${fmtOutput}" | grep -qF 'coreutils'; then
			fmtType="gnu"
		else
			fmtType="bsd"
			if printf '%s\n' "${fmtOutput}" | grep -qEe '-[^-]r'; then
				# NetBSD takes "-r" for "raw" processing,
				# not treating any line specially.
				fmtOptions="-r"
			elif printf '%s\n' "${fmtOutput}" | grep -qEe '-[^-]*n'; then
				# OpenBSD and FreeBSD take "-n" for "process
				# lines beginning with a dot", with similar
				# results to "-r" above.
				fmtOptions="-n"
			fi
		fi
	fi

	itemCount=0
	while IFS="$(printf "\t")" read -r faultFlag itemName faultProblem faultActions; do
		itemCount=$((1+itemCount))

		if test "${reformatJson}" = "yes"; then
			test "${itemCount}" -gt 1 && printf ','
			printf "\n"
		fi

		itemDescription=""
		itemDerivedFrom=""
		test -e "${workDir}/items/description/${itemName}" && read -r itemDescription < "${workDir}/items/description/${itemName}"
		test -n "${itemDescription}" || itemDescription="(unknown)"
		test -e "${workDir}/items/derivedFrom/${itemName}" && read -r itemDerivedFrom < "${workDir}/items/derivedFrom/${itemName}"

		case "${faultFlag}" in
		"a") faultFormat="${formatLowRiskFix}"; faultFlagDescription="Low risk automatic fix" ;;
		"R") faultFormat="${formatHighRiskFix}"; faultFlagDescription="Higher risk automatic fix" ;;
		"m") faultFormat="${formatManualFix}"; faultFlagDescription="Must be corrected manually" ;;
		"*") faultFormat=""; faultFlagDescription="(unknown flag)" ;;
		esac

		if test "${reformatJson}" = "yes"; then
			# JSON mode - perform rudimentary string escaping
			# and write a JSON object.

			printf " {\n"
			printf "  \"checkItem\": \"%s\",\n" "$(xyzJsonEscapeString "${itemName}")"
			printf "  \"itemDescription\": \"%s\",\n" "$(xyzJsonEscapeString "${itemDescription}")"
			printf "  \"derivedFrom\": \"%s\",\n" "$(xyzJsonEscapeString "${itemDerivedFrom}")"
			printf "  \"problem\": \"%s\",\n" "$(xyzJsonEscapeString "${faultProblem}")"
			printf "  \"fixTypeFlag\": \"%s\",\n" "${faultFlag}"
			printf "  \"fixTypeDescription\": \"%s\",\n" "$(xyzJsonEscapeString "${faultFlagDescription}")"
			printf "  \"actions\": ["
			printf "%s\n" "${faultActions}" | tr '|' '\n' | {
				actionCount=0
				while read -r faultAction; do
					actionCount=$((1+actionCount))
					test "${actionCount}" -gt 1 && printf ','
					printf "\n   "
					printf "\"%s\"" "$(xyzJsonEscapeString "${faultAction}")"
				done
			}
			printf "\n  ]\n"
			printf " }"

			continue
		fi

		# Reformat the item description and problem so that they fit
		# the width, indenting overflow lines so they start in the
		# right place since the first line is prefixed by a heading.

		itemDescription="$(printf "%s\n" "${itemDescription}" | xyzStringReformat $((outputWidth-headingWidth-2)) | sed '2,$s/^/ '"$(printf "%${headingWidth}s" "")"'/')"
		faultProblem="$(printf "%s\n" "${faultProblem}" | xyzStringReformat $((outputWidth-headingWidth-2)) | sed '2,$s/^/ '"$(printf "%${headingWidth}s" "")"'/')"

		printf "%${outputWidth}s\n" "" | tr ' ' '*'
		printf "%s%-${headingWidth}s%s %s\n" "${formatHeading}" "Item name:" "${formatReset}" "${itemName}"
		printf "%s%-${headingWidth}s%s %s\n" "${formatHeading}" "Description:" "${formatReset}" "${itemDescription}"
		test -n "${itemDerivedFrom}" && printf "%s%-${headingWidth}s%s %s\n" "${formatHeading}" "Derived from:" "${formatReset}" "${itemDerivedFrom}"
		printf "%s%-${headingWidth}s%s %s%s%s\n" "${formatHeading}" "Fault found:" "${formatReset}" "${formatProblem}" "${faultProblem}" "${formatReset}"
		printf "%s%-${headingWidth}s%s (%s): %s%s%s\n" "${formatHeading}" "Fix type:" "${formatReset}" "${faultFlag}" "${faultFormat}" "${faultFlagDescription}" "${formatReset}"
		printf "\n%s%s%s\n\n" "${formatHeading}" "Actions:" "${formatReset}"
		printf "%s\n" "${faultActions}" | tr '|' '\n' | while read -r faultAction; do
			printf "%s\n" "${faultAction}" | fmt -w "${outputWidth}" | sed '1s/^/ * /;2,$s/^/   /'
		done
		printf "\n"
	done

	test "${reformatJson}" = "yes" && printf "\n]\n"
}
