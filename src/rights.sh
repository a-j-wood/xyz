#!/bin/sh
#
# Checks and fixes relating to file and directory permissions and ownership.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# Check whether path $2 is owned by one of the accounts named in the
# space-separated list $1, setting the appropriate variables and returning
# the relevant return code.  The return code for failure is
# XYZ_RC_FAULT_HIGHRISK unless it is provided in $3.
#
rightsCheckOwnerAccountName () {
	test -e "$2" || return "${XYZ_RC_NOFAULT}"
	pathOwner="$(fsOwnerAccountName "$2")"
	firstName=""
	for checkName in $1; do
		test "${checkName}" = "${pathOwner}" && return "${XYZ_RC_NOFAULT}"
		test -n "${firstName}" || firstName="${checkName}"
	done
	pathType="File"
	test -d "$2" && pathType="Directory"
	if test "$1" = "${firstName}"; then
		faultDescription="${pathType} \"$2\" is not owned by user \"$1\""
	else
		faultDescription="${pathType} \"$2\" is not owned by any of these users: $1"
	fi
	fixActions="chown ${firstName} $2"
	test -n "$3" && return "$3"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}


# Check whether path $2 is owned by one of the groups named in the
# space-separated list $1, setting the appropriate variables and returning
# the relevant return code.  The return code for failure is
# XYZ_RC_FAULT_HIGHRISK unless it is provided in $3.
#
rightsCheckOwnerGroupName () {
	test -e "$2" || return "${XYZ_RC_NOFAULT}"
	pathOwner="$(fsOwnerGroupName "$2")"
	firstName=""
	for checkName in $1; do
		test "${checkName}" = "${pathOwner}" && return "${XYZ_RC_NOFAULT}"
		test -n "${firstName}" || firstName="${checkName}"
	done
	pathType="File"
	test -d "$2" && pathType="Directory"
	if test "$1" = "${firstName}"; then
		faultDescription="${pathType} \"$2\" is not owned by group \"$1\""
	else
		faultDescription="${pathType} \"$2\" is not owned by any of these groups: $1"
	fi
	fixActions="chown :${firstName} $2"
	test -n "$3" && return "$3"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}


# Check whether path $2 is owned by UID $1, setting the appropriate
# variables and returning the relevant return code.  The return code for
# failure is XYZ_RC_FAULT_HIGHRISK unless it is provided in $3.
#
rightsCheckOwnerUid () {
	test -e "$2" || return "${XYZ_RC_NOFAULT}"
	test "$(fsOwnerUid "$2")" = "$1" && return "${XYZ_RC_NOFAULT}"
	pathType="File"
	test -d "$2" && pathType="Directory"
	faultDescription="${pathType} \"$2\" is not owned by UID $1"
	fixActions="chown $1 $2"
	test -n "$3" && return "$3"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}


# Check whether path $2 is owned by GID $1, setting the appropriate
# variables and returning the relevant return code.  The return code for
# failure is XYZ_RC_FAULT_HIGHRISK unless it is provided in $3.
#
rightsCheckOwnerGid () {
	test -e "$2" || return "${XYZ_RC_NOFAULT}"
	test "$(fsOwnerGid "$2")" = "$1" && return "${XYZ_RC_NOFAULT}"
	pathType="File"
	test -d "$2" && pathType="Directory"
	faultDescription="${pathType} \"$2\" is not owned by GID $1"
	fixActions="chown :$1 $2"
	test -n "$3" && return "$3"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}


# Check whether path $2 has the exact octal file mode $1, setting the
# appropriate variables and returning the relevant return code.  The return
# code for failure is XYZ_RC_FAULT_HIGHRISK unless it is provided in $3.
#
rightsCheckExactPermissions () {
	test -e "$2" || return "${XYZ_RC_NOFAULT}"
	actualFileMode="$(fsFileMode "$2")"
	test "${actualFileMode}" = "$1" && return "${XYZ_RC_NOFAULT}"
	test "0${actualFileMode}" = "$1" && return "${XYZ_RC_NOFAULT}"
	pathType="File"
	test -d "$2" && pathType="Directory"
	faultDescription="${pathType} \"$2\" does not have file mode $1"
	fixActions="chmod $1 $2"
	test -n "$3" && return "$3"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}


registerItem "rights_tmp_owning_user" "Check /tmp is owned by the root user" ""
check_rights_tmp_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/tmp" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_tmp_owning_user () { chown root "${rootDir}/tmp" || { reportError "failed to correct /tmp owning user"; return 1; }; return 0; }

registerItem "rights_tmp_owning_group" "Check /tmp is owned by GID 0" ""
check_rights_tmp_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/tmp" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_tmp_owning_group () { chown :0 "${rootDir}/tmp" || { reportError "failed to correct /tmp owning group"; return 1; }; return 0; }

registerItem "rights_tmp_permissions" "Check /tmp has the correct file mode" ""
check_rights_tmp_permissions () { rightsCheckExactPermissions "1777" "${rootDir}/tmp" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_tmp_permissions () { chmod 1777 "${rootDir}/tmp" || { reportError "failed to correct /tmp file mode"; return 1; }; return 0; }

registerItem "rights_etccrontab_owning_user" "Check /etc/crontab is owned by the root user" "CIS-Debian12-2.4.1.2"
check_rights_etccrontab_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/crontab" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccrontab_owning_user () { chown root "${rootDir}/etc/crontab" || { reportError "failed to correct /etc/crontab owning user"; return 1; }; return 0; }

registerItem "rights_etccrontab_owning_group" "Check /etc/crontab is owned by GID 0" "CIS-Debian12-2.4.1.2"
check_rights_etccrontab_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/crontab" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccrontab_owning_group () { chown :0 "${rootDir}/etc/crontab" || { reportError "failed to correct /etc/crontab owning group"; return 1; }; return 0; }

registerItem "rights_etccrontab_permissions" "Check /etc/crontab has the correct file mode" "CIS-Debian12-2.4.1.2"
check_rights_etccrontab_permissions () { rightsCheckExactPermissions "600" "${rootDir}/etc/crontab" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccrontab_permissions () { chmod 600 "${rootDir}/etc/crontab" || { reportError "failed to correct /etc/crontab file mode"; return 1; }; return 0; }

registerItem "rights_etccronhourly_owning_user" "Check /etc/cron.hourly is owned by the root user" "CIS-Debian12-2.4.1.3"
check_rights_etccronhourly_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/cron.hourly" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccronhourly_owning_user () { chown root "${rootDir}/etc/cron.hourly" || { reportError "failed to correct /etc/cron.hourly owning user"; return 1; }; return 0; }

registerItem "rights_etccronhourly_owning_group" "Check /etc/cron.hourly is owned by GID 0" "CIS-Debian12-2.4.1.3"
check_rights_etccronhourly_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/cron.hourly" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccronhourly_owning_group () { chown :0 "${rootDir}/etc/cron.hourly" || { reportError "failed to correct /etc/cron.hourly owning group"; return 1; }; return 0; }

registerItem "rights_etccronhourly_permissions" "Check /etc/cron.hourly has the correct file mode" "CIS-Debian12-2.4.1.3"
check_rights_etccronhourly_permissions () { rightsCheckExactPermissions "700" "${rootDir}/etc/cron.hourly" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccronhourly_permissions () { chmod 700 "${rootDir}/etc/cron.hourly" || { reportError "failed to correct /etc/cron.hourly file mode"; return 1; }; return 0; }

registerItem "rights_etccrondaily_owning_user" "Check /etc/cron.daily is owned by the root user" "CIS-Debian12-2.4.1.4"
check_rights_etccrondaily_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/cron.daily" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccrondaily_owning_user () { chown root "${rootDir}/etc/cron.daily" || { reportError "failed to correct /etc/cron.daily owning user"; return 1; }; return 0; }

registerItem "rights_etccrondaily_owning_group" "Check /etc/cron.daily is owned by GID 0" "CIS-Debian12-2.4.1.4"
check_rights_etccrondaily_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/cron.daily" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccrondaily_owning_group () { chown :0 "${rootDir}/etc/cron.daily" || { reportError "failed to correct /etc/cron.daily owning group"; return 1; }; return 0; }

registerItem "rights_etccrondaily_permissions" "Check /etc/cron.daily has the correct file mode" "CIS-Debian12-2.4.1.4"
check_rights_etccrondaily_permissions () { rightsCheckExactPermissions "700" "${rootDir}/etc/cron.daily" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccrondaily_permissions () { chmod 700 "${rootDir}/etc/cron.daily" || { reportError "failed to correct /etc/cron.daily file mode"; return 1; }; return 0; }

registerItem "rights_etccronweekly_owning_user" "Check /etc/cron.weekly is owned by the root user" "CIS-Debian12-2.4.1.5"
check_rights_etccronweekly_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/cron.weekly" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccronweekly_owning_user () { chown root "${rootDir}/etc/cron.weekly" || { reportError "failed to correct /etc/cron.weekly owning user"; return 1; }; return 0; }

registerItem "rights_etccronweekly_owning_group" "Check /etc/cron.weekly is owned by GID 0" "CIS-Debian12-2.4.1.5"
check_rights_etccronweekly_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/cron.weekly" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccronweekly_owning_group () { chown :0 "${rootDir}/etc/cron.weekly" || { reportError "failed to correct /etc/cron.weekly owning group"; return 1; }; return 0; }

registerItem "rights_etccronweekly_permissions" "Check /etc/cron.weekly has the correct file mode" "CIS-Debian12-2.4.1.5"
check_rights_etccronweekly_permissions () { rightsCheckExactPermissions "700" "${rootDir}/etc/cron.weekly" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccronweekly_permissions () { chmod 700 "${rootDir}/etc/cron.weekly" || { reportError "failed to correct /etc/cron.weekly file mode"; return 1; }; return 0; }

registerItem "rights_etccronmonthly_owning_user" "Check /etc/cron.monthly is owned by the root user" "CIS-Debian12-2.4.1.6"
check_rights_etccronmonthly_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/cron.monthly" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccronmonthly_owning_user () { chown root "${rootDir}/etc/cron.monthly" || { reportError "failed to correct /etc/cron.monthly owning user"; return 1; }; return 0; }

registerItem "rights_etccronmonthly_owning_group" "Check /etc/cron.monthly is owned by GID 0" "CIS-Debian12-2.4.1.6"
check_rights_etccronmonthly_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/cron.monthly" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccronmonthly_owning_group () { chown :0 "${rootDir}/etc/cron.monthly" || { reportError "failed to correct /etc/cron.monthly owning group"; return 1; }; return 0; }

registerItem "rights_etccronmonthly_permissions" "Check /etc/cron.monthly has the correct file mode" "CIS-Debian12-2.4.1.6"
check_rights_etccronmonthly_permissions () { rightsCheckExactPermissions "700" "${rootDir}/etc/cron.monthly" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccronmonthly_permissions () { chmod 700 "${rootDir}/etc/cron.monthly" || { reportError "failed to correct /etc/cron.monthly file mode"; return 1; }; return 0; }

registerItem "rights_etccrond_owning_user" "Check /etc/cron.d is owned by the root user" "CIS-Debian12-2.4.1.7"
check_rights_etccrond_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/cron.d" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccrond_owning_user () { chown root "${rootDir}/etc/cron.d" || { reportError "failed to correct /etc/cron.d owning user"; return 1; }; return 0; }

registerItem "rights_etccrond_owning_group" "Check /etc/cron.d is owned by GID 0" "CIS-Debian12-2.4.1.7"
check_rights_etccrond_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/cron.d" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccrond_owning_group () { chown :0 "${rootDir}/etc/cron.d" || { reportError "failed to correct /etc/cron.d owning group"; return 1; }; return 0; }

registerItem "rights_etccrond_permissions" "Check /etc/cron.d has the correct file mode" "CIS-Debian12-2.4.1.7"
check_rights_etccrond_permissions () { rightsCheckExactPermissions "700" "${rootDir}/etc/cron.d" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etccrond_permissions () { chmod 700 "${rootDir}/etc/cron.d" || { reportError "failed to correct /etc/cron.d file mode"; return 1; }; return 0; }


registerItem \
  "rights_sshd_config" \
  "Check SSH daemon config files have correct ownership and permissions" \
  "CIS-Debian12-5.1.1"

check_rights_sshd_config () {
	find "${rootDir}/etc/ssh/sshd_config.d" -name "*.conf" -type f 2>/dev/null > "${workDir}/sshConfigFiles"
	printf "%s\n" "${rootDir}/etc/ssh/sshd_config" >> "${workDir}/sshConfigFiles"
	{
	while read -r sshConfigFile; do
		test -e "${sshConfigFile}" || continue
		test "$(fsOwnerUid "${sshConfigFile}")" = "0" || printf "chown 0 %s\n" "${sshConfigFile}"
		test "$(fsOwnerGid "${sshConfigFile}")" = "0" || printf "chown :0 %s\n" "${sshConfigFile}"
		actualFileMode="$(fsFileMode "${sshConfigFile}")"
		test "${actualFileMode}" = "600" && continue
		test "${actualFileMode}" = "0600" && continue
		test "${actualFileMode}" = "400" && continue
		test "${actualFileMode}" = "0400" && continue
		printf "chmod 600 %s\n" "${sshConfigFile}"
	done
	} < "${workDir}/sshConfigFiles" > "${workDir}/sshConfigFiles.fixactions"
	test -s "${workDir}/sshConfigFiles.fixactions" || return "${XYZ_RC_NOFAULT}"
	faultDescription="SSH daemon config files have incorrect ownership or permissions"
	fixActions="$(cat "${workDir}/sshConfigFiles.fixactions")"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}

fix_rights_sshd_config () {
	find "${rootDir}/etc/ssh/sshd_config.d" -name "*.conf" -type f 2>/dev/null > "${workDir}/sshConfigFiles"
	printf "%s\n" "${rootDir}/etc/ssh/sshd_config" >> "${workDir}/sshConfigFiles"
	{
	while read -r sshConfigFile; do
		test -e "${sshConfigFile}" || continue
		test "$(fsOwnerUid "${sshConfigFile}")" = "0" || { chown 0 "${sshConfigFile}" || { reportError "${sshConfigFile}: failed to correct owning user"; return 1; }; }
		test "$(fsOwnerGid "${sshConfigFile}")" = "0" || { chown :0 "${sshConfigFile}" || { reportError "${sshConfigFile}: failed to correct owning group"; return 1; }; }
		actualFileMode="$(fsFileMode "${sshConfigFile}")"
		test "${actualFileMode}" = "600" && continue
		test "${actualFileMode}" = "0600" && continue
		test "${actualFileMode}" = "400" && continue
		test "${actualFileMode}" = "0400" && continue
		chmod 600 "${sshConfigFile}" || { reportError "${sshConfigFile}: failed to correct file mode"; return 1; }
	done
	} < "${workDir}/sshConfigFiles"
	return 0
}

sshListHostPrivateKeys () {
	find "${rootDir}/etc/ssh" -xdev -type f \
	| {
	while read -r sshPrivateKeyFile; do
		test -n "${sshPrivateKeyFile}" || continue
		grep -Eq '^[^#]*BEGIN [^#]* PRIVATE KEY' "${sshPrivateKeyFile}" 2>/dev/null || continue
		printf "%s\n" "${sshPrivateKeyFile}"
	done
	}
}

registerItem "rights_sshd_hostkeys_private_owning_user" "Check SSH host private key owning user" "CIS-Debian12-5.1.2"
check_rights_sshd_hostkeys_private_owning_user () {
	test -s "${workDir}/sshdHostPrivateKeyFiles" || sshListHostPrivateKeys > "${workDir}/sshdHostPrivateKeyFiles"
	{
	while read -r sshPrivateKeyFile; do
		rightsCheckOwnerAccountName "root" "${sshPrivateKeyFile}" "${XYZ_RC_FAULT_HIGHRISK}" && continue
		printf "%s\n" "${sshPrivateKeyFile}"
	done
	} < "${workDir}/sshdHostPrivateKeyFiles" > "${workDir}/sshdHostPrivateKeyFiles.wronguser"

	test -s "${workDir}/sshdHostPrivateKeyFiles.wronguser" || return "${XYZ_RC_NOFAULT}"

	faultDescription="SSH host private key files are owned by the wrong user"
	fixActions="$(sed "s/^/chown root /" "${workDir}/sshdHostPrivateKeyFiles.wronguser")"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_rights_sshd_hostkeys_private_owning_user () {
	rc=0
	{
	while read -r sshPrivateKeyFile; do
		chown "root" "${sshPrivateKeyFile}" || { reportError "failed to correct owning user of ${sshPrivateKeyFile}"; rc=1; }
	done
	} < "${workDir}/sshdHostPrivateKeyFiles.wronguser"
	return "${rc}"
}

registerItem "rights_sshd_hostkeys_private_owning_group" "Check SSH host private key owning group" "CIS-Debian12-5.1.2"
check_rights_sshd_hostkeys_private_owning_group () {
	test -s "${workDir}/sshdHostPrivateKeyFiles" || sshListHostPrivateKeys > "${workDir}/sshdHostPrivateKeyFiles"

	sshKeyOwningGroup="root"
	grep -Eq "^ssh_keys:" "${rootDir}/etc/group" 2>/dev/null && sshKeyOwningGroup="ssh_keys"
	grep -Eq "^_ssh:" "${rootDir}/etc/group" 2>/dev/null && sshKeyOwningGroup="_ssh"
	grep -Eq "^ssh:" "${rootDir}/etc/group" 2>/dev/null && sshKeyOwningGroup="ssh"

	{
	while read -r sshPrivateKeyFile; do
		rightsCheckOwnerGroupName "root ssh_keys _ssh ssh" "${sshPrivateKeyFile}" "${XYZ_RC_FAULT_HIGHRISK}" && continue
		printf "%s\n" "${sshPrivateKeyFile}"
	done
	} < "${workDir}/sshdHostPrivateKeyFiles" > "${workDir}/sshdHostPrivateKeyFiles.wronggroup"

	test -s "${workDir}/sshdHostPrivateKeyFiles.wronggroup" || return "${XYZ_RC_NOFAULT}"

	faultDescription="SSH host private key files are owned by the wrong group"
	fixActions="$(sed "s/^/chown :${sshKeyOwningGroup} /" "${workDir}/sshdHostPrivateKeyFiles.wronggroup")"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_rights_sshd_hostkeys_private_owning_group () {
	rc=0
	{
	while read -r sshPrivateKeyFile; do
		test -n "${sshPrivateKeyFile}" || continue
		chown ":${sshKeyOwningGroup}" "${sshPrivateKeyFile}" || { reportError "failed to correct owning group of ${sshPrivateKeyFile}"; rc=1; }
	done
	} < "${workDir}/sshdHostPrivateKeyFiles.wronggroup"
	return "${rc}"
}

registerItem "rights_sshd_hostkeys_private_permissions" "Check SSH host private keys have the correct file mode" "CIS-Debian12-5.1.2"
check_rights_sshd_hostkeys_private_permissions () {
	test -s "${workDir}/sshdHostPrivateKeyFiles" || sshListHostPrivateKeys > "${workDir}/sshdHostPrivateKeyFiles"
	{
	while read -r sshPrivateKeyFile; do
		if fsGroupAndOtherCanRead "${sshPrivateKeyFile}" || fsGroupOrOtherCanDoMoreThanRead "${sshPrivateKeyFile}"; then
			printf "%s\n" "${sshPrivateKeyFile}"
		fi
	done
	} < "${workDir}/sshdHostPrivateKeyFiles" > "${workDir}/sshdHostPrivateKeyFiles.wrongpermissions"

	test -s "${workDir}/sshdHostPrivateKeyFiles.wrongpermissions" || return "${XYZ_RC_NOFAULT}"

	faultDescription="SSH host private key files have the wrong file mode"
	fixActions="$(sed "s/^/chmod 600 /" "${workDir}/sshdHostPrivateKeyFiles.wrongpermissions")"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_rights_sshd_hostkeys_private_permissions () {
	rc=0
	{
	while read -r sshPrivateKeyFile; do
		test -n "${sshPrivateKeyFile}" || continue
		chmod 600 "${sshPrivateKeyFile}" || { reportError "failed to correct file mode of ${sshPrivateKeyFile}"; rc=1; }
	done
	} < "${workDir}/sshdHostPrivateKeyFiles.wrongpermissions"
	return "${rc}"
}

registerItem "rights_sshd_hostkeys_public_owning_user" "Check SSH host public key owning user" "CIS-Debian12-5.1.3"
check_rights_sshd_hostkeys_public_owning_user () {
	test -s "${workDir}/sshdHostPrivateKeyFiles" || sshListHostPrivateKeys > "${workDir}/sshdHostPrivateKeyFiles"
	{
	while read -r sshPrivateKeyFile; do
		rightsCheckOwnerAccountName "root" "${sshPrivateKeyFile}.pub" "${XYZ_RC_FAULT_HIGHRISK}" && continue
		printf "%s\n" "${sshPrivateKeyFile}.pub"
	done
	} < "${workDir}/sshdHostPrivateKeyFiles" > "${workDir}/sshdHostPublicKeyFiles.wronguser"

	test -s "${workDir}/sshdHostPublicKeyFiles.wronguser" || return "${XYZ_RC_NOFAULT}"

	faultDescription="SSH host public key files are owned by the wrong user"
	fixActions="$(sed "s/^/chown root /" "${workDir}/sshdHostPublicKeyFiles.wronguser")"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_rights_sshd_hostkeys_public_owning_user () {
	rc=0
	{
	while read -r sshPublicKeyFile; do
		chown "root" "${sshPublicKeyFile}" || { reportError "failed to correct owning user of ${sshPublicKeyFile}"; rc=1; }
	done
	} < "${workDir}/sshdHostPublicKeyFiles.wronguser"
	return "${rc}"
}

registerItem "rights_sshd_hostkeys_public_owning_group" "Check SSH host public key owning group" "CIS-Debian12-5.1.3"
check_rights_sshd_hostkeys_public_owning_group () {
	test -s "${workDir}/sshdHostPrivateKeyFiles" || sshListHostPrivateKeys > "${workDir}/sshdHostPrivateKeyFiles"
	{
	while read -r sshPrivateKeyFile; do
		rightsCheckOwnerGid "0" "${sshPrivateKeyFile}.pub" "${XYZ_RC_FAULT_HIGHRISK}" && continue
		printf "%s\n" "${sshPrivateKeyFile}.pub"
	done
	} < "${workDir}/sshdHostPrivateKeyFiles" > "${workDir}/sshdHostPublicKeyFiles.wronggroup"

	test -s "${workDir}/sshdHostPublicKeyFiles.wronggroup" || return "${XYZ_RC_NOFAULT}"

	faultDescription="SSH host public key files are owned by the wrong group"
	fixActions="$(sed "s/^/chown :0 /" "${workDir}/sshdHostPublicKeyFiles.wronggroup")"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_rights_sshd_hostkeys_public_owning_group () {
	rc=0
	{
	while read -r sshPublicKeyFile; do
		test -n "${sshPublicKeyFile}" || continue
		chown ":0" "${sshPublicKeyFile}" || { reportError "failed to correct owning group of ${sshPublicKeyFile}"; rc=1; }
	done
	} < "${workDir}/sshdHostPublicKeyFiles.wronggroup"
	return "${rc}"
}

registerItem "rights_sshd_hostkeys_public_permissions" "Check SSH host public keys have the correct file mode" "CIS-Debian12-5.1.3"
check_rights_sshd_hostkeys_public_permissions () {
	test -s "${workDir}/sshdHostPrivateKeyFiles" || sshListHostPrivateKeys > "${workDir}/sshdHostPrivateKeyFiles"
	{
	while read -r sshPrivateKeyFile; do
		if fsGroupOrOtherCanDoMoreThanRead "${sshPrivateKeyFile}.pub"; then
			printf "%s\n" "${sshPrivateKeyFile}.pub"
		fi
	done
	} < "${workDir}/sshdHostPrivateKeyFiles" > "${workDir}/sshdHostPublicKeyFiles.wrongpermissions"

	test -s "${workDir}/sshdHostPublicKeyFiles.wrongpermissions" || return "${XYZ_RC_NOFAULT}"

	faultDescription="SSH host public key files have the wrong file mode"
	fixActions="$(sed "s/^/chmod 644 /" "${workDir}/sshdHostPublicKeyFiles.wrongpermissions")"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_rights_sshd_hostkeys_public_permissions () {
	rc=0
	{
	while read -r sshPublicKeyFile; do
		test -n "${sshPublicKeyFile}" || continue
		chmod 644 "${sshPublicKeyFile}" || { reportError "failed to correct file mode of ${sshPublicKeyFile}"; rc=1; }
	done
	} < "${workDir}/sshdHostPublicKeyFiles.wrongpermissions"
	return "${rc}"
}

# Write a list of possible audit daemon log files to a working file
# "auditDaemonLogFiles", and the directory name to "auditDaemonLogDir".
enumerateAuditDaemonLogFiles () {
	test -e "${workDir}/auditDaemonLogFiles" && return
	true > "${workDir}/auditDaemonLogFiles"
	true > "${workDir}/auditDaemonLogDir"
	test -e "${rootDir}/etc/audit/auditd.conf" || return
	auditLogFileSetting=$(sed -n '/^[[:space:]]*log_file[[:space:]]*=[[:space:]]*/s///p' "${rootDir}/etc/audit/auditd.conf" 2>/dev/null)
	auditLogDir="${auditLogFileSetting%/*}"
	if test -n "${auditLogDir}"; then
		printf "%s\n" "${auditLogDir}" > "${workDir}/auditDaemonLogDir"
		find "${auditLogDir}" -type f > "${workDir}/auditDaemonLogFiles" 2>/dev/null
	fi
}

# Write a list of audit daemon config and rules files to a working file
# "auditDaemonConfigFiles".
enumerateAuditDaemonConfigFiles () {
	test -e "${workDir}/auditDaemonConfigFiles" && return
	find "${rootDir}/etc/audit" -type f \( -name "*.conf" -o -name "*.rules" \) > "${workDir}/auditDaemonConfigFiles" 2>/dev/null
}

registerItem "rights_auditd_logfile_owning_user" "Check audit logs are owned by the root user" "CIS-Debian12-6.4.4.2"
check_rights_auditd_logfile_owning_user () {
	enumerateAuditDaemonLogFiles
	{
	while read -r auditDaemonLogFile; do
		test -e "${auditDaemonLogFile}" || continue
		test "$(fsOwnerUid "${auditDaemonLogFile}")" = "0" && continue
		printf "%s %s\n" "chown 0" "${auditDaemonLogFile}"
	done
	} < "${workDir}/auditDaemonLogFiles" > "${workDir}/rights_auditd_logfile_owning_user.fixactions"
	test -s "${workDir}/rights_auditd_logfile_owning_user.fixactions" || return "${XYZ_RC_NOFAULT}"
	faultDescription="Audit daemon log files are not owned by the root user"
	fixActions="$(cat "${workDir}/rights_auditd_logfile_owning_user.fixactions")"
	return "${XYZ_RC_FAULT_LOWRISK}"
}
fix_rights_auditd_logfile_owning_user () {
	{
	# shellcheck disable=SC2034
	while read -r dummy1 dummy2 auditDaemonLogFile; do
		chown 0 "${auditDaemonLogFile}" || { reportError "failed to correct owning user of ${auditDaemonLogFile}"; return 1; };
	done
	} < "${workDir}/rights_auditd_logfile_owning_user.fixactions"
	return 0
}

registerItem "rights_auditd_logfile_defined_group" "Check audit log group is configured correctly" "CIS-Debian12-6.4.4.3"
check_rights_auditd_logfile_defined_group () {
	test -e "${rootDir}/etc/audit/auditd.conf" || return "${XYZ_RC_NOFAULT}"
	auditLogGroupSetting="$(sed -n '/^[[:space:]]*log_group[[:space:]]*=[[:space:]]*/s///p' "${rootDir}/etc/audit/auditd.conf" 2>/dev/null)"
	test -n "${auditLogGroupSetting}" || return "${XYZ_RC_NOFAULT}"
	test "root" = "${auditLogGroupSetting}" && return "${XYZ_RC_NOFAULT}"
	test "adm" = "${auditLogGroupSetting}" && return "${XYZ_RC_NOFAULT}"
	faultDescription="Audit daemon 'log_group' setting is not 'root' or 'adm'"
	fixActions="set 'log_group' to 'root' or 'adm' in /etc/audit/auditd.cond"
	return "${XYZ_RC_FAULT_NOFIX}"
}

registerItem "rights_auditd_logfile_owning_group" "Check audit logs are owned by the correct group" "CIS-Debian12-6.4.4.3"
check_rights_auditd_logfile_owning_group () {
	test -e "${rootDir}/etc/audit/auditd.conf" || return "${XYZ_RC_NOFAULT}"
	auditLogGroupSetting="$(sed -n '/^[[:space:]]*log_group[[:space:]]*=[[:space:]]*/s///p' "${rootDir}/etc/audit/auditd.conf" 2>/dev/null)"
	enumerateAuditDaemonLogFiles
	{
	while read -r auditDaemonLogFile; do
		test -e "${auditDaemonLogFile}" || continue
		test "a$(fsOwnerGroupName "${auditDaemonLogFile}")" = "a${auditLogGroupSetting}" && continue
		printf "%s :%s %s\n" "chown" "${auditLogGroupSetting}" "${auditDaemonLogFile}"
	done
	} < "${workDir}/auditDaemonLogFiles" > "${workDir}/rights_auditd_logfile_owning_group.fixactions"
	test -s "${workDir}/rights_auditd_logfile_owning_group.fixactions" || return "${XYZ_RC_NOFAULT}"
	faultDescription="Audit daemon log files are not owned by the '${auditLogGroupSetting}' group"
	fixActions="$(cat "${workDir}/rights_auditd_logfile_owning_group.fixactions")"
	return "${XYZ_RC_FAULT_LOWRISK}"
}
fix_rights_auditd_logfile_owning_group () {
	{
	# shellcheck disable=SC2034
	while read -r dummy1 dummy2 auditDaemonLogFile; do
		chown ":${auditLogGroupSetting}" "${auditDaemonLogFile}" || { reportError "failed to correct owning group of ${auditDaemonLogFile}"; return 1; };
	done
	} < "${workDir}/rights_auditd_logfile_owning_group.fixactions"
	return 0
}

registerItem "rights_auditd_logfile_permissions" "Check audit logs have the correct file mode" "CIS-Debian12-6.4.4.1"
check_rights_auditd_logfile_permissions () {
	enumerateAuditDaemonLogFiles
	{
	while read -r auditDaemonLogFile; do
		test -e "${auditDaemonLogFile}" || continue
		if fsOtherCanRead "${auditDaemonLogFile}" || fsGroupOrOtherCanDoMoreThanRead "${auditDaemonLogFile}"; then
			printf "%s %s\n" "chmod 640" "${auditDaemonLogFile}"
		fi
	done
	} < "${workDir}/auditDaemonLogFiles" > "${workDir}/rights_auditd_logfile_permissions.fixactions"
	test -s "${workDir}/rights_auditd_logfile_permissions.fixactions" || return "${XYZ_RC_NOFAULT}"
	faultDescription="Audit daemon log files do not have the correct file mode"
	fixActions="$(cat "${workDir}/rights_auditd_logfile_permissions.fixactions")"
	return "${XYZ_RC_FAULT_LOWRISK}"
}
fix_rights_auditd_logfile_permissions () {
	{
	# shellcheck disable=SC2034
	while read -r dummy1 dummy2 auditDaemonLogFile; do
		chmod 640 "${auditDaemonLogFile}" || { reportError "failed to correct file mode of ${auditDaemonLogFile}"; return 1; };
	done
	} < "${workDir}/rights_auditd_logfile_permissions.fixactions"
	return 0
}

registerItem "rights_auditd_logdir_permissions" "Check the audit log directory has the correct file mode" "CIS-Debian12-6.4.4.4"
check_rights_auditd_logdir_permissions () {
	enumerateAuditDaemonLogFiles
	test -n "${workDir}/auditDaemonLogDir" || return "${XYZ_RC_NOFAULT}"
	read -r auditDaemonLogDir < "${workDir}/auditDaemonLogDir"
	test -d "${auditDaemonLogDir}" || return "${XYZ_RC_NOFAULT}"
	auditDaemonLogDirMode="$(fsFileMode "${auditDaemonLogDir}")"
	test "${auditDaemonLogDirMode}" = "700" && return "${XYZ_RC_NOFAULT}"
	test "${auditDaemonLogDirMode}" = "0700" && return "${XYZ_RC_NOFAULT}"
	rightsCheckExactPermissions "750" "${auditDaemonLogDir}" "${XYZ_RC_FAULT_LOWRISK}"
	return $?
}
fix_rights_auditd_logdir_permissions () {
	chmod 750 "${auditDaemonLogDir}" || { reportError "failed to correct file mode of ${auditDaemonLogDir}"; return 1; };
	return 0
}

registerItem "rights_auditd_config_owning_user" "Check the audit daemon configuration files are owned by the root user" "CIS-Debian12-6.4.4.6"
check_rights_auditd_config_owning_user () {
	enumerateAuditDaemonConfigFiles
	{
	while read -r auditDaemonConfigFile; do
		test -e "${auditDaemonConfigFile}" || continue
		test "$(fsOwnerUid "${auditDaemonConfigFile}")" = "0" && continue
		printf "%s %s\n" "chown 0" "${auditDaemonConfigFile}"
	done
	} < "${workDir}/auditDaemonConfigFiles" > "${workDir}/rights_auditd_config_owning_user.fixactions"
	test -s "${workDir}/rights_auditd_config_owning_user.fixactions" || return "${XYZ_RC_NOFAULT}"
	faultDescription="Audit daemon configuration files are not owned by the root user"
	fixActions="$(cat "${workDir}/rights_auditd_config_owning_user.fixactions")"
	return "${XYZ_RC_FAULT_LOWRISK}"
}
fix_rights_auditd_config_owning_user () {
	{
	# shellcheck disable=SC2034
	while read -r dummy1 dummy2 auditDaemonConfigFile; do
		chown 0 "${auditDaemonConfigFile}" || { reportError "failed to correct owning user of ${auditDaemonConfigFile}"; return 1; };
	done
	} < "${workDir}/rights_auditd_config_owning_user.fixactions"
	return 0
}

registerItem "rights_auditd_config_owning_group" "Check the audit daemon configuration files are owned by GID 0" "CIS-Debian12-6.4.4.7"
check_rights_auditd_config_owning_group () {
	enumerateAuditDaemonConfigFiles
	{
	while read -r auditDaemonConfigFile; do
		test -e "${auditDaemonConfigFile}" || continue
		test "$(fsOwnerGid "${auditDaemonConfigFile}")" = "0" && continue
		printf "%s %s\n" "chown :0" "${auditDaemonConfigFile}"
	done
	} < "${workDir}/auditDaemonConfigFiles" > "${workDir}/rights_auditd_config_owning_group.fixactions"
	test -s "${workDir}/rights_auditd_config_owning_group.fixactions" || return "${XYZ_RC_NOFAULT}"
	faultDescription="Audit daemon configuration files are not owned by GID 0"
	fixActions="$(cat "${workDir}/rights_auditd_config_owning_group.fixactions")"
	return "${XYZ_RC_FAULT_LOWRISK}"
}
fix_rights_auditd_config_owning_group () {
	{
	# shellcheck disable=SC2034
	while read -r dummy1 dummy2 auditDaemonConfigFile; do
		chown :0 "${auditDaemonConfigFile}" || { reportError "failed to correct owning group of ${auditDaemonConfigFile}"; return 1; };
	done
	} < "${workDir}/rights_auditd_config_owning_group.fixactions"
	return 0
}

registerItem "rights_auditd_config_permissions" "Check the audit daemon configuration files have the correct file mode" "CIS-Debian12-6.4.4.5"
check_rights_auditd_config_permissions () {
	enumerateAuditDaemonConfigFiles
	{
	while read -r auditDaemonConfigFile; do
		test -e "${auditDaemonConfigFile}" || continue
		if fsOtherCanRead "${auditDaemonConfigFile}" || fsGroupOrOtherCanDoMoreThanRead "${auditDaemonConfigFile}"; then
			printf "%s %s\n" "chmod 640" "${auditDaemonConfigFile}"
		fi
	done
	} < "${workDir}/auditDaemonConfigFiles" > "${workDir}/rights_auditd_config_permissions.fixactions"
	test -s "${workDir}/rights_auditd_config_permissions.fixactions" || return "${XYZ_RC_NOFAULT}"
	faultDescription="Audit daemon configuration files do not have the correct file mode"
	fixActions="$(cat "${workDir}/rights_auditd_config_permissions.fixactions")"
	return "${XYZ_RC_FAULT_LOWRISK}"
}
fix_rights_auditd_config_permissions () {
	{
	# shellcheck disable=SC2034
	while read -r dummy1 dummy2 auditDaemonConfigFile; do
		chmod 640 "${auditDaemonConfigFile}" || { reportError "failed to correct file mode of ${auditDaemonConfigFile}"; return 1; };
	done
	} < "${workDir}/rights_auditd_config_permissions.fixactions"
	return 0
}

registerItem "rights_etcpasswd_owning_user" "Check /etc/passwd is owned by the root user" "CIS-Debian12-7.1.1"
check_rights_etcpasswd_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/passwd" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcpasswd_owning_user () { chown root "${rootDir}/etc/passwd" || { reportError "failed to correct /etc/passwd owning user"; return 1; }; return 0; }

registerItem "rights_etcpasswd_owning_group" "Check /etc/passwd is owned by GID 0" "CIS-Debian12-7.1.1"
check_rights_etcpasswd_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/passwd" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcpasswd_owning_group () { chown :0 "${rootDir}/etc/passwd" || { reportError "failed to correct /etc/passwd owning group"; return 1; }; return 0; }

registerItem "rights_etcpasswd_permissions" "Check /etc/passwd has the correct file mode" "CIS-Debian12-7.1.1"
check_rights_etcpasswd_permissions () { fsGroupOrOtherCanDoMoreThanRead "${rootDir}/etc/passwd" && return "${XYZ_RC_LOWRISK}"; fsHasExecutePermissions "${rootDir}/etc/passwd" && return "${XYZ_RC_LOWRISK}"; return "${XYZ_RC_NOFAULT}"; }
fix_rights_etcpasswd_permissions () { chmod u-x,go-wx "${rootDir}/etc/passwd" || { reportError "failed to correct /etc/passwd file mode"; return 1; }; return 0; }

registerItem "rights_etcpasswddash_owning_user" "Check /etc/passwd- is owned by the root user" "CIS-Debian12-7.1.2"
check_rights_etcpasswddash_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/passwd-" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcpasswddash_owning_user () { chown root "${rootDir}/etc/passwd-" || { reportError "failed to correct /etc/passwd- owning user"; return 1; }; return 0; }

registerItem "rights_etcpasswddash_owning_group" "Check /etc/passwd- is owned by GID 0" "CIS-Debian12-7.1.2"
check_rights_etcpasswddash_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/passwd-" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcpasswddash_owning_group () { chown :0 "${rootDir}/etc/passwd-" || { reportError "failed to correct /etc/passwd- owning group"; return 1; }; return 0; }

registerItem "rights_etcpasswddash_permissions" "Check /etc/passwd- has the correct file mode" "CIS-Debian12-7.1.2"
check_rights_etcpasswddash_permissions () { fsGroupOrOtherCanDoMoreThanRead "${rootDir}/etc/passwd-" && return "${XYZ_RC_LOWRISK}"; fsHasExecutePermissions "${rootDir}/etc/passwd-" && return "${XYZ_RC_LOWRISK}"; return "${XYZ_RC_NOFAULT}"; }
fix_rights_etcpasswddash_permissions () { chmod u-x,go-wx "${rootDir}/etc/passwd-" || { reportError "failed to correct /etc/passwd- file mode"; return 1; }; return 0; }

registerItem "rights_etcgroup_owning_user" "Check /etc/group is owned by the root user" "CIS-Debian12-7.1.3"
check_rights_etcgroup_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/group" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcgroup_owning_user () { chown root "${rootDir}/etc/group" || { reportError "failed to correct /etc/group owning user"; return 1; }; return 0; }

registerItem "rights_etcgroup_owning_group" "Check /etc/group is owned by GID 0" "CIS-Debian12-7.1.3"
check_rights_etcgroup_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/group" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcgroup_owning_group () { chown :0 "${rootDir}/etc/group" || { reportError "failed to correct /etc/group owning group"; return 1; }; return 0; }

registerItem "rights_etcgroup_permissions" "Check /etc/group has the correct file mode" "CIS-Debian12-7.1.3"
check_rights_etcgroup_permissions () { fsGroupOrOtherCanDoMoreThanRead "${rootDir}/etc/group" && return "${XYZ_RC_LOWRISK}"; fsHasExecutePermissions "${rootDir}/etc/group" && return "${XYZ_RC_LOWRISK}"; return "${XYZ_RC_NOFAULT}"; }
fix_rights_etcgroup_permissions () { chmod u-x,go-wx "${rootDir}/etc/group" || { reportError "failed to correct /etc/group file mode"; return 1; }; return 0; }

registerItem "rights_etcgroupdash_owning_user" "Check /etc/group- is owned by the root user" "CIS-Debian12-7.1.4"
check_rights_etcgroupdash_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/group-" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcgroupdash_owning_user () { chown root "${rootDir}/etc/group-" || { reportError "failed to correct /etc/group- owning user"; return 1; }; return 0; }

registerItem "rights_etcgroupdash_owning_group" "Check /etc/group- is owned by GID 0" "CIS-Debian12-7.1.4"
check_rights_etcgroupdash_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/group-" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcgroupdash_owning_group () { chown :0 "${rootDir}/etc/group-" || { reportError "failed to correct /etc/group- owning group"; return 1; }; return 0; }

registerItem "rights_etcgroupdash_permissions" "Check /etc/group- has the correct file mode" "CIS-Debian12-7.1.4"
check_rights_etcgroupdash_permissions () { fsGroupOrOtherCanDoMoreThanRead "${rootDir}/etc/group-" && return "${XYZ_RC_LOWRISK}"; fsHasExecutePermissions "${rootDir}/etc/group-" && return "${XYZ_RC_LOWRISK}"; return "${XYZ_RC_NOFAULT}"; }
fix_rights_etcgroupdash_permissions () { chmod u-x,go-wx "${rootDir}/etc/group-" || { reportError "failed to correct /etc/group- file mode"; return 1; }; return 0; }

registerItem "rights_etcshadow_owning_user" "Check /etc/shadow is owned by the root user" "CIS-Debian12-7.1.5"
check_rights_etcshadow_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/shadow" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcshadow_owning_user () { chown root "${rootDir}/etc/shadow" || { reportError "failed to correct /etc/shadow owning user"; return 1; }; return 0; }

registerItem "rights_etcshadow_owning_group" "Check /etc/shadow is owned by GID 0" "CIS-Debian12-7.1.5"
check_rights_etcshadow_owning_group () { rightsCheckOwnerGroupName "root shadow" "${rootDir}/etc/shadow" "${XYZ_RC_FAULT_HIGHRISK}"; return $?; }
fix_rights_etcshadow_owning_group () {
	shadowOwningGroup="root"
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && shadowOwningGroup="shadow"
	chown ":${shadowOwningGroup}" "${rootDir}/etc/shadow" || { reportError "failed to correct /etc/shadow owning group"; return 1; }
	return 0
}

registerItem "rights_etcshadow_permissions" "Check that /etc/shadow is not world readable" "CIS-Debian12-7.1.5"
check_rights_etcshadow_permissions () {
	test -e "${rootDir}/etc/shadow" || return "${XYZ_RC_NOFAULT}"
	fileMode=$(fsFileMode "${rootDir}/etc/shadow")
	test -n "${fileMode}" || return "${XYZ_RC_NOFAULT}"
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return "${XYZ_RC_NOFAULT}"
	test "${lastOctet}" = "0" && return "${XYZ_RC_NOFAULT}"
	faultDescription="The /etc/shadow file is world accessible"
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && fixActions="chmod 640 /etc/shadow" || fixActions="chmod 600 /etc/shadow"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_rights_etcshadow_permissions () {
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && chmod 640 "${rootDir}/etc/shadow" || chmod 600 "${rootDir}/etc/shadow" || return 1
	return 0
}

registerItem "rights_etcshadowdash_owning_user" "Check /etc/shadow- is owned by the root user" "CIS-Debian12-7.1.6"
check_rights_etcshadowdash_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/shadow-" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcshadowdash_owning_user () { chown root "${rootDir}/etc/shadow-" || { reportError "failed to correct /etc/shadow- owning user"; return 1; }; return 0; }

registerItem "rights_etcshadowdash_owning_group" "Check /etc/shadow- is owned by GID 0" "CIS-Debian12-7.1.6"
check_rights_etcshadowdash_owning_group () { rightsCheckOwnerGroupName "root shadow" "${rootDir}/etc/shadow-" "${XYZ_RC_FAULT_HIGHRISK}"; return $?; }
fix_rights_etcshadowdash_owning_group () {
	shadowOwningGroup="root"
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && shadowOwningGroup="shadow"
	chown ":${shadowOwningGroup}" "${rootDir}/etc/shadow-" || { reportError "failed to correct /etc/shadow- owning group"; return 1; }
	return 0
}

registerItem "rights_etcshadowdash_permissions" "Check that /etc/shadow- is not world readable" "CIS-Debian12-7.1.6"
check_rights_etcshadowdash_permissions () {
	test -e "${rootDir}/etc/shadow-" || return "${XYZ_RC_NOFAULT}"
	fileMode=$(fsFileMode "${rootDir}/etc/shadow-")
	test -n "${fileMode}" || return "${XYZ_RC_NOFAULT}"
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return "${XYZ_RC_NOFAULT}"
	test "${lastOctet}" = "0" && return "${XYZ_RC_NOFAULT}"
	faultDescription="The /etc/shadow- file is world accessible"
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && fixActions="chmod 640 /etc/shadow-" || fixActions="chmod 600 /etc/shadow-"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_rights_etcshadowdash_permissions () {
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && chmod 640 "${rootDir}/etc/shadow-" || chmod 600 "${rootDir}/etc/shadow-" || return 1
	return 0
}

registerItem "rights_etcgshadow_owning_user" "Check /etc/gshadow is owned by the root user" "CIS-Debian12-7.1.7"
check_rights_etcgshadow_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/gshadow" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcgshadow_owning_user () { chown root "${rootDir}/etc/gshadow" || { reportError "failed to correct /etc/gshadow owning user"; return 1; }; return 0; }

registerItem "rights_etcgshadow_owning_group" "Check /etc/gshadow is owned by GID 0" "CIS-Debian12-7.1.7"
check_rights_etcgshadow_owning_group () { rightsCheckOwnerGroupName "root shadow" "${rootDir}/etc/gshadow" "${XYZ_RC_FAULT_HIGHRISK}"; return $?; }
fix_rights_etcgshadow_owning_group () {
	gshadowOwningGroup="root"
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && gshadowOwningGroup="shadow"
	chown ":${gshadowOwningGroup}" "${rootDir}/etc/gshadow" || { reportError "failed to correct /etc/gshadow owning group"; return 1; }
	return 0
}

registerItem "rights_etcgshadow_permissions" "Check that /etc/gshadow is not world readable" "CIS-Debian12-7.1.7"
check_rights_etcgshadow_permissions () {
	test -e "${rootDir}/etc/gshadow" || return "${XYZ_RC_NOFAULT}"
	fileMode=$(fsFileMode "${rootDir}/etc/gshadow")
	test -n "${fileMode}" || return "${XYZ_RC_NOFAULT}"
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return "${XYZ_RC_NOFAULT}"
	test "${lastOctet}" = "0" && return "${XYZ_RC_NOFAULT}"
	faultDescription="The /etc/gshadow file is world accessible"
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && fixActions="chmod 640 /etc/gshadow" || fixActions="chmod 600 /etc/gshadow"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_rights_etcgshadow_permissions () {
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && chmod 640 "${rootDir}/etc/gshadow" || chmod 600 "${rootDir}/etc/gshadow" || return 1
	return 0
}

registerItem "rights_etcgshadowdash_owning_user" "Check /etc/gshadow- is owned by the root user" "CIS-Debian12-7.1.8"
check_rights_etcgshadowdash_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/gshadow-" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcgshadowdash_owning_user () { chown root "${rootDir}/etc/gshadow-" || { reportError "failed to correct /etc/gshadow- owning user"; return 1; }; return 0; }

registerItem "rights_etcgshadowdash_owning_group" "Check /etc/gshadow- is owned by GID 0" "CIS-Debian12-7.1.8"
check_rights_etcgshadowdash_owning_group () { rightsCheckOwnerGroupName "root shadow" "${rootDir}/etc/gshadow-" "${XYZ_RC_FAULT_HIGHRISK}"; return $?; }
fix_rights_etcgshadowdash_owning_group () {
	gshadowOwningGroup="root"
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && gshadowOwningGroup="shadow"
	chown ":${gshadowOwningGroup}" "${rootDir}/etc/gshadow-" || { reportError "failed to correct /etc/gshadow- owning group"; return 1; }
	return 0
}

registerItem "rights_etcgshadowdash_permissions" "Check that /etc/gshadow- is not world readable" "CIS-Debian12-7.1.8"
check_rights_etcgshadowdash_permissions () {
	test -e "${rootDir}/etc/gshadow-" || return "${XYZ_RC_NOFAULT}"
	fileMode=$(fsFileMode "${rootDir}/etc/gshadow-")
	test -n "${fileMode}" || return "${XYZ_RC_NOFAULT}"
	lastOctet=$(printf "%s\n" "${fileMode}" | rev | cut -b 1)
	test -n "${lastOctet}" || return "${XYZ_RC_NOFAULT}"
	test "${lastOctet}" = "0" && return "${XYZ_RC_NOFAULT}"
	faultDescription="The /etc/gshadow- file is world accessible"
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && fixActions="chmod 640 /etc/gshadow-" || fixActions="chmod 600 /etc/gshadow-"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_rights_etcgshadowdash_permissions () {
	grep -Eq "^shadow:" "${rootDir}/etc/group" 2>/dev/null && chmod 640 "${rootDir}/etc/gshadow-" || chmod 600 "${rootDir}/etc/gshadow-" || return 1
	return 0
}

registerItem "rights_etcshells_owning_user" "Check /etc/shells is owned by the root user" "CIS-Debian12-7.1.9"
check_rights_etcshells_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/shells" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcshells_owning_user () { chown root "${rootDir}/etc/shells" || { reportError "failed to correct /etc/shells owning user"; return 1; }; return 0; }

registerItem "rights_etcshells_owning_group" "Check /etc/shells is owned by GID 0" "CIS-Debian12-7.1.9"
check_rights_etcshells_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/shells" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcshells_owning_group () { chown :0 "${rootDir}/etc/shells" || { reportError "failed to correct /etc/shells owning group"; return 1; }; return 0; }

registerItem "rights_etcshells_permissions" "Check /etc/shells has the correct file mode" "CIS-Debian12-7.1.9"
check_rights_etcshells_permissions () { fsGroupOrOtherCanDoMoreThanRead "${rootDir}/etc/shells" && return "${XYZ_RC_LOWRISK}"; fsHasExecutePermissions "${rootDir}/etc/shells" && return "${XYZ_RC_LOWRISK}"; return "${XYZ_RC_NOFAULT}"; }
fix_rights_etcshells_permissions () { chmod u-x,go-wx "${rootDir}/etc/shells" || { reportError "failed to correct /etc/shells file mode"; return 1; }; return 0; }

registerItem "rights_etcsecurityopasswd_owning_user" "Check /etc/security/opasswd is owned by the root user" "CIS-Debian12-7.1.10"
check_rights_etcsecurityopasswd_owning_user () { rightsCheckOwnerAccountName "root" "${rootDir}/etc/security/opasswd" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcsecurityopasswd_owning_user () { chown root "${rootDir}/etc/security/opasswd" || { reportError "failed to correct /etc/security/opasswd owning user"; return 1; }; return 0; }

registerItem "rights_etcsecurityopasswd_owning_group" "Check /etc/security/opasswd is owned by GID 0" "CIS-Debian12-7.1.10"
check_rights_etcsecurityopasswd_owning_group () { rightsCheckOwnerGid "0" "${rootDir}/etc/security/opasswd" "${XYZ_RC_FAULT_LOWRISK}"; return $?; }
fix_rights_etcsecurityopasswd_owning_group () { chown :0 "${rootDir}/etc/security/opasswd" || { reportError "failed to correct /etc/security/opasswd owning group"; return 1; }; return 0; }

registerItem "rights_etcsecurityopasswd_permissions" "Check /etc/security/opasswd has the correct file mode" "CIS-Debian12-7.1.10"
check_rights_etcsecurityopasswd_permissions () {
	fsGroupOrOtherCanRead "${rootDir}/etc/security/opasswd" && return "${XYZ_RC_LOWRISK}"
	fsGroupOrOtherCanDoMoreThanRead "${rootDir}/etc/security/opasswd" && return "${XYZ_RC_LOWRISK}"
	fsHasExecutePermissions "${rootDir}/etc/security/opasswd" && return "${XYZ_RC_LOWRISK}"
	return "${XYZ_RC_NOFAULT}"
}
fix_rights_etcsecurityopasswd_permissions () { chmod 600 "${rootDir}/etc/security/opasswd" || { reportError "failed to correct /etc/security/opasswd file mode"; return 1; }; return 0; }
