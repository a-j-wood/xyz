#!/bin/sh
#
# Checks and fixes relating to scheduled jobs.
#
# Copyright 2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

# Scheduler locations.  Different on different operating systems.
schedCronAllowFile="${rootDir}/etc/cron.allow"
schedCronDenyFile="${rootDir}/etc/cron.deny"
schedCrontabDir="${rootDir}/var/spool/cron/crontabs"
schedAtAllowFile="${rootDir}/etc/at.allow"
schedAtDenyFile="${rootDir}/etc/at.deny"

# Debian uses /var/spool/cron/crontabs but RHEL uses /var/spool/cron.
test -d "${schedCrontabDir}" || schedCrontabDir="${rootDir}/var/spool/cron"

case "${kernelName}" in
*bsd)
	schedCronAllowFile="${rootDir}/var/cron/cron.allow"
	schedCronDenyFile="${rootDir}/var/cron/cron.deny"
	schedCrontabDir="${rootDir}/var/cron/tabs"
	schedAtAllowFile="${rootDir}/var/cron/at.allow"
	schedAtDenyFile="${rootDir}/var/cron/at.deny"
	;;
esac

registerItem "sched_cron_generally_permitted" "Check whether non-root users are generally allowed to use crontab" "CIS-Debian12-2.4.1.8" crontab
check_sched_cron_generally_permitted () {
	# If there is an "allow" file, access is restricted.
	test -e "${schedCronAllowFile}" && return "${XYZ_RC_NOFAULT}"
	# If there is a "deny" file, access is restricted, but allowed by default.
	if test -e "${schedCronDenyFile}"; then
		faultDescription="The cron deny list file exists, which allows crontab access to all users not listed in it"
		fixActions="create an empty file ${schedCronAllowFile}"
		return "${XYZ_RC_FAULT_LOWRISK}"
	fi
	# If there is neither an allow nor a deny file, we can't say whether
	# access is definitely restricted.
	faultDescription="There is no cron allow list file, so crontab access may be permitted for all users"
 	fixActions="create an empty file ${schedCronAllowFile}"
	return "${XYZ_RC_FAULT_LOWRISK}"
}
fix_sched_cron_generally_permitted () {
	true >> "${schedCronAllowFile}"
	chmod 644 "${schedCronAllowFile}"
	return $?
}

registerItem "sched_at_generally_permitted" "Check whether non-root users are generally allowed to use at jobs" "CIS-Debian12-2.4.2.1" at
check_sched_at_generally_permitted () {
	# If there is an "allow" file, access is restricted.
	test -e "${schedAtAllowFile}" && return "${XYZ_RC_NOFAULT}"
	# If there is a "deny" file, access is restricted, but allowed by default.
	if test -e "${schedAtDenyFile}"; then
		faultDescription="The 'at' deny list file exists, which allows 'at' access to all users not listed in it"
		fixActions="create an empty file ${schedAtAllowFile}"
		return "${XYZ_RC_FAULT_LOWRISK}"
	fi
	# If there is neither an allow nor a deny file, we can't say whether
	# access is definitely restricted.
	faultDescription="There is no 'at' allow list file, so 'at' access may be permitted for all users"
 	fixActions="create an empty file ${schedAtAllowFile}"
	return "${XYZ_RC_FAULT_LOWRISK}"
}
fix_sched_at_generally_permitted () {
	true >> "${schedAtAllowFile}"
	chmod 644 "${schedAtAllowFile}"
	return $?
}

registerItem "sched_user_crontabs" "Check whether any user crontabs exist" "" crontab
check_sched_user_crontabs () {
	userCrontabList="$(find "${schedCrontabDir}" -mindepth 1 -maxdepth 1 -type f -not -name 'root' -printf '%f\n' 2>/dev/null)"
	test -n "${userCrontabList}" || return "${XYZ_RC_NOFAULT}"
	faultDescription="One or more non-root users have a crontab"
 	fixActions="remove user crontabs: $(printf '%s\n' "${userCrontabList}" | sort | tr '\n' ' ')"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_sched_user_crontabs () {
	find "${schedCrontabDir}" -mindepth 1 -maxdepth 1 -type f -not -name 'root' -printf '%f\n' 2>/dev/null > "${workDir}/userCrontabs"
	rc=0
	{
	while read -r crontabUser; do
		crontab -u "${crontabUser}" -r
		cmdStatus=$?
		if ! test "${cmdStatus}" -eq 0; then
			reportError "${crontabUser}: crontab removal failed with status ${cmdStatus}"
			rc=1
		fi
	done
	} 3<&0 < "${workDir}/userCrontabs"
	return "${rc}"
}

registerItem "sched_user_atjobs" "Check whether any user at jobs exist" "" atq atrm
check_sched_user_atjobs () {
	userAtJobUsernames="$(atq 2>/dev/null | awk '$NF!="root"{print $NF}' | sort -u)"
	test -n "${userAtJobUsernames}" || return "${XYZ_RC_NOFAULT}"
	faultDescription="One or more non-root users have at least one 'at' job"
 	fixActions="remove 'at' jobs for users: $(printf '%s\n' "${userAtJobUsernames}" | sort | tr '\n' ' ')"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_sched_user_atjobs () {
	atq 2>/dev/null | awk '$NF!="root"{print $1}' > "${workDir}/atJobs"
	rc=0
	{
	while read -r atJobId; do
		atrm "${atJobId}"
		cmdStatus=$?
		if ! test "${cmdStatus}" -eq 0; then
			reportError "${atJobId}: removal of 'at' job failed with status ${cmdStatus}"
			rc=1
		fi
	done
	} 3<&0 < "${workDir}/atJobs"
	return "${rc}"
}

registerItem "sched_root_crontab" "Check whether root has a crontab" "" crontab
check_sched_root_crontab () {
	test -s "${schedCrontabDir}/root" || return "${XYZ_RC_NOFAULT}"
	faultDescription="The root user has a crontab"
 	fixActions="remove the root crontab"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_sched_root_crontab () {
	crontab -u root -r
	return $?
}

registerItem "sched_root_atjobs" "Check whether any root at jobs exist" "" atq atrm
check_sched_root_atjobs () {
	userAtJobList="$(atq 2>/dev/null | awk '$NF=="root"{print $1}')"
	test -n "${userAtJobList}" || return "${XYZ_RC_NOFAULT}"
	faultDescription="The root user has at least one 'at' job"
 	fixActions="remove all root 'at' jobs"
	return "${XYZ_RC_FAULT_HIGHRISK}"
}
fix_sched_root_atjobs () {
	atq 2>/dev/null | awk '$NF=="root"{print $1}' > "${workDir}/atJobs"
	rc=0
	{
	while read -r atJobId; do
		atrm "${atJobId}"
		cmdStatus=$?
		if ! test "${cmdStatus}" -eq 0; then
			reportError "${atJobId}: removal of 'at' job failed with status ${cmdStatus}"
			rc=1
		fi
	done
	} 3<&0 < "${workDir}/atJobs"
	return "${rc}"
}
