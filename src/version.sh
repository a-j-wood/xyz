#!/bin/sh
#
# Output version information.
#
# Copyright 2024-2025 Andrew Wood
#
# License GPLv3+: GNU GPL version 3 or later; see `docs/COPYING'.
#

xyzVersion () {
	if test "${PROGRAM_NAME}" = "${PACKAGE_NAME}"; then
		printf "%s %s\n" "${PROGRAM_NAME}" "${PACKAGE_VERSION}"
	else
		printf "%s (%s) %s\n" "${PROGRAM_NAME}" "${PACKAGE_NAME}" "${PACKAGE_VERSION}"
	fi
	printf "Copyright %s %s\n" "2025" "Andrew Wood"
	printf "%s\n" "License: GPLv3+ <https://www.gnu.org/licenses/gpl-3.0.html>"
	printf "%s\n" "This is free software: you are free to change and redistribute it."
	printf "%s\n" "There is NO WARRANTY, to the extent permitted by law."
	printf "\n%s: <%s>\n" "Project web site" "${PACKAGE_URL}"
}
